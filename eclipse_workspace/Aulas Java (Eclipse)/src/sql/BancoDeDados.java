package sql;
import java.sql.*;

/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 10 de dez de 2016
*/
public class BancoDeDados {

	private static String url = "jdbc:mysql://localhost:3306/citacoesbd";
	private static String user= "citacoes_admin";
	private static String pass= "Teste123";
	
	protected static Connection conexao = null;
	
	public BancoDeDados(){
		if(getConexao() == null)	conecta();
	}
	
	public static boolean conecta(){
		try{
			setConexao(DriverManager.getConnection(url, user, pass));
			return true;
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	public static boolean desconecta(){
		try{
			getConexao().close();
			return true;
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * @return the conexao
	 */
	public static Connection getConexao() {
		return conexao;
	}

	/**
	 * @param conexao the conexao to set
	 */
	public static void setConexao(Connection conexao) {
		BancoDeDados.conexao = conexao;
	}
	
}
