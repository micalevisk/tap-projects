package sql;
import java.sql.*;

/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 10 de dez de 2016
*/
public class PersonagemDAO extends BancoDeDados {

	///////////////////////////////////////////////////
	public static void main(String[] args){
		PersonagemDAO tblpersonagem = new PersonagemDAO();
		
		System.out.println("--------------------");
		Personagem p1 = new Personagem("micalevisk", "Micael", "nenhum");
		boolean r = tblpersonagem.adicionarPersonagem(p1);
		System.out.printf("Inseriu? %s\n", r ? "sim" : "não");
		System.out.println("--------------------");
		
		
		System.out.println("--------------------");
		tblpersonagem.listarPersonagens();
		System.out.println("--------------------");
		
		
		System.out.println("--------------------");
		Personagem p = tblpersonagem.getPersonagem("rocket");
		System.out.println(p.toString());
		System.out.println("--------------------");
	}
	///////////////////////////////////////////////////
	
	
	public void listarPersonagens(){
		try{
			
			/// Cria um objeto para execução de comandos
			Statement st = super.conexao.createStatement();
			
			/// Executa uma consulta SQL
			ResultSet rs = st.executeQuery("SELECT * FROM personagens"); // SELECT * FROM {tabela} 
			
			/// Itera nas linhas da tabela retornada
			while(rs.next()){
				System.out.println(String.format("Personagem  %s (%s) do filme %s", 
						rs.getString(3), // coluna 3 (apelido)
						rs.getString(2), // coluna 2 (nome) 
						rs.getString(4)  // coluna 4 (filme)						
				));
			}
			
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
	}

	
	public boolean adicionarPersonagem(Personagem p){
		try{
			Statement st = super.conexao.createStatement();
			/// Executa um comando SQL
			st.executeUpdate(String.format("INSERT INTO personagens VALUES (%s, %s, %s, %s)", // INSERT INTO {tabela} VALUES ({p1}, {p2}, {p3}, {p4})
					"NULL",
					p.getApelido(),
					p.getNome(),
					p.getFilme()
			));
			return true;
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
			return false;
		}
	}

	
	public Personagem getPersonagem(String apelido){ // {coluna}
		try{
			Statement st = conexao.createStatement();
			ResultSet rs = st.executeQuery(String.format("SELECT * FROM personagens WHERE apelido='%s'", apelido)); // SELECT * FROM {tabela} WHERE {coluna}='{valor}'
			
			if(rs.next())
				return new Personagem(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
			
			return null;
		}
		catch(SQLException e){
			return null;
		}
	}
	

	/** IMPLEMENTAR:
	public boolean atualizarPersonagem(int id, Personagem p){
	}
	
	public boolean removerPersonagem(int id, Personagem p){
	}
	*/
}
