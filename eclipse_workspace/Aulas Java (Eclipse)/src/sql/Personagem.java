package sql;
/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 10 de dez de 2016
*/
public class Personagem {

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the apelido
	 */
	public String getApelido() {
		return apelido;
	}

	/**
	 * @param apelido the apelido to set
	 */
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the filme
	 */
	public String getFilme() {
		return filme;
	}

	/**
	 * @param filme the filme to set
	 */
	public void setFilme(String filme) {
		this.filme = filme;
	}

	/**
	 * @return the citacoes
	 */
	public Citacao[] getCitacoes() {
		return citacoes;
	}

	/**
	 * @param citacoes the citacoes to set
	 */
	public void setCitacoes(Citacao[] citacoes) {
		this.citacoes = citacoes;
	}

	private int id;
	private String apelido;
	private String nome;
	private String filme;
	private Citacao[] citacoes;
	
	public Personagem(String apelido, String nome, String filme){
		this(0, apelido, nome, filme);
	}
	
	public Personagem(int id, String apelido, String nome, String filme){
		this.id = id;
		this.apelido = apelido;
		this.nome = nome;
		this.filme = filme;
	}
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("{\n id:'%s'\n apelido:'%s'\n nome:'%s'\n filme:'%s'\n}", this.id, this.apelido, this.nome, this.filme);
	}
}
