package sql;
/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 10 de dez de 2016
*/
public class Citacao {
	
	private int id;
	private Personagem personagem;
	private String citacao;
	
	public Citacao(Personagem personagem, String citacao) {
		this.personagem = personagem;
		this.citacao = citacao;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the personagem
	 */
	public Personagem getPersonagem() {
		return personagem;
	}

	/**
	 * @param personagem the personagem to set
	 */
	public void setPersonagem(Personagem personagem) {
		this.personagem = personagem;
	}

	/**
	 * @return the citacao
	 */
	public String getCitacao() {
		return citacao;
	}

	/**
	 * @param citacao the citacao to set
	 */
	public void setCitacao(String citacao) {
		this.citacao = citacao;
	}
}
