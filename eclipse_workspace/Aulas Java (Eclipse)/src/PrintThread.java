/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 22 de dez de 2016
*/
public class PrintThread extends Thread {
	
	int from, to;
	
	public PrintThread(int from, int to){
		this.from = from;
		this.to   = to;
	}
	
	public void run(){
		System.out.println(Thread.currentThread());
		for(int i=from; i < to; ++i) System.out.println("i = "+i);
	}
	
	public static void main(String[] args) {
		int i;
		for(i=0; i<5; ++i){
			PrintThread pi = new PrintThread(i*10, (i+1)*10);
			pi.start();
		}
	}

}
