import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainClass extends JFrame {
  private JTable table;

  public static void main(String[] args) throws Exception {
    MainClass frame = new MainClass();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }

  public MainClass() throws Exception {
    getContentPane().setLayout(new BorderLayout());

    String[] columnNames = { "First Name", "Last Name", "Sport", "# of Years", "Vegetarian" };
    Object[][] data = { { "A", "B", "C", new Integer(5), new Boolean(false) },
    					{ "D", "E", "F", new Integer(3), new Boolean(true) } };


    table = new JTable(data, columnNames);

    JPanel tPanel = new JPanel(new BorderLayout());
    tPanel.add(table.getTableHeader(), BorderLayout.NORTH);
    tPanel.add(table, BorderLayout.CENTER);

    getContentPane().add(tPanel, BorderLayout.CENTER);
    
    JButton btnCriar = new JButton("criar");
    btnCriar.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    		Document document = new Document();
    		PdfWriter writer;
    		
    		try {
				writer = PdfWriter.getInstance(document, new FileOutputStream("my_jtable_shapes.pdf"));
				document.open();
				PdfContentByte cb = writer.getDirectContent();

				float x = table.getWidth();
				float y = table.getHeight();
				x = y = 500;
				
				PdfTemplate tp = cb.createTemplate(x, y);
				Graphics2D g2 = tp.createGraphicsShapes(x, y);
				table.print(g2);
				g2.dispose();
				cb.addTemplate(tp, 30, 300);
				
				// step 5: we close the document
			} catch (FileNotFoundException | DocumentException e) {
				e.printStackTrace();
			} finally{
				document.close();
			}
    		
    	}
    });
    tPanel.add(btnCriar, BorderLayout.SOUTH);
    
  }
}