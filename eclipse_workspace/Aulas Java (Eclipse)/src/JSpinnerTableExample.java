import java.awt.Component;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class JSpinnerTableExample extends JFrame {
	
	
	
	public JSpinnerTableExample() {
		super("JSpinnerTable Example");
		
		
		
		
		SpinnerNumberModel spinnerModel1 = new SpinnerNumberModel(10.0, -500.0, 500.0, .5);
		SpinnerDateModel spinnerModel2 = new SpinnerDateModel();

		DefaultTableModel dtm = new DefaultTableModel();
		dtm.setDataVector(new Object[][] { { spinnerModel1, "JSpinner1" }, { spinnerModel2, "JSpinner2" } },
				new Object[] { "JSpinner", "String" });

		JTable table = new JTable(dtm);
		table.getColumn("JSpinner").setCellRenderer(new SpinnerRenderer());
		table.getColumn("JSpinner").setCellEditor(new SpinnerEditor());

		table.setRowHeight(20);
		JScrollPane scroll = new JScrollPane(table);
		getContentPane().add(scroll);

		setSize(400, 100);
		setVisible(true);
	}

	public static void main(String[] args) {
		JSpinnerTableExample frame = new JSpinnerTableExample();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}

class SpinnerRenderer extends JSpinner implements TableCellRenderer {
	public SpinnerRenderer() {
		setOpaque(true);
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		setModel((SpinnerModel) value);

		return this;
	}
}

class SpinnerEditor extends AbstractCellEditor implements TableCellEditor {
	protected JSpinner spinner;

	public SpinnerEditor() {
		spinner = new JSpinner();
	}

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		spinner.setModel((SpinnerModel) value);

		return spinner;
	}

	public Object getCellEditorValue() {
		SpinnerModel sm = spinner.getModel();
		return sm;
	}
}