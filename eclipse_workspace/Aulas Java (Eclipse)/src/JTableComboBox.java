import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
* (c) https://www.youtube.com/watch?v=F4wjbb91suQ
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 25 de dez de 2016
*/
public class JTableComboBox extends JFrame {

	private JPanel contentPane;

	private String[] strCategorias = new String[] {"t�cnico", "romance", "a��o/aventura", "religioso", "revista", "did�tico", "biogr�fico"};
	private JComboBox cbxCategoria;
	
	public JTableComboBox()
	{
		super("Tabela com ComboBox");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// OUR TABLE MODEL
		DefaultTableModel dm = new DefaultTableModel();
		
		// OUR DATA
		dm.setDataVector(new Object[][]
				{
				{"1","Ander","categoria..."},
				{"2","Ander","categoria..."},
				{"3","Ander","categoria..."},
				{"4","Ander","categoria..."},
				{"5","Ander","categoria..."},
				{"6","Ander","categoria..."},
				{"7","Ander","categoria..."},
				},
				new Object[]{"no.","Name","Position"});
		
		// OUR TABLE
		JTable table = new JTable(dm);
		
		// OUR COMBOBOX
		cbxCategoria = new JComboBox<String>(strCategorias);
		cbxCategoria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();  
		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
		cbxCategoria.setRenderer(UIResource);
		cbxCategoria.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae){
				// DISPLAY SELECTED STUFF
				System.out.println( cbxCategoria.getSelectedItem() );
			}
		});
		
		// OUR COMBO COLUMN
		TableColumn col = table.getColumnModel().getColumn(2);
		col.setCellEditor(new DefaultCellEditor(cbxCategoria));
		
		// OUR SCROLLPANE
		JScrollPane pane = new JScrollPane(table);
		getContentPane().add(pane);
		setSize(350,200);
	}
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JTableComboBox frame = new JTableComboBox();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	public JTableComboBox() {
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		cbxCategoria = new JComboBox();
		cbxCategoria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbxCategoria.setModel(new DefaultComboBoxModel(strCategorias));
		cbxCategoria.setSelectedIndex(0);
		cbxCategoria.setBounds(168, 22, 143, 29);
		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();  
		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
		cbxCategoria.setRenderer(UIResource);
		contentPane.add(cbxCategoria);
	}
	*/

	

}
