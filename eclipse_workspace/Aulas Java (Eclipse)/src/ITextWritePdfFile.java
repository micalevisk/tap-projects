/*****************************************************************/
/* Copyright 2013 Code Strategies                                */
/* This code may be freely used and distributed in any project.  */
/* However, please do not remove this credit if you publish this */
/* code in paper or electronic form, such as on a web site.      */
/*****************************************************************/

// (c) http://www.avajava.com/tutorials/lessons/how-do-i-write-to-a-pdf-file-using-itext.html?page=1
// (c) http://www.javacoderanch.com/how-to-create-a-table-in-pdf-document.html	http://www.mysamplecode.com/2012/10/create-table-pdf-java-and-itext.html

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
//import com.lowagie.text.List;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPCell;

public class ITextWritePdfFile {

	
	public static void main(String[] args) {
		String[][] data = new String[][] { 
				{ "1", "JDOW", "JOHN", "DOW" }, 
				{ "2", "STIGER", "SCOTT", "TIGER" },
				{ "3", "FBAR", "FOO", "BAR" } };
			
		List<String> headers = new ArrayList<String>() {{
			add("#");
		    add("A");
		    add("B");
		    add("C");
		}};

		//
		// Create a new document.
		//
		Document document = new Document(PageSize.LETTER.rotate());

		try {
			//
			// Get an instance of PdfWriter and create a Table.pdf file as an
			// output.
			//
			PdfWriter.getInstance(document, new FileOutputStream(new File("Table.pdf")));
			document.open();

			//
			// Create an instance of PdfPTable. After that we transform the
			// header and
			// data array into a PdfPCell object. When each table row is
			// complete we
			// have to call the table.completeRow() method.
			//
			// For better presentation we also set the cell font name, size and
			// weight.
			// And we also define the background fill for the cell.
			//
			float[] columnWidths = { 0.5f, 2f, 5f, 2f };
			// PdfPTable table = new PdfPTable(headers.length);
			PdfPTable table = new PdfPTable(columnWidths);

			//// INSERINDO OS HEADERS
			/*
			for(int i = 0; i < headers.length; i++) {
				String header = headers[i];
				PdfPCell cell = new PdfPCell();
				cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header.toUpperCase(), new Font(Font.HELVETICA, 10, Font.BOLD)));
				table.addCell(cell);
			}
			table.completeRow();
			*/
			headers.forEach(header -> {
				PdfPCell cell = new PdfPCell();
				cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header.toUpperCase(), new Font(Font.HELVETICA, 10, Font.BOLD)));
				table.addCell(cell);
			});
			table.completeRow();

			/// INSERINDO DADOS
			for (int i = 0; i < data.length; i++) {
				for (int j = 0; j < data[i].length; j++) {
					String datum = data[i][j];
					PdfPCell cell = new PdfPCell();
					cell.setPhrase(new Phrase(datum, new Font(Font.HELVETICA, 10, Font.NORMAL)));
					table.addCell(cell);
				}
				table.completeRow();
			}

			document.addTitle("Table Demo");
			document.add(table);

			System.out.println("Gerado com sucesso!");

		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			document.close();
		}
	}

	/**
	 * public static void main(String[] args) { try { File file = new
	 * File("itext-test.pdf"); FileOutputStream fileout = new
	 * FileOutputStream(file); Document document = new Document();
	 * PdfWriter.getInstance(document, fileout); document.addAuthor("Me");
	 * document.addTitle("My iText Test");
	 * 
	 * document.open();
	 * 
	 * Chunk chunk = new Chunk("iText Test"); Font font = new
	 * Font(Font.COURIER); font.setStyle(Font.UNDERLINE);
	 * font.setStyle(Font.ITALIC); chunk.setFont(font);
	 * chunk.setBackground(Color.CYAN); document.add(chunk);
	 * 
	 * Paragraph paragraph = new Paragraph(); paragraph.add("Hello World");
	 * paragraph.setAlignment(Element.ALIGN_CENTER); document.add(paragraph);
	 * 
	 * Image image; try { image = Image.getInstance("world.gif");
	 * image.setAlignment(Image.MIDDLE); document.add(image); } catch
	 * (MalformedURLException e) { e.printStackTrace(); } catch (IOException e)
	 * { e.printStackTrace(); }
	 * 
	 * List list = new List(true, 15); list.add("ABC"); list.add("DEF");
	 * document.add(list);
	 * 
	 * document.close(); } catch (FileNotFoundException e) {
	 * e.printStackTrace(); } catch (DocumentException e) { e.printStackTrace();
	 * } }
	 */
}
