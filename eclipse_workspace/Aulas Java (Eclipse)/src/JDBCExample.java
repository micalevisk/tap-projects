import java.sql.*;

// (c) https://www.tutorialspoint.com/jdbc/jdbc-create-database.htm

public class JDBCExample{
    private static String jdbcDriver = "com.mysql.jdbc.Driver";
    
    private static final String DBNAME = "TIGER";// "Schema"

    
    private static String URL = "jdbc:mysql://localhost/"; 
    private static String ROOT_LOGIN = "root";
    private static String ROOT_PASS = "max13678";

	private static String USER = "TRIGGER_admin";
	private static String PASS = "123";
    
    ////// PRIMEIRA EXECU��O DO PROGRAMA:
    /// cria o banco, se j� existe, ent�o cancela as pr�ximas opera��es
    /// cria o usuario com permiss�es para usar o banco criado
    /// cria as tabelas necess�rias
	private static final String CMD_CRIAR = "CREATE TABLE personagens (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,apelido VARCHAR(20),nome VARCHAR(50),filme VARCHAR(100))";
    private static final String CMD_criarUsuario = "GRANT ALL PRIVILEGES ON ?.* TO ?@'localhost' IDENTIFIED BY ? WITH GRANT OPTION";//dbname,username,password
  

    private static int criarBancoDeDados(){
		try {
			
			Class.forName(jdbcDriver);
//			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/?user="+ROOT_LOGIN+"&password="+ROOT_PASS+"&useSSL=false");
			Connection con = DriverManager.getConnection(URL+"?useSSL=false", ROOT_LOGIN, ROOT_PASS);
			
			Statement st = con.createStatement();
			return st.executeUpdate("CREATE DATABASE "+DBNAME); // 0,1 ou 2 
			
		} catch (ClassNotFoundException | SQLException e) {
			/// BD J� EXISTE, i.e., TUDO CERTO PARA O PROGRAMA
			return ( e.getMessage().contains("exist") ) ? 3 : -1; 
		}
    }
    
    
    private static boolean criarUsuario(){
    	try{
			Connection con = DriverManager.getConnection(URL+"?useSSL=false", ROOT_LOGIN, ROOT_PASS);
			
			String comando = CMD_criarUsuario.replaceFirst("\\?", DBNAME);
			PreparedStatement pst = con.prepareStatement(comando);
			pst.setString(1, USER);
			pst.setString(2, PASS);
			int result = pst.executeUpdate();
			
			System.out.println("criarUsuario:"+result);
			return ( result >= 0 ); 
    	} catch(SQLException e){
    		return false;
    	}
    }

    private static boolean criarTabelas(){
    	try{
			Connection con = DriverManager.getConnection(URL+DBNAME+"?useSSL=false", USER, PASS);
			
			Statement st = con.createStatement();
			int result = st.executeUpdate(CMD_CRIAR);
			
			System.out.println("criarTabelas:"+result);
			return ( result >= 0 ); 
    	} catch(SQLException e){
    		return (e.getMessage().contains("exists"));
    	}
    }
    
    
    /**
     * Cria um banco e as tabelas que ser�o utilizadas pelo programa.</br>
     * � necess�rio do login e senha de um usu�rio com permiss�es avan�adas no BD.
     * @return
     */
    public static boolean iniciarBancoDeDados(){
    	int result = criarBancoDeDados();
    	System.out.println("criarBancoDeDados:"+result);
    	if(result == 3) return true; /// BD j� existe
    	return (result >= 0) && criarUsuario() && criarTabelas();
    }
    
    
    public static void main(String[] args) {
		System.out.println( iniciarBancoDeDados() );
	}
}

/*
public class JDBCExample {
	// JDBC driver name and database URL
	// static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/";

	// Database credentials
	static final String USER = "username";
	static final String PASS = "password";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating database...");
			stmt = conn.createStatement();

			String sql = "CREATE DATABASE STUDENTS";
			stmt.executeUpdate(sql);
			System.out.println("Database created successfully...");
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Goodbye!");
	}// end main
}// end JDBCExample
*/