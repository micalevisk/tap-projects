import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.text.AbstractDocument;
import javax.swing.text.NumberFormatter;
import com.toedter.calendar.JDateChooser;
import extra.DocumentSizeFilter;
import extra.JTextFieldComBrancos;
import extra.KeyLivro;
import extra.dataValida;
import model.bean.Config;
import model.bean.Livro;
import model.bean.Usuario;
import extra.Constantes;
import java.awt.event.KeyAdapter;
/**
 * θ - para exibir detalhes (editáveis) de um título selecionado previamente	[OITAVA dJANELA]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 27 de dez de 2016
 */
public class Detalhes_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private final Font lblFONTE = new Font("Ubuntu Light", Font.BOLD, 11);
	private final Font chxFONTE = new Font("Tahoma", Font.PLAIN, 12);

	private JLabel lblTituloJanela;
	private String textTitulo = "Editar Informações De ";
	private JTextFieldComBrancos txtNomeLivro;
	private String txtNomeLivro_placeholder = "digite aqui o nome do título";// *
	private JTextFieldComBrancos txtAutor;
	private String txtAutor_placeholder = "digite aqui o nome do(s) autor(es)";// *
	private JSpinner spnEdicao;// *

	private Vector<String> strCategorias = new Vector<String>();
	private JComboBox cbxCategoria;// *
	private JTextArea txtarDescricao;
	private JRadioButton rbtnEmprestado, rbtnFavorito, rbtnDesejado, rbtnComprado;

	private String strFormatoData = "dd/MM/yyyy";
	private SimpleDateFormat formatoData = new SimpleDateFormat(strFormatoData);
	private String textAquisicao = "Dia da Aquisição";
	private String textEmprestimo = "Dia do Empréstimo";
	private JLabel lblDiaDevolucao;
	private JLabel lblDiaAquisicao;
	JDateChooser dcrDiaAquisicao, dcrDiaDevolucao;

	private JButton btnSalvar;
	
	private Usuario usuarioLogado = null; // o 'usuarios_login' será crucial
	private Livro livro = null; // o livro selecionado; o 'id' será crucial
	private int indice; // o índice do livro na sua respectiva lista 
	private Config configsAtualizadas = null;
	//////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * Recupera os dados importantes para a tabela no BD.<br>
	 * KeyLivro.NOME: nome do título 	(String)<br>
	 * KeyLivro.AUTOR: autor do título	(String)<br>
	 * KeyLivro.EDICAO: edição do título	(String)<br>
	 * KeyLivro.DESCRICAO: descrição sobre o título	(String)<br>
	 * KeyLivro.CATEGORIA: a categoria escolhida		(String)<br>
	 * KeyLivro.EMPRESTADO: indica se o título é emprestado	(Boolean)<br>
	 * KeyLivro.DESEJADO: indica se o título é desejado		(Boolean)<br>
	 * KeyLivro.FAVORITO: indica se o título é favorito		(Boolean)<br>
	 * KeyLivro.AQUISICAO: dia de aquisição						(String)<br>
	 * KeyLivro.DEVOLUCAO: dia da devolução, se for emprestado	(String)<br>
	 * KeyLivro.TIPO: identificação do grupo do livro	(KeyLivro)
	 * @return A tabela Hash que contém todos os dados úteis com as chaves acima.
	 */
	private HashMap<KeyLivro, Object> getDadosUteis(){
		boolean ehEmprestado = rbtnEmprestado.isSelected(); // para não ter que fazer a mesma pergunta duas vezes
		return new HashMap<KeyLivro,Object>() 
		{{
			put(KeyLivro.NOME, txtNomeLivro.getText() );
			put(KeyLivro.AUTOR, txtAutor.getText() );
			put(KeyLivro.EDICAO, (Integer)spnEdicao.getValue());
			put(KeyLivro.DESCRICAO, txtarDescricao.getText() );
			put(KeyLivro.CATEGORIA, cbxCategoria.getSelectedItem().toString().toLowerCase() );
			put(KeyLivro.EHEMPRESTADO, ehEmprestado );
			put(KeyLivro.EHDESEJADO, new Boolean(rbtnDesejado.isSelected()) );
			put(KeyLivro.EHFAVORITO, new Boolean(rbtnFavorito.isSelected()) );
			put(KeyLivro.AQUISICAO, DateFormat.getDateInstance().format(dcrDiaAquisicao.getDate()) );
			put(KeyLivro.DEVOLUCAO, (ehEmprestado) ? DateFormat.getDateInstance().format(dcrDiaDevolucao.getDate()) : null );
			put(KeyLivro.TIPO, tipoDoLivroCadastrado());
		}};
	}

	/**
	 * Carrega os dados dos parâmetros para os seus respectivos campos, dispostos nesta janela.
	 * @param nomeLivro
	 * @param autor
	 * @param numEdicao
	 * @param descricao
	 * @param categoria
	 * @param ehDesejado
	 * @param ehFavorito
	 * @param diaAquisicao
	 * @param diaDevolucao
	 */
	private void carregarDados(String nomeLivro,String autor,int numEdicao, String descricao,String categoria,boolean ehDesejado,boolean ehFavorito, String diaAquisicao, String diaDevolucao){
		textTitulo += "\""+nomeLivro+"\"";
		lblTituloJanela.setText(textTitulo);
		
		txtNomeLivro.setText(nomeLivro);
		txtAutor.setText(autor);
		spnEdicao.setValue(numEdicao);
		
		txtarDescricao.setText(descricao);
		cbxCategoria.setSelectedItem(categoria);
		rbtnDesejado.setSelected(ehDesejado);
		rbtnFavorito.setSelected(ehFavorito);
		
		try {
			Date data = formatoData.parse(diaAquisicao);
			dcrDiaAquisicao.setDate(data);
		} catch (ParseException e) {
			System.out.println("ERRO carregarDados "+e.getMessage());
		}
		if(diaDevolucao != null){ // pois é emprestado
			rbtnEmprestado.setSelected(true);
			lblDiaAquisicao.setText(textEmprestimo);
			lblDiaDevolucao.setEnabled(true);
			dcrDiaDevolucao.setEnabled(true);
			try {
				Date data = formatoData.parse(diaDevolucao);
				dcrDiaDevolucao.setDate(data);
			} catch (ParseException e) {
				System.out.println("ERRO carregarDados "+e.getMessage());
//				e.printStackTrace();
			}
		}
		
	}

	private void carregarDadosDoLivro(){
		carregarDados(livro.getNome(), livro.getAutor(), livro.getEdicao(), livro.getDescricao(), livro.getCategoria(), livro.isDesejado(), livro.isFavorito(), livro.getDia_aquisicao(), livro.getDia_devolucao());
	}
	
	
	public Detalhes_jdialog(Usuario usuarioLogado, Livro livro, int indice){
		this(usuarioLogado,livro,indice, null);
	}
	public Detalhes_jdialog(Usuario usuarioLogado, Livro livro, int indice, Frame parent){
		this(usuarioLogado,livro,indice, parent, true);
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public Detalhes_jdialog(Usuario usuarioLogado, Livro livro, int indice, Frame parent, boolean modal){
		super(parent, modal);
		
		this.usuarioLogado = usuarioLogado;
		this.livro = livro;
		this.indice = indice;
		this.configsAtualizadas = usuarioLogado.getConfigsUsuario();
		this.strCategorias = configsAtualizadas.getCategoriasComoVector();
		
		// para alterar o desenhdo da janela:
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		// inicializar dialog:
		initComponents();
		
		carregarDadosDoLivro();
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	private void initComponents() {
		setResizable(false);
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 509, 356);

		JScrollPane scrpnPrincipal = new JScrollPane();
		scrpnPrincipal.setBounds(0, 0, 434, 261);
		scrpnPrincipal.setBorder(null);
		getContentPane().add(scrpnPrincipal);

		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(new Color(245, 245, 245));
		pnlPrincipal.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(95, 113, 160)));
		pnlPrincipal.setLayout(null);
		scrpnPrincipal.setViewportView(pnlPrincipal);

		lblTituloJanela = new JLabel(textTitulo);
		lblTituloJanela.setEnabled(false);
		lblTituloJanela.setFont(new Font("Ubuntu Mono", Font.PLAIN, 14));
		lblTituloJanela.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloJanela.setBounds(22, 11, 457, 21);
		pnlPrincipal.add(lblTituloJanela);

		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		CheckTextField alternarBotaoEntrar = new CheckTextField();

		////////////////////////////////////////////[ SOBRE O TÍTULO ]////////////////////////////////////////////
		JPanel pnlSobreOTitulo = new JPanel();
		pnlSobreOTitulo.setForeground(UIManager.getColor("Button.foreground"));
		pnlSobreOTitulo.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "<HTML><B>sobre o título</B></HTML>", TitledBorder.RIGHT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlSobreOTitulo.setBackground(new Color(245, 245, 245));
		pnlSobreOTitulo.setBounds(22, 36, 281, 111);
		pnlPrincipal.add(pnlSobreOTitulo);
		pnlSobreOTitulo.setLayout(null);

		///////////////////[ RÓTULOS ]///////////////////
		JLabel lblNomeDoLivro = new JLabel("Nome do Título");
		lblNomeDoLivro.setHorizontalAlignment(SwingConstants.LEFT);
		lblNomeDoLivro.setFont(lblFONTE);
		lblNomeDoLivro.setBounds(10, 11, 256, 14);
		pnlSobreOTitulo.add(lblNomeDoLivro);

		JLabel lblAutor = new JLabel("Autor(es)");
		lblAutor.setHorizontalAlignment(SwingConstants.LEFT);
		lblAutor.setFont(lblFONTE);
		lblAutor.setBounds(88, 58, 178, 14);
		pnlSobreOTitulo.add(lblAutor);

		JLabel lblEdicao = new JLabel("Edição");
		lblEdicao.setVerticalAlignment(SwingConstants.TOP);
		lblEdicao.setHorizontalAlignment(SwingConstants.LEFT);
		lblEdicao.setFont(lblFONTE);
		lblEdicao.setBounds(10, 58, 43, 14);
		pnlSobreOTitulo.add(lblEdicao);
		

		//////////////////////////////////////////[ DATAS ]//////////////////////////////////////////
		JPanel pnlDatas = new JPanel();
		pnlDatas.setBackground(new Color(245, 245, 245));
		pnlDatas.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "<HTML><B>datas</B></HTML>", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlDatas.setBounds(313, 36, 166, 111);
		pnlPrincipal.add(pnlDatas);
		pnlDatas.setLayout(null);

		lblDiaAquisicao = new JLabel(textAquisicao);
		lblDiaAquisicao.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiaAquisicao.setFont(lblFONTE);
		lblDiaAquisicao.setBounds(10, 14, 146, 14);
		pnlDatas.add(lblDiaAquisicao);

		dcrDiaAquisicao = new JDateChooser(new Date());
		dcrDiaAquisicao.setBorder(null);
		dcrDiaAquisicao.setDateFormatString(strFormatoData);
		dcrDiaAquisicao.setBounds(10, 29, 146, 20);
		dcrDiaAquisicao.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent e){
				toggleBtnSalvar();
			}
		});
		pnlDatas.add(dcrDiaAquisicao);

		lblDiaDevolucao = new JLabel("Dia da Devolução");
		lblDiaDevolucao.setEnabled(false);
		lblDiaDevolucao.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiaDevolucao.setFont(lblFONTE);
		lblDiaDevolucao.setBounds(10, 65, 146, 14);
		pnlDatas.add(lblDiaDevolucao);

		dcrDiaDevolucao = new JDateChooser();
		dcrDiaDevolucao.setEnabled(false);
		dcrDiaDevolucao.setBorder(null);
		dcrDiaDevolucao.setDateFormatString(strFormatoData);
		dcrDiaDevolucao.setToolTipText("se o livro for emprestado");
		dcrDiaDevolucao.setBounds(10, 80, 146, 20);
		dcrDiaDevolucao.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent e){
				toggleBtnSalvar();
			}
		});
		pnlDatas.add(dcrDiaDevolucao);
		
		/////////////////////////////////////[ BOTÕES PRINCIPAIS  ]/////////////////////////////////////
		btnSalvar = new JButton("Salvar");
		btnSalvar.setActionCommand("_SALVAR");
		btnSalvar.setEnabled(false);
		btnSalvar.setIcon(new ImageIcon(Constantes.PATH_ICONS + "tick.png"));
		btnSalvar.setMnemonic(KeyEvent.VK_S);
		btnSalvar.addActionListener(acaoBotao);
		btnSalvar.setCursor(Constantes.CURSOR_BOTOES);
		btnSalvar.setForeground(new Color(0, 128, 0));
		btnSalvar.setFont(new Font("Tw Cen MT", Font.PLAIN, 14));
		btnSalvar.setToolTipText("salvar alterações");
		btnSalvar.setBounds(353, 166, 126, 128);
		pnlPrincipal.add(btnSalvar);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setActionCommand("_CANCELAR");
		btnCancelar.setIcon(new ImageIcon(Constantes.PATH_ICONS + "cancel.png"));
		btnCancelar.setMnemonic(KeyEvent.VK_R);
		btnCancelar.addActionListener(acaoBotao);
		btnCancelar.setCursor(Constantes.CURSOR_BOTOES);
		btnCancelar.setForeground(new Color(128, 0, 0));
		btnCancelar.setToolTipText("fechar");
		btnCancelar.setFont(new Font("Tw Cen MT", Font.PLAIN, 14));
		btnCancelar.setBounds(353, 305, 126, 43);
		pnlPrincipal.add(btnCancelar);

		////////////////////////[ CAMPOS ]////////////////////////
		txtNomeLivro = new JTextFieldComBrancos(txtNomeLivro_placeholder);
		txtNomeLivro.setBounds(10, 25, 256, 26);
		txtNomeLivro.setBorder(Constantes.brITEM_OBRIGATORIO);
		txtNomeLivro.addCaretListener(alternarBotaoEntrar);
		pnlSobreOTitulo.add(txtNomeLivro);
		txtNomeLivro.setColumns(10);

		txtAutor = new JTextFieldComBrancos(txtAutor_placeholder);
		txtAutor.setColumns(10);
		txtAutor.setBorder(Constantes.brITEM_OBRIGATORIO);
		txtAutor.addCaretListener(alternarBotaoEntrar);
		txtAutor.setBounds(88, 74, 178, 26);
		pnlSobreOTitulo.add(txtAutor);

		spnEdicao = new JSpinner();
		spnEdicao.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spnEdicao.setBounds(10, 74, 43, 26);
		// apenas números
		JFormattedTextField txtonlynumbers = ((JSpinner.NumberEditor) spnEdicao.getEditor()).getTextField();
		((NumberFormatter) txtonlynumbers.getFormatter()).setAllowsInvalid(false);
		// texto centralizado
		JComponent editor = spnEdicao.getEditor();
		JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor) editor;
		spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		pnlSobreOTitulo.add(spnEdicao);

		/////////////////////////////////////[ GERAL ]/////////////////////////////////////
		JPanel pnlDetalhes = new JPanel();
		pnlDetalhes.setToolTipText("informações gerais");
		pnlDetalhes.setBackground(new Color(245, 245, 245));
		pnlDetalhes.setBorder(new TitledBorder(null, "<HTML><B>geral</B></HTML>", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDetalhes.setBounds(22, 158, 321, 190);
		pnlPrincipal.add(pnlDetalhes);
		pnlDetalhes.setLayout(null);

		JScrollPane scrpnlBoxParaDescricao = new JScrollPane();
		scrpnlBoxParaDescricao.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrpnlBoxParaDescricao.setBounds(10, 22, 148, 157);
		pnlDetalhes.add(scrpnlBoxParaDescricao);

		txtarDescricao = new JTextArea();
		txtarDescricao.setFont(new Font("Nirmala UI", Font.PLAIN, 12));
		txtarDescricao.setBackground(new Color(255, 255, 255));
		txtarDescricao.setLineWrap(true);
		txtarDescricao.setWrapStyleWord(true);
		txtarDescricao.setToolTipText("escreva algo sobre este título (ex: editora, resumo, etc)");
		scrpnlBoxParaDescricao.setViewportView(txtarDescricao);
		// limitando quantidade de caracteres:
		AbstractDocument pDoc = (AbstractDocument)txtarDescricao.getDocument();
		pDoc.setDocumentFilter(new DocumentSizeFilter(Constantes.MAX_CHARS_DESC));

		JLabel lblDescricao = new JLabel("Descrição");
		lblDescricao.setBackground(new Color(245, 245, 245));
		lblDescricao.setFont(lblFONTE);
		lblDescricao.setHorizontalAlignment(SwingConstants.CENTER);
		scrpnlBoxParaDescricao.setColumnHeaderView(lblDescricao);

		cbxCategoria = new JComboBox(strCategorias);
		cbxCategoria.setToolTipText("aperte ENTER para confirmar");
		cbxCategoria.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					toggleBtnSalvar();
				}
			}
		});
		cbxCategoria.setEditable(true);
		// cbxCategoria.setModel(new DefaultComboBoxModel(strCategorias));
		cbxCategoria.setBorder(Constantes.brITEM_OBRIGATORIO);
		cbxCategoria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbxCategoria.setSelectedIndex(0);
		cbxCategoria.setBounds(168, 22, 143, 29);
		pnlDetalhes.add(cbxCategoria);
		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();
		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
		cbxCategoria.setRenderer(UIResource);

		//////////////////////////////[ RADIOBUTTONS ]//////////////////////////////
		JPanel pnlCheckboxes = new JPanel();
		pnlCheckboxes.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		pnlCheckboxes.setBackground(SystemColor.controlHighlight);
		pnlCheckboxes.setBounds(168, 62, 143, 117);
		pnlDetalhes.add(pnlCheckboxes);
		pnlCheckboxes.setLayout(null);

		rbtnEmprestado = new JRadioButton("Emprestado");
		rbtnEmprestado.setToolTipText("o título é emprestado?");
		rbtnEmprestado.setMnemonic(KeyEvent.VK_E);
		rbtnEmprestado.setActionCommand("_RADIOBUTTON");
		rbtnEmprestado.addActionListener(acaoBotao);
		rbtnEmprestado.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnEmprestado.setBounds(6, 6, 131, 23);
		rbtnEmprestado.setBackground(null);
		rbtnEmprestado.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnEmprestado);

		rbtnDesejado = new JRadioButton("Desejado");
		rbtnDesejado.setToolTipText("adicioná-lo na lista de desejos?");
		rbtnDesejado.setMnemonic(KeyEvent.VK_D);
		rbtnDesejado.setActionCommand("_RADIOBUTTON");
		rbtnDesejado.addActionListener(acaoBotao);
		rbtnDesejado.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnDesejado.setBackground((Color) null);
		rbtnDesejado.setBounds(6, 33, 131, 23);
		rbtnDesejado.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnDesejado);

		rbtnFavorito = new JRadioButton("Favorito");
		rbtnFavorito.setToolTipText("é o seu título favorito?");
		rbtnFavorito.setMnemonic(KeyEvent.VK_F);
		rbtnFavorito.setActionCommand("_RADIOBUTTON");
		rbtnFavorito.addActionListener(acaoBotao);
		rbtnFavorito.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnFavorito.setBounds(6, 59, 131, 23);
		rbtnFavorito.setBackground(null);
		rbtnFavorito.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnFavorito);
		
		rbtnComprado = new JRadioButton("Apenas Comprado");
		rbtnComprado.setSelected(true);
		rbtnComprado.setMnemonic(KeyEvent.VK_A);
		rbtnComprado.setToolTipText("não é favorito");
		rbtnComprado.setActionCommand("_RADIOBUTTON");
		rbtnComprado.addActionListener(acaoBotao);
		rbtnComprado.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnComprado.setBounds(6, 87, 131, 23);
		rbtnComprado.setBackground(null);
		rbtnComprado.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnComprado);

		ButtonGroup grupoRadioButtons = new ButtonGroup();
		grupoRadioButtons.add(rbtnEmprestado);
		grupoRadioButtons.add(rbtnDesejado);
		grupoRadioButtons.add(rbtnFavorito);
		grupoRadioButtons.add(rbtnComprado);

	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private KeyLivro tipoDoLivroCadastrado(){
		return rbtnDesejado.isSelected() ? KeyLivro.DESEJADOS : 
				rbtnEmprestado.isSelected() ? KeyLivro.EMPRESTADOS :
					KeyLivro.COMPRADOS; // tratar verificar se é favorito também (pelo atributo do livro)
	}
	
	private boolean camposValidos() {
		boolean validoNome = !txtNomeLivro.textIsEmpty();
		boolean validoAutor = !txtAutor.textIsEmpty();
		boolean validoCategoria = !cbxCategoria.getSelectedItem().toString().isEmpty();
		boolean validoDiaAquisicao = dataValida.isValidDate(dcrDiaAquisicao, strFormatoData);
		boolean validoDiaDevolucao = (rbtnEmprestado.isSelected()) ? dataValida.isValidDate(dcrDiaDevolucao, strFormatoData) : true;

		txtNomeLivro.setBorder((validoNome) ? Constantes.brPADRAO : Constantes.brITEM_OBRIGATORIO);
		txtAutor.setBorder((validoAutor) ? Constantes.brPADRAO : Constantes.brITEM_OBRIGATORIO);
		cbxCategoria.setBorder((validoCategoria) ? null : Constantes.brITEM_OBRIGATORIO);

		return validoDiaAquisicao && validoDiaDevolucao && validoNome && validoAutor && validoCategoria ;
	}

	private void toggleBtnSalvar() {
		btnSalvar.setEnabled(camposValidos());
	}

	private void toggleDiaDevolucao() {
		boolean estaMarcado;
		if (estaMarcado = this.rbtnEmprestado.isSelected())
			this.lblDiaAquisicao.setText(textEmprestimo);
		else
			this.lblDiaAquisicao.setText(textAquisicao);
		
		this.lblDiaDevolucao.setEnabled(estaMarcado);
		this.dcrDiaDevolucao.setEnabled(estaMarcado);
		
		toggleBtnSalvar();
	}

	
	private void atualizarDadosLivro(){
		HashMap<KeyLivro, Object> dadosUteis = getDadosUteis();
		livro.setNome( (String) dadosUteis.get(KeyLivro.NOME) );
		livro.setAutor( (String) dadosUteis.get(KeyLivro.AUTOR) );
		livro.setDescricao( (String) dadosUteis.get(KeyLivro.DESCRICAO) );
		livro.setEdicao( (Integer) dadosUteis.get(KeyLivro.EDICAO) );
		livro.setCategoria( (String) dadosUteis.get(KeyLivro.CATEGORIA) );
		livro.setDesejado( (Boolean) dadosUteis.get(KeyLivro.EHDESEJADO) );
		livro.setFavorito( (Boolean) dadosUteis.get(KeyLivro.EHFAVORITO) );
		livro.setDia_aquisicao( (String) dadosUteis.get(KeyLivro.AQUISICAO) );
		livro.setDia_devolucao( (String) dadosUteis.get(KeyLivro.DEVOLUCAO) );
		livro.setTipo( (KeyLivro) dadosUteis.get(KeyLivro.TIPO) );
	}
	
	private void alterarCadastro() {
		if(!camposValidos()) return;//FIXME forçando checagem dupla (devido ao campo de categoria que não está checando no momento correto) 
		try{
			KeyLivro tipoAnterior = livro.getTipo();
			boolean ehFavoritoAnterior = livro.isFavorito();
			
			atualizarDadosLivro();
			
			KeyLivro novoTipo = livro.getTipo();
			boolean tipoAlterado = (tipoAnterior != novoTipo);

			boolean novoEhFavorito = livro.isFavorito();
			boolean favoritoAlterado = (novoEhFavorito != ehFavoritoAnterior);
			boolean inserirEmFavoritosTambem = false; /// INSERIR EM novoTipo E EM "FAVORITOS"
			boolean removerDeFavoritosTambem = false; /// REMOVER DE tipoAnterior E EM "FAVORITOS"
			
			/// PARA TRATAR O CASO ESPECIAL ONDE A LISTA DE "COMPRADOS" E "FAVORITOS" É MESMA (ou não)
			if(favoritoAlterado){
				if(novoEhFavorito)/// ñfav -> fav
					inserirEmFavoritosTambem = true;
				else /// fav -> ñfav
					removerDeFavoritosTambem = true;
			}

			/// ATUALIZANDO A LISTA RELACIONADA A ESTE LIVRO (SE NECESSÁRIO)
			if( (tipoAlterado) || (favoritoAlterado) ){
				HashMap<KeyLivro, List<Livro>> livrosUsuario = this.usuarioLogado.getLivrosUsuario();
				
				if(tipoAlterado){
					livrosUsuario.get(tipoAnterior).remove(indice); /// REMOVER DA LISTA ANTERIOR (pelo índice)
					livrosUsuario.get(novoTipo).add(livro); /// ADICIONAR NA LISTA CORRETA
				}
				
				if(removerDeFavoritosTambem) usuarioLogado.removerLivroComId(livro.getId(), KeyLivro.FAVORITOS);
				if(inserirEmFavoritosTambem) livrosUsuario.get(KeyLivro.FAVORITOS).add(livro);
			}
	
			/// ATUALIZAR NO BD
			if( livro.getDAO().atualizarLivroDoUsuario(this.usuarioLogado, this.livro) ){
				configsAtualizadas.getDAO().anexarNovaCategoria(configsAtualizadas, livro.getCategoria());
					
				this.dispose();
			}else{
				JOptionPane.showMessageDialog(null, "Algo deu erro ao alterar o cadastro", "Erro", JOptionPane.ERROR_MESSAGE);
			}
			
		}catch(NullPointerException ex){
			System.out.println("ERRO alterarCadastro() "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class CheckTextField implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent e) {
			toggleBtnSalvar();
		}
	}

	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand().substring(1);

			switch (comando) {
			case "RADIOBUTTON":	toggleDiaDevolucao();	break;
			case "SALVAR":	alterarCadastro();	break;
			case "CANCELAR": dispose();	break;

			default:
				throw new IllegalArgumentException("Invalid Command: " + comando);
			}
		}
	}
}
