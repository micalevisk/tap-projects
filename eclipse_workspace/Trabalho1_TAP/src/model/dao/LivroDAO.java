package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import connection.ConnectionFactory;
import extra.Constantes;
import extra.KeyLivro;
import model.bean.Livro;
import model.bean.Usuario;

/**
 * 	[tbl_livros]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class LivroDAO extends ConnectionFactory {

	// username, �Desejado, �Favorito, nome, autor, descri��o, edi��o, categoria, dia/da/aquisi��o, dia/da/devolu��o
	private static final String CMD_cadastrarLivro = "INSERT INTO" + Constantes.TBL_LIVROS + "VALUES ( NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
	private static final String CMD_mostrarLivrosUsuario = "SELECT * FROM"+ Constantes.TBL_LIVROS + "WHERE usuarios_login=?";// username
	private static final String CMD_mostrarLivrosUsuarioEAutor = "SELECT * FROM"+ Constantes.TBL_LIVROS + "WHERE usuarios_login=? AND autor LIKE ?";// username, autor
	private static final String CMD_atualizarLivroId = "UPDATE "+Constantes.TBL_LIVROS+" SET desejado=?, favorito=?, nome=?, autor=?, descricao=?, edicao=?, categoria=?, dia_aquisicao=?, dia_devolucao=? WHERE usuarios_login=? AND id=?";

	private static final String CMD_qtdLivrosAdquiridosUsuario = "SELECT COUNT(DISTINCT id) FROM "+Constantes.TBL_LIVROS+" WHERE desejado IS FALSE AND usuarios_login=?";// username
	private static final String CMD_qtdLivrosDesejadosUsuario = "SELECT COUNT(DISTINCT id) FROM "+Constantes.TBL_LIVROS+" WHERE desejado IS TRUE AND usuarios_login=?";// username

	private static final String CMD_mostrarLivrosCompradosUsuario = "SELECT * FROM"+ Constantes.TBL_LIVROS+ "WHERE desejado IS FALSE AND dia_devolucao IS NULL AND usuarios_login=?";// username
	private static final String CMD_mostrarLivrosEmprestadosUsuario = "SELECT * FROM "+Constantes.TBL_LIVROS+" WHERE desejado IS FALSE AND dia_devolucao IS NOT NULL AND usuarios_login=?";// username
	private static final String CMD_mostrarLivrosDesejadosUsuario = "SELECT * FROM "+Constantes.TBL_LIVROS+" WHERE desejado IS TRUE AND dia_devolucao IS NULL AND usuarios_login=?";// username

	private static final String CMD_mostrarRegistros = "SELECT * FROM" + Constantes.TBL_LIVROS; // retorna 11 colunas
	private static final String CMD_apagarRegistro = "DELETE FROM"+Constantes.TBL_LIVROS+"WHERE id=?";// id
	private static final String CMD_mostrarIDLivro = "SELECT id FROM"+Constantes.TBL_LIVROS+"WHERE usuarios_login=? AND nome=? AND autor=?";// username, nomeLivro, autorLivro


	////////////////////////////////////////////////////////[ �TIL ]////////////////////////////////////////////////////////
	
	/**
	 * Busca no BD pelo <b>login</b> e nome e autor do livro <b>l</b> retornando a coluna <b>id</b> do mesmo.
	 * @param login O username.
	 * @param l O livro alvo.
	 * @return <code>-1</code> se houve erro.
	 */
	public static int recuperarIdDoLivro(String login, Livro l){
		return recuperarIdDoLivro(new Usuario(login,""), l);
	}
	
	private static int recuperarIdDoLivro(Usuario u, Livro l){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarIDLivro);
			pst.setString(1, u.getLogin());
			pst.setString(2, l.getNome());
			pst.setString(3, l.getAutor());
			ResultSet rs = pst.executeQuery();

			return rs.next() ? rs.getInt(1) : -1; 
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarIdDoLivro "+ex.getMessage());
			return -1;
		}
	}

	/**
	 * Se o n�o usu�rio existe, ent�o cair� na exce��o e retorna <code>FALSE</code>.<br>
	 * Al�m de, se for inserido, o <b>l.id</b> ser� atualizado para o valor correto do <b>id</b>.s
	 * @param u Um usu�rio que j� existe no BD e seus livros foram carregados para {@link Usuario#getLivrosUsuario()}.
	 * @param l O novo livro.
	 * @return <code>TRUE</code> caso seja adicionado, i.e., sem c�pia de nome,autor e edi��o e o usu�rio existe.
	 */
	public static boolean adicionarLivro(Usuario u, Livro l){
		try{
			// verifica se j� existe tal livro
			if(u.livroEstaNaLista(l)) return false;
			// adiciona no BD
			PreparedStatement pst = conexao.prepareStatement(CMD_cadastrarLivro);
			pst.setString(1, u.getLogin());
			pst.setBoolean(2, l.isDesejado());
			pst.setBoolean(3, l.isFavorito());
			pst.setString(4, l.getNome());
			pst.setString(5, l.getAutor());
			pst.setString(6, l.getDescricao());
			pst.setInt(7, l.getEdicao());
			pst.setString(8, l.getCategoria());
			pst.setString(9, l.getDia_aquisicao());
			pst.setString(10, l.getDia_devolucao());
			pst.executeUpdate();
			
			/// atualiza ID do objeto Livro
			l.setId();
			
			return true;
		}catch(SQLException ex){
			System.out.println("� ERRO adicionarLivro " + ex.getMessage());
			return false;
		}
	}

	/**
	 * Filtra todos os livros associados a um {@linkplain Usuario#login} registrados no BD.
	 * @param u O usu�rio alvo.
	 * @return uma lista contendo todos os livros encontrados (ou zero livros) <b>ou</b> <code>NULL</code> se deu algum erro.
	 */
	public static List<Livro> recuperarLivrosDoUsuario(Usuario u){
		try{
			List<Livro> livros = new ArrayList<>();
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarLivrosUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();

			while(rs.next()){
				Livro l = new Livro(rs.getInt(1), rs.getString(2), rs.getBoolean(3), rs.getBoolean(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
				livros.add(l);
			}

			return livros;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarLivrosDoUsuario "+ ex.getMessage());
			return null;
		}
	}

	/**
	 * Filtar todos os livros associados a um {@linkplain Usuario#login} e autor no BD.
	 * @param u O usu�rio alvo.
	 * @param autor O nome do autor a ser buscado.
	 * @return Uma lista contendo todos os livros encontrados (ou zero livros) <b>ou</b> <code>NULL</code> se deu algum erro.
	 */
	public static List<Livro> recuperarLivrosDoUsuarioPorAutor(Usuario u, String autor){
		try{
			List<Livro> livros = new ArrayList<>();
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarLivrosUsuarioEAutor);
			pst.setString(1, u.getLogin());
			pst.setString(2, "%"+autor+"%");
			ResultSet rs = pst.executeQuery();

			while(rs.next()){
				Livro l = new Livro(rs.getInt(1), rs.getString(2), rs.getBoolean(3), rs.getBoolean(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
				livros.add(l);
			}

			return livros;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarLivrosDoUsuarioPorAutor "+ex.getMessage());
			return null;
		}
	}

	/**
	 * Atualiza todas as colunas de inform��o da tabela de livros associadas a um {@linkplain Usuario#login} e um {@linkplain Livro#id} (�nico) do livro.
	 * @param u O usu�rio alvo da altera��o.
	 * @param l O livro atualizado que ser� efetivado no BD.
	 * @return <code>TRUE</code> se n�o houve erros ao executar o comando SQL.
	 */
	public static boolean atualizarLivroDoUsuario(Usuario u, Livro l){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_atualizarLivroId);
			pst.setBoolean(1, l.isDesejado());
			pst.setBoolean(2, l.isFavorito());
			pst.setString(3, l.getNome());
			pst.setString(4, l.getAutor());
			pst.setString(5, l.getDescricao());
			pst.setInt(6, l.getEdicao());
			pst.setString(7, l.getCategoria());
			pst.setString(8, l.getDia_aquisicao());
			pst.setString(9, l.getDia_devolucao());
			pst.setString(10, l.getUsuarios_login());
			pst.setInt(11, l.getId());
			pst.executeUpdate();
			return true;
		}catch(SQLException ex){
			System.out.println("� ERRO atualizarLivroDoUsuario "+ex.getMessage());
			return false;
		}
	}

	/**
	 * Retorna a quantidade de livros (adiquiridos/emprestados) associados a um {@linkplain Usuario#login}.<br>
	 * Admite que o login existe pois o comando SQL n�o garante isso.
	 * @param u O usu�rio que ser� buscado no BD.
	 * @return Um n�mero <code>maior ou igual a 0</code> que ser� a quantidade de livros <b>OU</b> -1 indicando erro.
	 */
	public static int qtdLivrosAdquiridosDoUsuario(Usuario u){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_qtdLivrosAdquiridosUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();

			return rs.next() ? rs.getInt(1) : 0;
		}catch(SQLException ex){
			System.out.println("� ERRO qtdLivrosAdquiridosDoUsuario "+ex.getMessage());
			return -1;
		}
	}

	/**
	 * Retorna a quantidade de livros marcados como <i>desejado</i> associados a um {@linkplain Usuario#login}.<br>
	 * Admite que o login existe pois o comando SQL n�o garante isso.
	 * @param u O usu�rio que ser� buscado no BD.
	 * @return Um n�mero <code>&ge;0</code> que ser� a quantidade de livros encontrados <b>OU</b> <code>-1</code> indicando erro.
	 */
	public static int qtdLivrosDesejadosDoUsuario(Usuario u){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_qtdLivrosDesejadosUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();

			return rs.next() ? rs.getInt(1) : 0;
		}catch(SQLException ex){
			System.out.println("� ERRO qtdLivrosDesejadosDoUsuario "+ex.getMessage());
			return -1;
		}
	}

	/**
	 * Retorna uma lista de livros que n�o possuem data de devolu��o e n�o foram marcados como <i>desejado</i> no BD associadas a um usu�rio.
	 * @param u O usu�rio alvo da busca.
	 * @return Uma lista contendo todos os livros encontrados (ou zero livros) <b>ou</b> <code>NULL</code> se deu erro.
	 */
	private static List<Livro> recuperarLivrosCompradosDoUsuario(Usuario u){
		try{
			List<Livro> livros = new ArrayList<>();
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarLivrosCompradosUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				Livro l = new Livro(rs.getInt(1), rs.getString(2), rs.getBoolean(3), rs.getBoolean(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
				livros.add(l);
			}

			return livros;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarLivrosCompradosDoUsuario "+ex.getMessage());
			return null;
		}
	}

	/**
	 * Retorna uma lista de livros que possuem data de devolu��o no BD associadas a um usu�rio.
	 * @param u O usu�rio alvo da busca.
	 * @return Uma lista contendo todos os livros encontrados (ou zero livros) <b>ou</b> <code>NULL</code> se deu erro.
	 */
	private static List<Livro> recuperarLivrosEmprestadosDoUsuario(Usuario u){
		try{
			List<Livro> livros = new ArrayList<>();
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarLivrosEmprestadosUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();

			while(rs.next()){
				Livro l = new Livro(rs.getInt(1), rs.getString(2), rs.getBoolean(3), rs.getBoolean(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
				livros.add(l);
			}

			return livros;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarLivrosEmprestadosDoUsuario "+ex.getMessage());
			return null;
		}
	}

	/**
	 * Retorna uma lista de livros marcados como <i>desejado</i> associados a um {@linkplain Usuario#login}.
	 * @param u O usu�rio alvo da busca.
	 * @return Uma lista contendo todos os livros encontrados (ou zero livros) <b>ou</b> <code>NULL</code> se deu erro.
	 */
	private static List<Livro> recuperarLivrosDesejadosDoUsuario(Usuario u){
		try{
			List<Livro> livros = new ArrayList<>();
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarLivrosDesejadosUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();

			while(rs.next()){
				Livro l = new Livro(rs.getInt(1), rs.getString(2), rs.getBoolean(3), rs.getBoolean(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
				livros.add(l);
			}

			return livros;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarLivrosDesejadosDoUsuario "+ex.getMessage());
			return null;
		}
	}

	/**
	 * Retorna uma rela��o contendo todos os livros de um usu�rio no BD. Com as <b>chaves</b>:<br>
	 * KeyLivro.COMPRADOS<br>
	 * KeyLivro.EMPRESTADOS<br>
	 * KeyLivro.DESEJADOS<br>
	 * KeyLivro.FAVORITOS
	 * @param u O usu�rio alvo da busca.
	 * @return Uma tabela com as chaves citadas acima.
	 */
	public static HashMap<KeyLivro, List<Livro>> recuperarLivrosCadastradosDoUsuario(Usuario u){
		HashMap<KeyLivro, List<Livro>> livros = new HashMap<>();
		List<Livro> livrosComprados = recuperarLivrosCompradosDoUsuario(u);

		livros.put(KeyLivro.COMPRADOS, livrosComprados);
		livros.put(KeyLivro.EMPRESTADOS, recuperarLivrosEmprestadosDoUsuario(u));
		livros.put(KeyLivro.DESEJADOS, recuperarLivrosDesejadosDoUsuario(u));
		livros.put(KeyLivro.FAVORITOS, livrosComprados.stream()
										.filter(l -> l.isFavorito())
										.collect(Collectors.toList())); //java8
		return livros;
	}

	/**
	 * Retorna uma lista de todos os livros contidos no BD.
	 * @return A lista com zero elementos <b>ou</b> <code>NULL</code> se deu erro.
	 */
	public static List<Livro> recuperarLivrosCadastrados(){
		try{
			List<Livro> livros = new ArrayList<>();
			Statement st = conexao.createStatement();
			ResultSet rs = st.executeQuery(CMD_mostrarRegistros);

			while(rs.next()){
				Livro l = new Livro(rs.getInt(1), rs.getString(2), rs.getBoolean(3), rs.getBoolean(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11));
				livros.add(l);
			}

			return livros;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarLivrosCadastrados "+ex.getMessage());
			return null;
		}
	}

	/**
	 * Tenta deletar a linha que cont�m o <b>id</b> do livro passado, do BD.
	 * @param l O livro que ser� apagado.
	 * @return <code>TRUE</code> se apagou com sucesso.
	 */
	public static boolean apagarLivroUnico(Livro l){
		return apagarLivroUnico(l.getId());
	}

	/**
	 * Tenta deletar a linha que cont�m o <b>id</b> passado, do BD.
	 * @param idLivro O id �nico definido no BD.
	 * @return <code>TRUE</code> se apagou com sucesso.
	 */
	public static boolean apagarLivroUnico(int idLivro){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_apagarRegistro);
			pst.setInt(1, idLivro);
			pst.executeUpdate();
			return true;
		}catch(SQLException ex){
			System.out.println("� ERRO apagarLivroUnico "+ex.getMessage());
			return false;
		}
	}
	
	
	/*
	private static void exportarTabelaDeLivrosDoUsuario(Usuario u){
		JFileChooser fcrExportar = new JFileChooser();
		fcrExportar.setDialogTitle("Escolha um local para salvar a tabela SQL");
		fcrExportar.setFileSelectionMode(JFileChooser.FILES_ONLY); // seleciona apenas arquivos
		fcrExportar.setAcceptAllFileFilterUsed(false); // desabilita a op��o de "todas as extens�es"
		fcrExportar.setFileFilter(new FileTypeFilter("csv", "Comma-Separated Values"));
		
		int userSelection = fcrExportar.showSaveDialog(null);
		if (userSelection == JFileChooser.APPROVE_OPTION) {
			FileTypeFilter filtroSelecionado = (FileTypeFilter)fcrExportar.getFileFilter();
			File fileToSave = fcrExportar.getSelectedFile();
		    String filePath = fileToSave.getAbsolutePath().toLowerCase();

		    if(!(filePath.endsWith("csv"))){
		    	filePath += filtroSelecionado.getExtension();
		    	fileToSave = new File(filePath);
		    }

		    
		    String filename = fileToSave.getAbsolutePath();
	        try {
	            FileWriter fw = new FileWriter(filename);
				Statement st = conexao.createStatement();
				ResultSet rs = st.executeQuery("SELECT * FROM "+ Constantes.TBL_LIVROS);
	            while (rs.next()) {
	                fw.append(rs.getString(1));
	                fw.append(',');
	                fw.append(rs.getString(2));
	                fw.append(',');
	                fw.append(rs.getString(3));
	                fw.append(',');
	                fw.append(rs.getString(4));
	                fw.append(',');
	                fw.append(rs.getString(5));
	                fw.append(',');
	                fw.append(rs.getString(6));
	                fw.append(',');
	                fw.append(rs.getString(7));
	                fw.append(',');
	                fw.append(rs.getString(8));
	                fw.append(',');
	                fw.append(rs.getString(9));
	                fw.append(',');
	                fw.append(rs.getString(10));
	                fw.append(',');
	                fw.append(rs.getString(11));
	                fw.append('\n');
	            }
	            fw.flush();
	            fw.close();

	            System.out.println("CSV File is created successfully.");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
        
		}
        
	}
	*/
	
}
