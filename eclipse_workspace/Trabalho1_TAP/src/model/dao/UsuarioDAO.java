//TODO	atualizar senha; apagar usu�rio
package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import connection.ConnectionFactory;
import extra.Constantes;
import extra.KeyUsuario;
import extra.RetornosLogin;
import model.bean.Usuario;

/**
 * 	[tbl_usuarios]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class UsuarioDAO extends ConnectionFactory {
	
	private static final String CMD_criarUsuario = "INSERT INTO"+ Constantes.TBL_USUARIOS +"VALUES(?,?)"; // username, userpass
	private static final String CMD_senhaUsuario = "SELECT senha FROM"+ Constantes.TBL_USUARIOS +"WHERE login=?"; // username
	private static final String CMD_mostrarCredenciais = "SELECET * FROM" + Constantes.TBL_USUARIOS + "WHERE login=?"; // username
	private static final String CMD_quantidadeUsuarios = "SELECT COUNT(login) FROM" + Constantes.TBL_USUARIOS;
	private static final String CMD_quantidadesGeral = "SELECT COUNT(DISTINCT id), COUNT(DISTINCT (CASE WHEN favorito IS TRUE THEN id END)), COUNT(DISTINCT (CASE WHEN desejado IS TRUE THEN id END)), COUNT(DISTINCT dia_devolucao) FROM "+Constantes.TBL_LIVROS;
	
	private String senhaUsuario(Usuario u){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_senhaUsuario);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();
			
			return rs.next() ? rs.getString(1) : null;			
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
			return null;
		}
	}

	
	////////////////////////////////////////////////////////[ ÚTIL ]////////////////////////////////////////////////////////
	
	/**
	 * Se o usuário existe, se existir então cairá na exce��o e retorna <code>FALSE</code>.
	 * @param u Usuário alvo.
	 * @return <code>TRUE</code> caso seja criado com sucesso.
	 */
	public boolean criarUsuario(Usuario u){
		PreparedStatement pst = null;
		try{
			pst = conexao.prepareStatement(CMD_criarUsuario);
			pst.setString(1, u.getLogin());
			pst.setString(2, u.getSenha());
			pst.executeUpdate();
			
			return true;
		}catch(SQLException ex){
//			System.out.println("§ ERRO criarUsuario " + ex.getMessage());
			return false;
		}
	}
	
	/**
	 * Cria um {@link Usuario} a partir de um login que existe no BD.
	 * @param login O nick que ser� buscado.
	 * @return O usuário criado ou <code>NULL</code> se não houver cadastro com este <b>login</b>.
	 */
	public Usuario recuperarUsuarioComLogin(String login){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarCredenciais);
			pst.setString(1, login);
			ResultSet rs = pst.executeQuery();
			
			return (rs.next()) ? new Usuario(rs.getString(1), rs.getString(2)) : null; 
		}catch(SQLException ex){
//			System.out.println("§ ERRO recuperarUsuarioComLogin " +ex.getMessage());
			return null;
		}
	}
	
	/**
	 * Verifica se o login e senha do usuário est�o de acordo com o banco.
	 * @param u Usuário alvo.
	 * @return {@link RetornosLogin#TUDO_CERTO}, {@link RetornosLogin#LOGIN_NAO_EXISTE} ou {@link RetornosLogin#SENHA_INVALIDA} 
	 */
	public RetornosLogin checarCredenciais(Usuario u){
		String senhaBD = senhaUsuario(u); // recupera a senha associado ao u.login
		if(senhaBD == null) return RetornosLogin.LOGIN_NAO_EXISTE;
		if(!senhaBD.equals(u.getSenha())) return RetornosLogin.SENHA_INVALIDA;
		return RetornosLogin.TUDO_CERTO;
	}
	
	public boolean fecharConexao(){
		return desconectar();
	}

	/**
	 * Retorna uma rela��o da vis�o geral do BD. com as <b>chaves</b>:<br>
	 * KeyLivro.QTD_USUARIOS: quantidade total de usu�rios cadastrados.<br>
	 * KeyLivro.QTD_LIVROS: quantidade total de livros registrados.<br>
	 * KeyLivro.QTD_FAVORITOS: quantidade total de livros favoritados.<br>
	 * KeyLivro.QTD_DESEJADOS: quantidade total de livros na lista de desejos.<br>
	 * KeyLivro.QTD_EMPRESTIMOS: quantidade total de livros emprestados.
	 * @return Uma tabela com as chaves citadas acima.
	 */
	public static HashMap<KeyUsuario, Integer> visaoGeral(){
		try{
			Statement st = conexao.createStatement();
			ResultSet rs = st.executeQuery(CMD_quantidadesGeral);
			st = conexao.createStatement();
			ResultSet rs2 = st.executeQuery(CMD_quantidadeUsuarios);
			if(!rs.next() || !rs2.next()) return null;
			
			return new HashMap<KeyUsuario, Integer>() 
			{{
				put(KeyUsuario.QTD_USUARIOS, rs2.getInt(1));
				put(KeyUsuario.QTD_LIVROS,  rs.getInt(1));
				put(KeyUsuario.QTD_FAVORITOS, rs.getInt(2));
				put(KeyUsuario.QTD_DESEJADOS,  rs.getInt(3));
				put(KeyUsuario.QTD_EMPRESTIMOS,  rs.getInt(4));
			}};
			
		}catch(SQLException ex){
//			System.out.println("§ ERRO visaoGeral "+ex.getMessage());
			return null;
		}
	}
}
