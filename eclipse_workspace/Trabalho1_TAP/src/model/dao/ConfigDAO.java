package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import connection.ConnectionFactory;
import extra.Constantes;
import model.bean.Config;
import model.bean.Usuario;

/**
 * 	[tbl_configs]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class ConfigDAO extends ConnectionFactory {

	private static final String CMD_criarConfig = "INSERT INTO "+ Constantes.TBL_CONFIGS +"  VALUES(?,?)"; // username, categorias
	private static final String CMD_mostrarConfig = "SELECT * FROM"+Constantes.TBL_CONFIGS+"WHERE usuarios_login=?"; // username
//	private static final String CMD_anexarCategoria = "UPDATE " + Constantes.TBL_CONFIGS + " SET categorias=CONCAT(categorias, ?) WHERE usuarios_login=?"; // ,novaCategoria, username
	private static final String CMD_anexarCategoria = "UPDATE " + Constantes.TBL_CONFIGS + " SET categorias=? WHERE usuarios_login=?"; // ,novaCategoria, username

	private boolean categoriaExiste(String categorias, String novaCategoria){
		return Arrays.asList( categorias.split(",") ).contains(novaCategoria);
	}


	////////////////////////////////////////////////////////[ �TIL ]////////////////////////////////////////////////////////

	/**
	 * Verifica se o usu�rio existe, se existir ent�o retorna <i>FALSE</i>, caso contr�rio, insere na tabela de configs com a configura��o passada.
	 * @param c A configura��o do usu�rio alvo.
	 * @return <code>TRUE</code> caso seja criado com sucesso, i.e., existe um usu�rio com o mesmo login no BD e este n�o possua configura��es inicializadas ainda.
	 */
	public boolean criarConfig(Config c){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_criarConfig);
			pst.setString(1, c.getUsuarios_login());
			pst.setString(2, c.getCategorias());
			pst.executeUpdate();

			return true;
		}catch(SQLException ex){
			System.out.println("� ERRO criarConfig " + ex.getMessage());
			return false;
		}
	}

	/**
	 * Forma uma {@link Config} baseada na tabela relacionada ao usu�rio passado, no BD.
	 * @param u O usu�rio alvo
	 * @return <code>NULL</code> se n�o existir configura��es para este usu�rio no BD.
	 */
	public Config recuperarConfigsDoUsuario(Usuario u){
		try{
			PreparedStatement pst = conexao.prepareStatement(CMD_mostrarConfig);
			pst.setString(1, u.getLogin());
			ResultSet rs = pst.executeQuery();

			return rs.next() ? new Config(rs.getString(1), rs.getString(2)) : null;
		}catch(SQLException ex){
			System.out.println("� ERRO recuperarConfigsDoUsuario " + ex.getMessage());
			return null;
		}
	}

	/**
	 * Verifica se � uma categoria nova, se for, ent�o atualiza no BD. E atualiza o atributo de <b>categorias</b> passado.
	 * @param c A configura��o do usu�rio alvo.
	 * @param novaCategoria O nome da categoria a ser anexada.
	 * @return <code>TRUE</code> se for (ou n�o) uma categoria nova.
	 */
	public boolean anexarNovaCategoria(Config c, String novaCategoria){
		try{
			String categoriasAtuais = c.getCategorias();
			novaCategoria = novaCategoria.toLowerCase().replaceAll(",", "").trim(); // conven��o adotada = min�sculas, sem espa�os extras e sem v�rgulas.
			if( !categoriaExiste(categoriasAtuais, novaCategoria) ){
				PreparedStatement pst = conexao.prepareStatement(CMD_anexarCategoria);
//				pst.setString(1, ","+novaCategoria);

				categoriasAtuais += ","+novaCategoria;
				pst.setString(1, categoriasAtuais);
				pst.setString(2, c.getUsuarios_login());
				pst.executeUpdate();

				c.setCategorias(categoriasAtuais);
			}

			return true;
		}catch(SQLException ex){
			System.out.println("� ERRO anexarNovaCategoria " + ex.getMessage());
			return false;
		}
	}
}
