package model.bean;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.HashMap;

import extra.Constantes;
import extra.KeyLivro;
import extra.RetornosLogin;
import model.dao.LivroDAO;
import model.dao.UsuarioDAO;


/**
 * Cont�m a liga��o inicial (principal) entre usu�rio (�nico) e sua senha registrada.<br>
 * Carrega todos os dados, relacionados ao usu�rio indicado, que est�o armazenados no BD.<br>
 * � a ponte de conex�o com o <i>Data Access Objects</i> (DAO).
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class Usuario {

	private String login;// PK NN
	private String senha;// NN

	private Config configsUsuario;
	private HashMap<KeyLivro, List<Livro>> livrosUsuario; // para armazenar a lista de livros: COMPRADOS, EMPRESTADOS, DESEJADOS e FAVORITOS

	private static UsuarioDAO usuarioDAO = new UsuarioDAO();


	/**
	 * Inicializa um {@link Usuario} com associa��es direta ao BD mas sem conect�-lo (carregar dados �teis).
	 * @param login
	 * @param senha
	 */
	public Usuario(String login, String senha){
		this.login = login;
		this.senha = senha;

		this.configsUsuario = new Config(login, Constantes.CATEGORIAS_DEFAULT);
		this.livrosUsuario = null; // sem livros
	}


	/**
	 * Verifica se um livro (com nome e autor) est� na lista de livros do usu�rio, i.e., est� no BD.
	 * @param l
	 * @return
	 */
	public boolean livroEstaNaLista(Livro l){
		return
				this.livrosUsuario.get(KeyLivro.COMPRADOS).contains(l) ||
				this.livrosUsuario.get(KeyLivro.EMPRESTADOS).contains(l) ||
				this.livrosUsuario.get(KeyLivro.DESEJADOS).contains(l);
	}


	/**
	 * Consulta da lista de livros passada filtrando as que cont�m o nome passado.
	 * @param livrosAlvos
	 * @param autor
	 * @return
	 */
	public List<Livro> filtarLivrosAutor(List<Livro> livrosAlvos, String autor){
		return livrosAlvos.stream()
							.filter(l -> l.getAutor().contains(autor))
							.collect(Collectors.toList()); //java8
	}


	/**
	 * Tenta inserir um {@link Usuario} no <b>BD</b>.
	 * @return <code>TRUE</code> se ele foi inserido com sucesso, i.e.: n�o existe e foi inicializado em <i>tbl_usuarios</i> e em <i>tbl_configs</i>.
	 */
	public boolean cadastrarUsuario(){
		return Usuario.usuarioDAO.criarUsuario(this) && configsUsuario.getDAO().criarConfig(this.configsUsuario);
	}


	/**
	 * Verifica se o {@link Usuario#login} est� de acordo com a {@link Usuario#senha} definida no <b>BD</b><br>
	 * Admite que o usu�rio j� foi inserido no BD (e com configura��es inicializadas).<br>
	 * Se for realizado sem erros, ent�o atualiza todos os atributos do usu�rio.
	 * @return A resposta obtida ao tentar entrar (para tratar).
	 */
	public RetornosLogin fazerLogin(){
		RetornosLogin loginResponse = Usuario.usuarioDAO.checarCredenciais(this);

		if(loginResponse == RetornosLogin.TUDO_CERTO){ // carregar dados do BD.
			this.configsUsuario = configsUsuario.getDAO().recuperarConfigsDoUsuario(this);
			this.livrosUsuario = new LivroDAO().recuperarLivrosCadastradosDoUsuario(this);
		}

		return loginResponse;
	}


	/**
	 * Fecha a conex�o com o BD, se estiver conectado, e "perde" atribudos.
	 */
	public void fazerLogout(){
//		Usuario.usuarioDAO.fecharConexao();
		this.login = null;
		this.senha = null;
		this.configsUsuario = null;
		this.livrosUsuario = null;
	}



	public void adicionarLivroComprado(Livro l){
		this.livrosUsuario.get(KeyLivro.COMPRADOS).add(l);
	}

	public void adicionarLivroEmprestado(Livro l){
		this.livrosUsuario.get(KeyLivro.EMPRESTADOS).add(l);
	}

	public void adicionarLivroDesejado(Livro l){
		this.livrosUsuario.get(KeyLivro.DESEJADOS).add(l);
	}

	public boolean removerLivroComId(int id, KeyLivro tipo){
		try{
			int indice = getIndiceLivroComId(id, tipo);
			if(indice >= 0){
				this.livrosUsuario.get(tipo).remove( indice );
				return true;
			}
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("removerLivroComId "+e.getMessage());
		}
		return false;
	}


	/**
	 * Compara dois usu�rios pelo {@linkplain Usuario#login} e {@linkplain Usuario#senha}.
	 * @param o
	 * @return <code>TRUE</code> se forem iguais.
	 */
	@Override
	public boolean equals(Object o){
		if(o == this) return true;
		if(!(o instanceof Usuario)) return false;

		Usuario u = (Usuario)o;
		return u.login.equals(this.login) && u.senha.equals(this.senha);
	}


	///////////////////////////////////////// [ GETTERS ] /////////////////////////////////////////

	public UsuarioDAO getDAO(){
		return this.usuarioDAO;
	}

	public Config getConfigsUsuario(){
		return this.configsUsuario;
	}

	public HashMap<KeyLivro, List<Livro>> getLivrosUsuario(){
		return this.livrosUsuario;
	}

	/**
	 * Procura na lista do Usuario (filtrada pelo tipo) um t�tulo com o <b>id</b> (�nico) igual ao argumento passado.
	 * @param id
	 * @param tipo
	 * @return O �ndice (na lista respectiva filtrada pelo <i>tipo</i>) do t�tulo encontrado <b>ou</b>, <code>-1</code> caso n�o tenha encontrado.
	 */
	private int getIndiceLivroComId(int id, KeyLivro tipo){
		List<Livro> livros = this.livrosUsuario.get(tipo);

		OptionalInt idResponse = IntStream.range(0, livros.size()) //java8
								.filter(i -> livros.get(i).getId() == id)
								.findFirst();

		return idResponse.isPresent() ? idResponse.getAsInt() : -1;

		/* OBTER ID
		this.livrosUsuario.get(tipo).stream()
		.filter(l -> l.getId() == id)
		.findFirst()
		.get()
		.getId();
		*/
	}

	public String getLogin() {
		return this.login;
	}

	public String getSenha(){
		return this.senha;
	}

	public void atualizarUsuario(){
		this.fazerLogin();
	}

	///////////////////////////////////////// [ SETTERS ] /////////////////////////////////////////

	public void setConfigsUsuario(Config configAtualizada){
		this.configsUsuario = configAtualizada;
	}

	public void setListaLivrosUsuario(KeyLivro alvo, List<Livro> novaLista){
		this.livrosUsuario.put(alvo, novaLista);
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
