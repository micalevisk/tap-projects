package model.bean;

import extra.KeyLivro;
import model.dao.LivroDAO;

/**
 * Cont�m a rela��o final de usu�rio e seus livros com todas as informa��es sobre cada t�tulo.<br>
 * � a ponte de conex�o com o <i>Data Access Objects</i> (DAO).
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class Livro {

	private int id; // PK NN
	private String usuarios_login; // NN FK:Usuarios.login
	private boolean desejado ;// NN
	private boolean favorito;// NN
	private String nome; // NN
	private String autor; // NN
	private String descricao;
	private int edicao; // (1) NN
	private String categoria; // NN
	private String dia_aquisicao; // NN
	private String dia_devolucao;
	private KeyLivro tipo;

	private static LivroDAO livroDAO = new LivroDAO();


	private KeyLivro tipoDoLivro(){
		return this.desejado ? KeyLivro.DESEJADOS : 
				this.dia_devolucao != null ? KeyLivro.EMPRESTADOS :
						KeyLivro.COMPRADOS; // tratar verificar se � favorito tamb�m (pelo atributo do livro)
	}
	
	/**
	 * 11 campos correspondentes �s colunas no BD.
	 * @param id
	 * @param usuarios_login
	 * @param desejado
	 * @param favorito
	 * @param nome
	 * @param autor
	 * @param descricao
	 * @param edicao
	 * @param categoria
	 * @param dia_aquisicao
	 * @param dia_devolucao
	 */
	public Livro(int id, String usuarios_login, boolean desejado, boolean favorito, String nome, String autor,	String descricao, int edicao, String categoria, String dia_aquisicao, String dia_devolucao) {
		this(id,usuarios_login,desejado,favorito,nome,autor,descricao,edicao,categoria,dia_aquisicao,dia_devolucao,null);
	}
	/**
	 * @param id
	 * @param usuarios_login
	 * @param desejado
	 * @param favorito
	 * @param nome
	 * @param autor
	 * @param descricao
	 * @param edicao
	 * @param categoria
	 * @param dia_aquisicao
	 * @param dia_devolucao
	 * @param tipo
	 */
	public Livro(int id, String usuarios_login, boolean desejado, boolean favorito, String nome, String autor,	String descricao, int edicao, String categoria, String dia_aquisicao, String dia_devolucao, KeyLivro tipo) {
		this.id = id;
		this.usuarios_login = usuarios_login;
		this.desejado = desejado;
		this.favorito = favorito;
		this.nome = nome;
		this.autor = autor;
		this.descricao = descricao;
		this.edicao = edicao;
		this.categoria = categoria;
		this.dia_aquisicao = dia_aquisicao;
		this.dia_devolucao = dia_devolucao;
//		this.dia_devolucao = (dia_devolucao == null) ? "--" : dia_aquisicao;
		this.tipo = (tipo == null) ? tipoDoLivro() : tipo;
	}

	/**
	 * Tenta inserir um {@link Livro} no <b>BD</b>.
	 * @param u O us�rio que est� cadastrando o novo livro no BD.
	 * @return <code>TRUE</code> se ele foi inserido com sucesso, i.e.: n�o existe com mesmo nome e autor e foi inicializado em <i>tbl_usuarios</i>.
	 */
	public boolean cadastrarLivro(Usuario u){
		return Livro.livroDAO.adicionarLivro(u, this);
	}


	@Override
	public boolean equals(Object o){
		if(o == this) return true;
		if(!(o instanceof Livro)) return false;
		Livro l = (Livro) o;
		return l.nome.equals(this.nome) && l.autor.equals(this.autor) && (l.edicao == this.edicao);
	}

	@Override
	public String toString(){
		return String.format("[%d,%s]{nome:%s, autor:%s, descri��o:%s, edi��o:%d, categoria:%s, aquisi��o:%s, devolu��o:%s, � desejado?%s, � favorito?%s}",id,usuarios_login, nome,autor,descricao,edicao,categoria,dia_aquisicao,dia_devolucao,desejado,favorito);
	}

	///////////////////////////////////////// [ GETTERS ] /////////////////////////////////////////

	public LivroDAO getDAO(){
		return Livro.livroDAO;
	}

	public int getId() {
		return id;
	}

	public String getUsuarios_login() {
		return usuarios_login;
	}

	public boolean isDesejado() {
		return desejado;
	}

	public boolean isFavorito() {
		return favorito;
	}

	public String getNome() {
		return nome;
	}

	public String getAutor() {
		return autor;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getEdicao() {
		return edicao;
	}

	public String getCategoria() {
		return categoria;
	}

	public String getDia_aquisicao() {
		return dia_aquisicao;
	}

	public String getDia_devolucao() {
		return dia_devolucao;
	}

	public KeyLivro getTipo(){
		return this.tipo;
	}

	///////////////////////////////////////// [ SETTERS ] /////////////////////////////////////////

	public void setId(){
		this.id = livroDAO.recuperarIdDoLivro(this.usuarios_login, this);
	}

	public void setUsuarios_login(String usuarios_login) {
		this.usuarios_login = usuarios_login;
	}

	public void setDesejado(boolean desejado) {
		this.desejado = desejado;
	}

	public void setFavorito(boolean favorito) {
		this.favorito = favorito;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setEdicao(int edicao) {
		this.edicao = edicao;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public void setDia_aquisicao(String dia_aquisicao) {
		this.dia_aquisicao = dia_aquisicao;
	}

	public void setDia_devolucao(String dia_devolucao) {
		this.dia_devolucao = dia_devolucao;
	}

	public void setTipo(KeyLivro tipo){
		this.tipo = tipo;
	}


}
