package model.bean;

import java.util.Arrays;
import java.util.Vector;

import model.dao.ConfigDAO;

/**
 * Cont�m as liga��es secund�rias que registram configura��es pra cada usu�rio (�nico).<br>
 * � a ponte de conex�o com o <i>Data Access Objects</i> (DAO).
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class Config {

	private String usuarios_login; // PK NN FK:Usuarios.login
	private String categorias;

	private static ConfigDAO configDAO = new ConfigDAO();


	/**
	 * @param usuarios_login
	 * @param categoria
	 */
	public Config(String usuarios_login, String categoria){
		this.usuarios_login = usuarios_login;
		this.categorias = categoria;
	}


	@Override
	public boolean equals(Object o){
		if(o == this) return true;
		if(!(o instanceof Config)) return false;

		Config c = (Config)o;
		return c.usuarios_login.equals(this.usuarios_login) && c.categorias.equals(this.categorias);
	}

	@Override
	public String toString(){
		return String.format("[%s]{categorias:%s}", usuarios_login, categorias);
	}


	///////////////////////////////////////// [ GETTERS ] /////////////////////////////////////////

	public ConfigDAO getDAO(){
		return Config.configDAO;
	}

	public String getUsuarios_login() {
		return usuarios_login;
	}

	public String getCategorias() {
		return categorias;
	}

	public Vector<String> getCategoriasComoVector(){
		return new Vector<String>( Arrays.asList( categorias.split(",") ) );
	}

	///////////////////////////////////////// [ SETTERS ] /////////////////////////////////////////

	public void setUsuarios_login(String usuarios_login) {
		this.usuarios_login = usuarios_login;
	}

	public void setCategorias(String categorias) {
		this.categorias = categorias;
	}


}
