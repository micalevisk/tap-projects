package connection;

import java.sql.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;


/**
 * Conex�o direta com o JDBC
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 29 de dez de 2016
 */
public class ConnectionFactory extends BancoDeDados {

	public static Connection conexao = null;
	
	public ConnectionFactory(){
		super();//carrega os dados do arquivo XML
		/*
		System.out.printf("URL:%s\n", URL);
		System.out.printf("DBNAME:%s\n",DBNAME);
		System.out.printf("user:%s,%s\n\n",USER,PASS);
		*/
		if(URL!=null)
			if(ConnectionFactory.conexao == null) conectar();
	}
	
	
	
	/**
	 * Conecta ao banco de dados.
	 * @return <i>TRUE</i> se foi conectado com sucesso.
	 */
	private static boolean conectar(){
		try{
			conexao = DriverManager.getConnection(URL, USER, PASS);
			return true;
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}

	
	@Deprecated
	/**
	 * Fecha a conex�o com o banco de dados.
	 * @return <i>TRUE</i> se foi desconectado com sucesso.
	 */
	public static boolean desconectar(){
		try{
			conexao.close();
			return true;
		}catch(SQLException ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}
	
	
}