package connection;

import java.sql.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import extra.Constantes;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;

/**
 * Para criar o BD, o usu�rio que o utilizar� e as tabelas necess�rias.<br>
 * Ler configura��es a partir de um arquivo <b>XML</b>.
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 6 de jan de 2017
 */
public class BancoDeDados {
	
	private static Connection conexao = null;
	
	protected static final String CONFIG_FILE = "bibliotecapessoal_configs.xml";
	private static final String JDBCDRIVER = "com.mysql.jdbc.Driver";

	private static String ROOT_LOGIN="", ROOT_PASS="", ROOT_URL;
	protected static String DBNAME, URL, USER, PASS;

	
	private static final String CMD_criarUsuario = "GRANT ALL PRIVILEGES ON ?.* TO ?@'localhost' IDENTIFIED BY ? WITH GRANT OPTION";// dbname,username,password
	
	private static final String CMD_criarTabelaUsuarios = "CREATE TABLE "+Constantes.TBL_USUARIOS+"( login VARCHAR(25) PRIMARY KEY NOT NULL, senha VARCHAR(25) NOT NULL )";
	private static final String CMD_criarTabelaConfigs = "CREATE TABLE "+Constantes.TBL_CONFIGS+"( usuarios_login VARCHAR(25) PRIMARY KEY NOT NULL, categorias TEXT DEFAULT NULL,FOREIGN KEY (usuarios_login) REFERENCES "+Constantes.TBL_USUARIOS+"(login)	)";
	private static final String CMD_criarTabelaLivros = 
			"CREATE TABLE "+Constantes.TBL_LIVROS+"("+
				"id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"+
				"usuarios_login VARCHAR(25) NOT NULL,"+
				"desejado BOOLEAN NOT NULL DEFAULT FALSE,"+
				"favorito BOOLEAN NOT NULL DEFAULT FALSE,"+
				"nome TEXT NOT NULL,"+
				"autor TEXT NOT NULL,"+
			    "descricao VARCHAR(200),"+
				"edicao INT UNSIGNED NOT NULL DEFAULT 1,"+
				"categoria TEXT NOT NULL,"+
				"dia_aquisicao VARCHAR(10) NOT NULL,"+
				"dia_devolucao VARCHAR(10),"+
				"FOREIGN KEY (usuarios_login) REFERENCES "+Constantes.TBL_CONFIGS+"(usuarios_login) )";
	
	public BancoDeDados() {
		if(URL == null){
			if(!setConfigs())
				JOptionPane.showMessageDialog(null, "Erro ao ler o arquivo de configura��o do Banco de Dados", "Arquivo '"+CONFIG_FILE+"'", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/////////////////////////////[ �TIL ]/////////////////////////////
	
	/**
	 * Cria um banco e as tabelas que ser�o utilizadas pelo programa.<br>
	 * � necess�rio do login e senha de um usu�rio com permiss�es avan�adas no BD.
	 * @return <code>TRUE</code> se o BD foi criado com sucesso ou, se ele j� existe, admite que as tabelas foram criadas corretamente.  
	 */
	public boolean iniciarBancoDeDados(){
		/// ADMITE QUE A AUS�NCIA DAS CREDENCIAS DO ROOT SIGNIFICA QUE TUDO J� FOI CRIADO.
		return (ROOT_LOGIN.isEmpty() || ROOT_PASS.isEmpty()) ? (URL != null) : (criarBancoDeDados() != -1) && criarUsuario() && criarTabelas();
	}
	
	
	/////////////////////////////[ LER ARQUIVO XML ]/////////////////////////////
	
	private boolean setConfigs(){
		try{
			File fXmlFile = new File(CONFIG_FILE);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("database");
			Node nNode = nList.item(0);
			if(nNode.getNodeType() == Node.ELEMENT_NODE){
				Element eElement = (Element) nNode;
				setCredenciais(
						eElement.getAttribute("root_login"),
						eElement.getAttribute("root_pass"),
						eElement.getElementsByTagName("nomebd").item(0).getTextContent(),
						eElement.getElementsByTagName("url").item(0).getTextContent(),
						eElement.getElementsByTagName("user").item(0).getTextContent(),
						eElement.getElementsByTagName("pass").item(0).getTextContent()
				);
			}
			return true;
		}catch(ParserConfigurationException | SAXException | IOException e){
			/// USA AS CONFIGURA��ES PADR�ES
			return false;
		}
	}
	
	
	/////////////////////////////[ AUXILIARES ]/////////////////////////////

	private void setCredenciais(String rootLogin, String rootPass, String nomebd, String url, String user, String pass){
		ROOT_LOGIN = rootLogin;
		ROOT_PASS = rootPass;
		ROOT_URL = url+"?useSSL=false";
		
		DBNAME = nomebd;
		URL = url+nomebd+"?useSSL=false";
		USER = user;
		PASS = pass;
		/*
		System.out.printf("root:%s,%s\n",ROOT_LOGIN,ROOT_PASS);
		System.out.printf("URL:%s\n\n",ROOT_URL);
		System.out.printf("URL:%s\n", URL);
		System.out.printf("DBNAME:%s\n",DBNAME);
		System.out.printf("user:%s,%s\n\n",USER,PASS);
		*/
	}

	
	private static int criarBancoDeDados() {
		try {
			Class.forName(JDBCDRIVER);
			conexao = DriverManager.getConnection(ROOT_URL, ROOT_LOGIN, ROOT_PASS);
			
			Statement st = conexao.createStatement();
			return st.executeUpdate("CREATE DATABASE " + DBNAME); // 0,1 ou 2

		} catch (ClassNotFoundException | SQLException e) {
			/// BD J� EXISTE, i.e., TUDO CERTO PARA O PROGRAMA
			return (e.getMessage().contains("exist")) ? 3 : -1;
		}
	}

	private static boolean criarUsuario() {
		try {
			conexao = DriverManager.getConnection(ROOT_URL, ROOT_LOGIN, ROOT_PASS);
			String comando = CMD_criarUsuario.replaceFirst("\\?", DBNAME);
			
			PreparedStatement pst = conexao.prepareStatement(comando);
			pst.setString(1, USER);
			pst.setString(2, PASS);
			return (pst.executeUpdate() >= 0);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	
	private static int criarTabelaUsuarios() {
		try {
			conexao = DriverManager.getConnection(URL, USER, PASS);
			Statement st = conexao.createStatement();
			return st.executeUpdate(CMD_criarTabelaUsuarios);

		} catch (SQLException e) {
			return (e.getMessage().contains("exists")) ? 0 : -1;
		}
	}
	private static int criarTabelaConfigs() {
		try {
			conexao = DriverManager.getConnection(URL, USER, PASS);
			Statement st = conexao.createStatement();
			return st.executeUpdate(CMD_criarTabelaConfigs);

		} catch (SQLException e) {
			return (e.getMessage().contains("exists")) ? 0 : -1;
		}
	}
	private static int criarTabelaLivros() {
		try {
			conexao = DriverManager.getConnection(URL, USER, PASS);
			Statement st = conexao.createStatement();
			return st.executeUpdate(CMD_criarTabelaLivros);

		} catch (SQLException e) {
			return (e.getMessage().contains("exists")) ? 0 : -1;
		}
	}
	
	
	private static boolean criarTabelas() {
		return (criarTabelaUsuarios() >= 0) && (criarTabelaConfigs() >= 0) && (criarTabelaLivros() >= 0);
	}

	
	
}
