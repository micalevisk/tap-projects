import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

import extra.FileTypeFilter;
import extra.JAdvancedTable;
import extra.KeyUsuario;
import extra.WriteTablePDF;
import model.bean.Livro;
import model.bean.Usuario;
import model.dao.LivroDAO;
import extra.Constantes;
import org.apache.commons.lang3.text.WordUtils;
/**
* δ - para exibir todos os dados possíveis (em quantidade)	[QUARTA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 25 de dez de 2016
*/
public class GerarRelatorio_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private Font lblFonte = new Font("Ubuntu Light", Font.BOLD, 13);
	
	// CARREGAR DADOS DA TABELA
	private String[] nomeColunasVisaoGeral =  { "#","Usuário", "Nome","Autor(es)","Descrição","Edição","Categoria","Favorito","Aquisição","Devolução" };
    public int col_id=0, col_usuario=1, col_nome=2, col_autor=3, col_descricao=4, col_edicao=5, col_categoria=6, col_favorito=7, col_diaAquisicao=8, col_diaDevolucao=9;
    
    private DefaultTableModel modelTabelaVisaoGeral = new DefaultTableModel(null,nomeColunasVisaoGeral) {
		Class[] columnTypes = new Class[] {Integer.class,String.class,String.class,String.class,String.class,String.class,String.class,String.class,String.class,String.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	
	private String[] nomeColunasQuantidades = {"Usuários", "Livros Cadastrados", "Livros Favoritos", "Livros Desejados", "Livros Emprestados"};
	private DefaultTableModel modelTabelaQuantidades = new DefaultTableModel(null, nomeColunasQuantidades){
			boolean[] columnEditables = new boolean[] {false,false,false,false,false};
			public boolean isCellEditable(int row, int column) {return columnEditables[column];	}
			Class[] columnTypes = new Class[] {Integer.class,Integer.class,Integer.class,Integer.class,Integer.class};
			public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() {
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment(JLabel.CENTER);
	        return this;
	    }
	};
	
//	private JTable tblVisaoGeral;
	private JAdvancedTable tblVisaoGeral;
	private JAdvancedTable tblQuantidades;
	private JFileChooser fcrExportar;
	
	private FileFilter pdfFilter = new FileTypeFilter("pdf", "Portable Document Format");
	private FileFilter csvFilter = new FileTypeFilter("csv", "Comma-Separated Values");
	
	private Usuario usuarioLogado;
	private List<Livro> livrosCadastrados; /// PARA A TABELA DE LIVROS
	private HashMap<KeyUsuario, Integer> quantidadesGeral;
	//////////////////////////////////////////////////////////////////////////////////////////////

	

    public GerarRelatorio_jdialog(JFrame parent, Usuario usuarioLogado){
    	this(parent, usuarioLogado, true);
    }
	
    /**
     * @wbp.parser.constructor
     */
	public GerarRelatorio_jdialog(JFrame parent, Usuario usuarioLogado, boolean modal){
		super(parent, modal);
		initComponents();
		setResizable(false);
		this.usuarioLogado = usuarioLogado;
		this.livrosCadastrados = new LivroDAO().recuperarLivrosCadastrados();
		this.quantidadesGeral = usuarioLogado.getDAO().visaoGeral();
		
		popularTabelaVisaoGeral();
		popularTabelaQuantidades();
	}
	
	
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	public void initComponents() {
		setTitle("Relatório do Sistema");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 802, 608);
		setLocationRelativeTo(null);
		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBorder(null);
		pnlPrincipal.setBackground(SystemColor.control);
		setContentPane(pnlPrincipal);
		
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
	
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuPrimario = new JMenuBar();
		setJMenuBar(mnbMenuPrimario);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		mnArquivo.setIcon(new ImageIcon(Constantes.PATH_ICONS+"folder.png"));
		mnArquivo.setMnemonic(KeyEvent.VK_A);
		mnbMenuPrimario.add(mnArquivo);

		JMenuItem mniExportar = new JMenuItem("Exportar");
		mniExportar.setEnabled(true);
		mniExportar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"save.gif"));
		mniExportar.setActionCommand("_EXPORTAR");
		mniExportar.addActionListener(acaoBotao);
		mniExportar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.ALT_MASK));
		mniExportar.setToolTipText("salvar tabelas em arquivo");
		mnArquivo.add(mniExportar);

		JMenuItem mniCancelar = new JMenuItem("Cancelar");
		mniCancelar.setActionCommand("_CANCELAR");
		mniCancelar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cancel.png"));
		mniCancelar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		mniCancelar.addActionListener(acaoBotao);
		mnArquivo.add(mniCancelar);
		
		JMenuItem mniSobre = new JMenuItem("Sobre");
		mniSobre.setIcon(new ImageIcon(Constantes.PATH_ICONS+"information.png"));
		mniSobre.setActionCommand("_SOBRE");
		mniSobre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		mniSobre.addActionListener(acaoBotao);
		mnArquivo.add(mniSobre);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cross.png"));
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setToolTipText("fechar programa");
		mnArquivo.add(mniFechar);

		// DIALOG PARA SALVAR ARQUIVO
		fcrExportar = new JFileChooser(){
		    @Override
		    public void approveSelection(){
		        File f = getSelectedFile();
		        if(f.exists() && getDialogType() == SAVE_DIALOG){
		            int result = JOptionPane.showConfirmDialog(this,"Deseja substituir o arquivo?","Arquivo já existe",JOptionPane.YES_NO_OPTION);
		            switch(result){
		                case JOptionPane.YES_OPTION:
		                    super.approveSelection();
		                    return;
		                case JOptionPane.NO_OPTION:
		                    return;
		                case JOptionPane.CLOSED_OPTION:
		                    return;
		                case JOptionPane.CANCEL_OPTION:
		                    cancelSelection();
		                    return;
		            }
		        }
		        super.approveSelection();
		    }       
		};
		fcrExportar.setDialogTitle("Escolha um local para salvar o relatório");
		fcrExportar.setFileSelectionMode(JFileChooser.FILES_ONLY); // seleciona apenas arquivos
		fcrExportar.setAcceptAllFileFilterUsed(false); // desabilita a opção de "todas as extensões"
		fcrExportar.setFileFilter(pdfFilter);
		fcrExportar.setFileFilter(csvFilter);
		pnlPrincipal.setLayout(null);
		
		
		/////////////////////////[ FUNDO DAS TABELAS ]/////////////////////// 
		JScrollPane scrpnTabela = new JScrollPane();
		scrpnTabela.setBounds(10, 11, 776, 458);
		pnlPrincipal.add(scrpnTabela);

		JScrollPane scrpnContagem = new JScrollPane();
		scrpnContagem.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrpnContagem.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrpnContagem.setBounds(10, 505, 776, 40);
		pnlPrincipal.add(scrpnContagem);
		
		/////////////////////////[ TABELAS ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblVisaoGeral = new JAdvancedTable(modelTabelaVisaoGeral);
		tblVisaoGeral.setRowSorter(new TableRowSorter<DefaultTableModel>(modelTabelaVisaoGeral)); // definindo ordenação
		tblVisaoGeral.getRowSorter().toggleSortOrder(col_id); // definindo ordenação padrão pela coluna mestre
		tblVisaoGeral.setToolTipText("clique em uma coluna para ordená-la");
		tblVisaoGeral.setEnabled(false);
		tblVisaoGeral.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblVisaoGeral.getColumnModel().getColumn(col_edicao).setPreferredWidth(55);
		tblVisaoGeral.getColumnModel().getColumn(col_favorito).setPreferredWidth(55);
		tblVisaoGeral.getColumnModel().getColumn(col_id).setPreferredWidth(25);
		tblVisaoGeral.getColumnModel().getColumn(col_id).setCellRenderer(tableRendererParaID);
		tblVisaoGeral.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblVisaoGeral.setBackground(SystemColor.textHighlightText);
		tblVisaoGeral.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
		    @Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
		        JLabel renderedLabel = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        renderedLabel.setHorizontalAlignment(JLabel.CENTER);
		        renderedLabel.setBackground(row % 2 == 0 ? new Color(242, 242, 242) : Color.WHITE);
		        
		        return renderedLabel;
		    }
		});
		scrpnTabela.setViewportView(tblVisaoGeral);
		
		JLabel lblTotal = new JLabel("TOTAL");
		lblTotal.setIcon(new ImageIcon(Constantes.PATH_ICONS+"layout_edit.png"));
		lblTotal.setFont(new Font("Ubuntu Light", Font.BOLD, 16));
		lblTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotal.setBounds(10, 480, 776, 24);
		pnlPrincipal.add(lblTotal);
		
		tblQuantidades = new JAdvancedTable(modelTabelaQuantidades);
		tblQuantidades.setRowSelectionAllowed(false);
		tblQuantidades.setEnabled(false);
		// CENTRALIZAR COLUNAS DE CLASSE 'Integer'
		tblQuantidades.setDefaultRenderer(Integer.class, centerRenderer); 
		scrpnContagem.setViewportView(tblQuantidades);
	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void salvarRelatorio(){
		//(c) http://www.codejava.net/java-se/swing/show-save-file-dialog-using-jfilechooser
		//(c) http://www.codejava.net/java-se/swing/add-file-filter-for-jfilechooser-dialog		
		int userSelection = fcrExportar.showSaveDialog(null);
		 
		if(userSelection == JFileChooser.APPROVE_OPTION){
			FileTypeFilter filtroSelecionado = (FileTypeFilter)fcrExportar.getFileFilter();
		    File fileToSave = fcrExportar.getSelectedFile();
		    String filePath = fileToSave.getAbsolutePath().toLowerCase();

		    if(!(filePath.endsWith("pdf")) && !(filePath.endsWith("csv"))){
		    	filePath += filtroSelecionado.getExtension();
		    	fileToSave = new File(filePath);
		    }
		    
		    filePath = fileToSave.getAbsolutePath().toLowerCase();
		    if(filePath.endsWith("pdf")){ 
//		    	if(!exportarPDF(filePath, tblQuantidades))
	    		if(!exportarPDF(filePath))
		    		JOptionPane.showMessageDialog(null, "Não foi possível salvar!", "Erro Ao Exportar", JOptionPane.ERROR_MESSAGE);
		    }
		    else if(filePath.endsWith("csv")){ 
		    	if(!exportarCSV(filePath, tblVisaoGeral))
		    		JOptionPane.showMessageDialog(null, "Não foi possível salvar!", "Erro Ao Exportar", JOptionPane.ERROR_MESSAGE);
		    }
		}

	}


	public void redimensionarLarguraColuna(JTable table, int column) {
	    final TableColumnModel columnModel = table.getColumnModel();
        int width = 35; // Min width
        for(int row = 0; row < table.getRowCount(); row++){
            TableCellRenderer renderer = table.getCellRenderer(row, column);
            Component comp = table.prepareRenderer(renderer, row, column);
            width = Math.max(comp.getPreferredSize().width , width)+5;
        }
        if(width > 300)  width=300; // Max width
        columnModel.getColumn(column).setPreferredWidth(width);
	}
	
	
	private void popularTabelaVisaoGeral(){
		Object[] row = new Object[modelTabelaVisaoGeral.getColumnCount()];
		for(int i=0; i < livrosCadastrados.size(); ++i){
			Livro l = livrosCadastrados.get(i);
			if(l != null){
				row[col_id] = i+1;
				row[col_usuario] = l.getUsuarios_login();
				row[col_nome] = WordUtils.capitalize(l.getNome());
				row[col_autor] = l.getAutor().toUpperCase();
				row[col_descricao] = l.getDescricao();
				row[col_edicao] = Integer.toString( l.getEdicao() );
				row[col_categoria] = l.getCategoria();
				row[col_favorito] = l.isFavorito() ? "•" : "";
				row[col_diaAquisicao] = l.getDia_aquisicao();
				String diaDevolucao = l.getDia_devolucao();
				row[col_diaDevolucao] = diaDevolucao == null ? "--" : diaDevolucao;
			}
			
			modelTabelaVisaoGeral.addRow(row);
		}
		
		tblVisaoGeral.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		redimensionarLarguraColuna(tblVisaoGeral, col_nome);
		redimensionarLarguraColuna(tblVisaoGeral, col_descricao);
	}

	private void popularTabelaQuantidades(){
		if(this.quantidadesGeral == null) return;
		Object[] row = new Object[]{
			quantidadesGeral.get(KeyUsuario.QTD_USUARIOS),
			quantidadesGeral.get(KeyUsuario.QTD_LIVROS),
			quantidadesGeral.get(KeyUsuario.QTD_FAVORITOS),
			quantidadesGeral.get(KeyUsuario.QTD_DESEJADOS),
			quantidadesGeral.get(KeyUsuario.QTD_EMPRESTIMOS)
		};
		this.modelTabelaQuantidades.addRow(row);
	}
	
	
	private boolean exportarPDF(String caminhoArquivo){
//		return new WriteTablePDF(caminhoArquivo, this.nomeColunasVisaoGeral, table, this.usuarioLogado.getLogin(), "Relatório gerado pelo programa Gerenciador de Biblioteca Pessoal").writePDF();
		return WriteTablePDF.writePDF(caminhoArquivo, this.usuarioLogado.getLogin(), "Relatório gerado pelo programa Gerenciador de Biblioteca Pessoal", 
				this.nomeColunasVisaoGeral, this.tblVisaoGeral, 
				this.nomeColunasQuantidades, this.tblQuantidades);
	}
	
	public boolean exportarCSV(String caminhoArquivo, JTable tabela){
		List<String> columnNames = new ArrayList<String>();
		for(int i=0; i < tabela.getColumnCount(); ++i)
			columnNames.add(tabela.getColumnName(i));
		String header = String.join(",", columnNames);//java8
		
		DefaultTableModel dtm = (DefaultTableModel)tabela.getModel();
		int nRow = tabela.getRowCount();
		int nCol = tabela.getColumnCount();
		
		String response = new String(header + "\n");
	    for(int i = 0 ; i < nRow ; i++){
	        for(int j = 0 ; j < nCol ; j++){
	        	Object valor = dtm.getValueAt(i,j);
	        	if(j == col_favorito && !((String)valor).isEmpty()) valor = "x";
	        	response +=  valor + ",";
	        }
	        response = response.replaceFirst(",$", "\n");
	    }

	    try {
	    	FileWriter fw = new FileWriter(caminhoArquivo);
	    	fw.append(response);
	    	fw.flush();
	    	fw.close();
		    
			return true;
		} catch (IOException e) {
//			e.printStackTrace();
			return false;
		}
	}

	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "EXPORTAR": salvarRelatorio(); break;
	        	case "CANCELAR": dispose(); break;
	        	case "FECHAR": System.exit(0); break;
	        	case "SOBRE": Constantes.abrirSobre(null); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
}


