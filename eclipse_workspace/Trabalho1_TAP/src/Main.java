import java.awt.EventQueue;

import javax.swing.JOptionPane;

import connection.BancoDeDados;


/**
 * Inicia o fluxo do programa.
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 1 de jan de 2017
 */
public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BancoDeDados bd = new BancoDeDados(); /// inicializar BD
					if(!bd.iniciarBancoDeDados())
						JOptionPane.showMessageDialog(null, "Erro ao iniciar o Banco de Dados", "ERRO BD", JOptionPane.ERROR_MESSAGE);
					else
						new Secundaria_jframe(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
