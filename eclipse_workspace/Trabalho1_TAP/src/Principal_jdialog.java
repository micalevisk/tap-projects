import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AbstractDocument;

import extra.DocumentSizeFilter;
import extra.JTextFieldSemBrancos;
import extra.JTextFieldLimit;
import extra.RetornosLogin;
import extra.Constantes;
import model.bean.Usuario;
import java.awt.SystemColor;

/**
 * ω - invocará os sitemas de login e registro	[JANELA PRINCIPAL]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 27 de dez de 2016
 */
public class Principal_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private JTextFieldSemBrancos txtLogin; private String txtLogin_placeholder="seu nick aqui";
	private JPasswordField pswSenha; private String pswSenha_toolTipText="sua senha aqui";
	private JLabel lblCadastrar; 
	private JButton btnEntrar;
	
	private Usuario usuario = null;

//	private Border bordaExited = new LineBorder(Color.BLACK, 1);
//	private Border bordaEntered = new LineBorder(Color.CYAN, 1);
	//////////////////////////////////////////////////////////////////////////////////////////////
	


	public Principal_jdialog(JFrame parent){
		this(parent,true);
	}

	/**
	 * @wbp.parser.constructor
	 */
	public Principal_jdialog(JFrame parent, boolean modal){
		super(parent, modal);
		// inicializar dialog:
		initComponents();
		
		// para fechar o programa ao clicar no botão 'x'
		this.addWindowListener(new WindowAdapter() { 
		    @Override public void windowClosing(WindowEvent e) { 
		        fecharPrograma();
		      }
	    });
	}
	
	/**
	 * Recupera as informações digitas pelo usuário nos campos de login e senha.
	 * @return Array de duas Strings {login, senha}
	 */
	public String[] getCredenciais(){
		return new String[]{txtLogin.getText(), new String(pswSenha.getPassword())};
	}
	
	
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	private void initComponents() {
		setTitle("Biblioteca Pessoal v0.06-1");
		setResizable(false);
		setBounds(100, 100, 507, 196);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Constantes.PATH_ICONS+"book_open.png"));
		

		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.controlLtHighlight);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(null);
		setContentPane(pnlPrincipal);
	
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		CheckTextField alternarBotaoEntrar = new CheckTextField();
		
		
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuPrimario = new JMenuBar();
		setJMenuBar(mnbMenuPrimario);
		
		JMenu mnArquivo = new JMenu();
		mnArquivo.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cog.png"));
		mnArquivo.setMnemonic(KeyEvent.VK_A);
		mnbMenuPrimario.add(mnArquivo);
		
		JMenuItem mniSobre = new JMenuItem("Sobre");
		mniSobre.setIcon(new ImageIcon(Constantes.PATH_ICONS+"information.png"));
		mniSobre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		mniSobre.setActionCommand("_SOBRE");
		mniSobre.addActionListener(acaoBotao);
		mnArquivo.add(mniSobre);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cross.png"));
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setToolTipText("fechar programa");
		mnArquivo.add(mniFechar);
		/////////////////////////////////////////////////////////// 
		
		///////////////////[ CAMPOS PRINCIPAIS ]///////////////////
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setForeground(Color.DARK_GRAY);
		lblLogin.setFont(new Font("Visitor TT2 BRK", Font.PLAIN, 18));
		lblLogin.setBounds(267, 11, 118, 20);
		pnlPrincipal.add(lblLogin);
		
		txtLogin = new JTextFieldSemBrancos(txtLogin_placeholder);
		txtLogin.setDocument(new JTextFieldLimit(Constantes.MAX_CHARS_LOGIN));
		txtLogin.setActionCommand("_ENTRAR");
		txtLogin.setBounds(267, 28, 118, 20);
		pnlPrincipal.add(txtLogin);
		txtLogin.setColumns(10);
		txtLogin.addCaretListener(alternarBotaoEntrar);
		txtLogin.addActionListener(acaoBotao);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenha.setForeground(Color.DARK_GRAY);
		lblSenha.setFont(new Font("Visitor TT2 BRK", Font.PLAIN, 18));
		lblSenha.setBounds(267, 59, 118, 20);
		pnlPrincipal.add(lblSenha);
		
		pswSenha = new JPasswordField();
		pswSenha.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent me) {
				pswSenha.setEchoChar((char)0);
			}
			@Override
			public void mouseExited(MouseEvent me) {
				pswSenha.setEchoChar('×');
			}
		});
		pswSenha.setActionCommand("_ENTRAR");
		pswSenha.addActionListener(acaoBotao);
		pswSenha.setEchoChar('×');
		pswSenha.setToolTipText(pswSenha_toolTipText);
		pswSenha.setBounds(267, 76, 118, 20);
		pnlPrincipal.add(pswSenha);
		pswSenha.addCaretListener(alternarBotaoEntrar);
		// limitando quantidade de caracteres
		AbstractDocument pDoc = (AbstractDocument)pswSenha.getDocument();
		pDoc.setDocumentFilter(new DocumentSizeFilter(Constantes.MAX_CHARS_PASS));
		
		
		// (c) http://stackoverflow.com/questions/14159536/creating-jbutton-with-customized-look
		btnEntrar = new JButton("ENTRAR");
		btnEntrar.setMnemonic(KeyEvent.VK_E);
		btnEntrar.setToolTipText("fazer login");
		btnEntrar.setActionCommand("_ENTRAR");
		btnEntrar.addActionListener(acaoBotao);
		btnEntrar.setCursor(Constantes.CURSOR_BOTOES);
		btnEntrar.setEnabled(false);
		btnEntrar.setBounds(395, 28, 96, 68);
		btnEntrar.setFont(new Font("Tahoma", Font.BOLD, 14));
		pnlPrincipal.add(btnEntrar);
		
		////////////////[ EXTRAS ]////////////////
		JPanel pnlCadastro = new JPanel();
		pnlCadastro.setBackground(pnlPrincipal.getBackground());
		pnlCadastro.setBounds(267, 107, 224, 34);
		pnlPrincipal.add(pnlCadastro);
		pnlCadastro.setLayout(new BorderLayout(0, 0));

		JLabel lblInfoConta = new JLabel("não possui uma conta?");
		lblInfoConta.setForeground(Color.DARK_GRAY);
		pnlCadastro.add(lblInfoConta, BorderLayout.NORTH);
		lblInfoConta.setVerticalAlignment(SwingConstants.TOP);
		lblInfoConta.setFont(new Font("Ubuntu Mono", Font.PLAIN, 14));
		lblInfoConta.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblCadastrar = new JLabel("cadastrar-me");
		lblCadastrar.setToolTipText("criar um usuário com o login e senha digitados acima");
		lblCadastrar.setEnabled(false);
		pnlCadastro.add(lblCadastrar, BorderLayout.CENTER);
		lblCadastrar.setCursor(Constantes.CURSOR_BOTOES);
		lblCadastrar.setHorizontalAlignment(SwingConstants.CENTER);
		lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCadastrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent me){
				if(lblCadastrar.isEnabled()) lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 15));
			}
			@Override
			public void mouseReleased(MouseEvent me){
				if(lblCadastrar.isEnabled()){
					lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 12));
					chamarCadastroUsuario(false);
				}
			}
		});

		
		
		
//		JLabel lblBackgroundimage = new JLabel(new ImageIcon(Constantes.PATH_ICONS+"icomp_background.png"));
		JLabel lblBackgroundimage = new JLabel(new ImageIcon(Constantes.PATH_ICONS+"sideground.png"));
		lblBackgroundimage.setBounds(0, 0, 257, 141);
		pnlPrincipal.add(lblBackgroundimage);

		
	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private boolean camposValidos(){
		return (pswSenha.getPassword().length > 0) && !(txtLogin.textIsEmpty());
	}
	
	private void toggleBtnEntrar(){
		boolean tudoValido = camposValidos();
		lblCadastrar.setEnabled(tudoValido);
		btnEntrar.setEnabled(tudoValido);
	}
	
	public void chamarEntrar(){
		try{
			String[] credenciais = getCredenciais();
			this.usuario = new Usuario(credenciais[0], credenciais[1]);
			RetornosLogin loginResponse = usuario.fazerLogin();
			
			if(loginResponse == RetornosLogin.SENHA_INVALIDA){
				JOptionPane.showMessageDialog(null, "A senha não corresponde ao nick \""+credenciais[0]+"\"","Erro Ao Entrar", JOptionPane.ERROR_MESSAGE);
				this.pswSenha.requestFocusInWindow();
				return;
			}
			if(loginResponse == RetornosLogin.LOGIN_NAO_EXISTE){
				int opcao = JOptionPane.showConfirmDialog(null, "O nick \""+credenciais[0]+"\"não existe!\nDeseja efeutar o cadastro?", "Erro Ao Entrar", JOptionPane.YES_NO_OPTION);
				if(opcao == JOptionPane.YES_OPTION) chamarCadastroUsuario(true);
				else this.txtLogin.requestFocusInWindow();
				return;
			}
			this.dispose();
		}catch(NullPointerException e){
//				System.out.println("chamarEntrar " + e.getMessage());
			JOptionPane.showMessageDialog(null, "Erro ao conectar com o Banco de Dados", "ERRO", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void chamarCadastroUsuario(boolean forceRegister){
		String[] credenciais = getCredenciais();
		this.usuario = new Usuario(credenciais[0], credenciais[1]);
		if( (!usuario.cadastrarUsuario()) && (!forceRegister) ){
			JOptionPane.showMessageDialog(null, "O nick \""+credenciais[0]+"\" já está sendo usado!","Erro Ao Cadastrar", JOptionPane.ERROR_MESSAGE);
			this.txtLogin.requestFocusInWindow();
			return;
		}
		
		chamarEntrar();
	}
	
	private void fecharPrograma(){
		System.exit(0);
	}
	

	public Usuario getUsuarioLogado(){
		return (btnEntrar.isEnabled()) ?  this.usuario : null;
	}
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class CheckTextField implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent e) {
			toggleBtnEntrar();
		}
	}
	
	
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "ENTRAR": chamarEntrar(); break;
	        	case "FECHAR": fecharPrograma(); break;
	        	case "SOBRE": Constantes.abrirSobre(null); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
}
