import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import extra.JAdvancedTable;
import extra.JTextFieldComBrancos;
import extra.KeyLivro;
import model.bean.Livro;
import model.bean.Usuario;
import model.dao.LivroDAO;
import extra.Constantes;
/**
* γ - exibir informações dos livros da tabela de 'livros emprestados' para edição	[TERCEIRA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 25 de dez de 2016
*/
public class Emprestimos_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private String[] nomeColunas =  { "#","Nome","Autor(es)",	"Descrição",	"Edição",	"Dia da Aquisição","Dia da Devolução" }; // informações básicas + categoria&datas
    public int col_id=0, col_nome=1, col_autor=2, col_descricao=3, col_edicao=4, col_diaAquisicao=5, col_diaDevolucao=6;
    
    private JAdvancedTable tblLivrosEmprestados;
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(null,nomeColunas) {
		boolean[] columnEditables = {false, true, true, false, false, false};
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {Integer.class,String.class,String.class,String.class,String.class,String.class,String.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() {
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment(JLabel.CENTER);
	        setToolTipText("editar detalhes");
	        return this;
	    }
	};
	
	
    private JButton btnSalvarAlteracoes;
    private JButton btnRemoverSelecionados;
    private JTextFieldComBrancos txtAutorProcurado;
    
    private Usuario usuarioLogado;
    private List<Livro> livrosEmprestados;
    private List<Integer> indicesRemovidos;
	//////////////////////////////////////////////////////////////////////////////////////////////


    public Emprestimos_jdialog(JFrame parent, Usuario usuarioLogado){
    	this(parent, usuarioLogado, true);
    }
    
    /**
	 * @wbp.parser.constructor
	 */
	public Emprestimos_jdialog(JFrame parent, Usuario usuarioLogado, boolean modal){
		super(parent, modal);
		setResizable(true);
		initComponents();
		
    	this.usuarioLogado = usuarioLogado;
    	this.livrosEmprestados = usuarioLogado.getLivrosUsuario().get(KeyLivro.EMPRESTADOS);
    			
    	atualizarDadosTabela(this.tblLivrosEmprestados, this.livrosEmprestados);
	}
	
	
	
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	public void initComponents() {
		setTitle("Consultar Títulos Emprestados");
		setType(Type.POPUP);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 802, 389);
		setLocationRelativeTo(null);
		JPanel  pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.inactiveCaption);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlPrincipal);

		
		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(new Color(255, 255, 255));
		pnlPrincipal.add(pnlBotoes, BorderLayout.NORTH);
		pnlBotoes.setLayout(new BorderLayout(0, 0));
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuSecundario = new JMenuBar();
		setJMenuBar(mnbMenuSecundario);
		
		JMenu mnArquivo = new JMenu();
		mnArquivo.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cog.png"));
		mnArquivo.setMnemonic(KeyEvent.VK_A);
		mnbMenuSecundario.add(mnArquivo);
		
		JMenuItem mniCancelar = new JMenuItem("Cancelar");
		mniCancelar.setActionCommand("_CANCELAR");
		mniCancelar.addActionListener(acaoBotao);
		mniCancelar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cancel.png"));
		mniCancelar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		mnArquivo.add(mniCancelar);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cross.png"));
		mniFechar.setToolTipText("fechar programa");
		mnArquivo.add(mniFechar);
		
		////////////////////////[ BOTÕES ]////////////////////////
		btnSalvarAlteracoes = new JButton("Salvar Alterações");
		btnSalvarAlteracoes.setActionCommand("_SALVAR");
		btnSalvarAlteracoes.addActionListener(acaoBotao);
		btnSalvarAlteracoes.setMnemonic(KeyEvent.VK_S);
		btnSalvarAlteracoes.setCursor(Constantes.CURSOR_BOTOES);
		btnSalvarAlteracoes.setForeground(new Color(0, 128, 0));
		pnlBotoes.add(btnSalvarAlteracoes, BorderLayout.WEST);
			
		String textRemoverSelecionados = "Remover Selecionados";
		btnRemoverSelecionados = new JButton(textRemoverSelecionados);
		btnRemoverSelecionados.setActionCommand("_REMOVER");
		btnRemoverSelecionados.addActionListener(acaoBotao);
//		btnRemoverSelecionados.setMnemonic(KeyEvent.VK_R);
		btnRemoverSelecionados.setCursor(Constantes.CURSOR_BOTOES);
		btnRemoverSelecionados.setEnabled(false);
		btnRemoverSelecionados.setForeground(new Color(128, 0, 0));
		pnlBotoes.add(btnRemoverSelecionados, BorderLayout.CENTER);
		
		txtAutorProcurado = new JTextFieldComBrancos("buscar autor");
		txtAutorProcurado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent ke) {
				JTextFieldComBrancos campo = (JTextFieldComBrancos) ke.getSource();
				String autor = campo.textIsEmpty() ? null : campo.getText();
				buscar(autor, livrosEmprestados, tblLivrosEmprestados, usuarioLogado);
			}
		});
		pnlBotoes.add(txtAutorProcurado, BorderLayout.EAST);
		txtAutorProcurado.setHorizontalAlignment(SwingConstants.LEFT);
		txtAutorProcurado.setColumns(10);
		
		
		/////////////////////////[ FUNDO DA TABELA ]/////////////////////// 
		JScrollPane scrpnlTabela = new JScrollPane();
		pnlPrincipal.add(scrpnlTabela);
		scrpnlTabela.addMouseListener(new MouseAdapter() { // unselect when clicked out table
			@Override
			public void mouseClicked(MouseEvent me) {
				tblLivrosEmprestados.clearSelection();
			}
		});

		/////////////////////////[ TABELA ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivrosEmprestados = new JAdvancedTable(modelTabelaLivros);
		scrpnlTabela.setViewportView(tblLivrosEmprestados);
		tblLivrosEmprestados.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivrosEmprestados.getTableHeader().setReorderingAllowed(false); // disable user column draggin
		tblLivrosEmprestados.setRowSorter(new TableRowSorter<DefaultTableModel>(modelTabelaLivros)); // definindo ordenação
		tblLivrosEmprestados.getRowSorter().toggleSortOrder(col_id); // definindo ordenação padrão pela coluna mestre
		tblLivrosEmprestados.setDefaultRenderer(Integer.class, centerRenderer); 
		tblLivrosEmprestados.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivrosEmprestados.setBackground(SystemColor.textHighlightText);
		tblLivrosEmprestados.getColumnModel().getColumn(col_id).setPreferredWidth(1);
		tblLivrosEmprestados.getColumnModel().getColumn(col_id).setCellRenderer(tableRendererParaID); // definir fonte e cursor para a coluna mestre
		tblLivrosEmprestados.addMouseListener(new MouseAdapter() { // definir ação quando clicar duas vezes na coluna mestre
			@Override
			public void mousePressed(MouseEvent me) {
				Point p = me.getPoint();
				int row = tblLivrosEmprestados.rowAtPoint(p);
				int col = tblLivrosEmprestados.columnAtPoint(p);
				if(col == col_id){
//					JOptionPane.showMessageDialog(null, "MOSTRAR informações avançadas DO LIVRO DE id = " + tblLivrosEmprestados.getIdFromRow(row, col_id));
					chamarDetalhes( tblLivrosEmprestados.getSelectedRow() );
				}
			}
		});
		tblLivrosEmprestados.getSelectionModel().addListSelectionListener(new ListSelectionListener() { // habilitar somente se tiver alguma linha selecionada
			@Override
	        public void valueChanged(ListSelectionEvent e){ 
	            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	            btnRemoverSelecionados.setEnabled(!lsm.isSelectionEmpty());
	            
	            int qtdSelecionadas = tblLivrosEmprestados.getSelectedRows().length;
	            btnRemoverSelecionados.setText(textRemoverSelecionados+ (qtdSelecionadas>0 ? (" ("+qtdSelecionadas+")") : ""));
	        }
		});
		tblLivrosEmprestados.addMouseMotionListener(new MouseMotionAdapter() { // definir cursor quando estiver sobre a coluna mestre
			@Override
			public void mouseMoved(MouseEvent mv) {
				int col = tblLivrosEmprestados.columnAtPoint(mv.getPoint());
				if(col == col_id) setCursor(Constantes.CURSOR_BOTOES);
				else setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});
		// definir clique quando apertar ENTER numa linha da tabela:
		tblLivrosEmprestados.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "_EDITAR");
		tblLivrosEmprestados.getActionMap().put("_EDITAR",new AbstractAction() { 
			@Override
			public void actionPerformed(ActionEvent ae) {
				chamarDetalhes( tblLivrosEmprestados.getSelectedRow() );
			}
		});
		tblLivrosEmprestados.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
		    @Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
		        JLabel renderedLabel = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        renderedLabel.setHorizontalAlignment(JLabel.CENTER);
		        renderedLabel.setBackground(row % 2 == 0 ? new Color(230, 230, 255) : Color.WHITE);
		        
		        return renderedLabel;
		    }
		});
		
	
	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void chamarDetalhes(int indice){
		try{
			Livro lclicado = this.livrosEmprestados.get(indice);
			KeyLivro tipoAntes = lclicado.getTipo();
			
			Detalhes_jdialog detalhes = new Detalhes_jdialog(this.usuarioLogado, lclicado, indice);
			detalhes.setLocationRelativeTo(null);
			detalhes.setVisible(true);
			KeyLivro tipoDepois = lclicado.getTipo();
			
			if(tipoAntes == tipoDepois)	atualizarLivroTabela(lclicado, indice);
			else /// caso o livro tenha "migrado" de lista
				atualizarDadosTabela(this.tblLivrosEmprestados, this.livrosEmprestados); 
			
		}catch(ArrayIndexOutOfBoundsException ex){
			System.out.println("ERRO chamarDetalhes() "+ex.getMessage());
		}
	}
	
	/**
	 * Filtra as linhas de uma tabela pelo atributo {@link Livro#getAutor()}.<br>
	 * Exibe todos os elementos da lista caso o <code>autor</code> seja <code>NULL</code>.
	 * @param autor O nome do autor que será buscado.
	 * @param titulosAlvo A Lista de livros em que será efetuada a busca.
	 * @param tabelaAlvo A tabela que será alterada com essa busca.
	 * @param usuarioAlvo
	 */
	private void buscar(String autor, List<Livro> titulosAlvo, JTable tabelaAlvo, Usuario usuarioAlvo){
		if(autor == null) atualizarDadosTabela(tabelaAlvo, titulosAlvo);
		else atualizarDadosTabela(tabelaAlvo, usuarioAlvo.filtarLivrosAutor(titulosAlvo, autor));
	}
	
	/**
	 * Carrega os dados de uma lista de Livro para a visualização na JTable.<br>
	 * Utiliza a lista de livros nessa classe para preencher a JTable.
	 * @param tabela A JTable alvo da alteração. 
	 * @param dados A lista dos títulos que serão carregados.
	 */
	private void atualizarDadosTabela(JTable tabela, List<Livro> dados){
		DefaultTableModel modelTabela = (DefaultTableModel) tabela.getModel();
		modelTabela.setRowCount(0); // zerando
		
		Object[] row = new Object[tabela.getColumnCount()];
		for(int i=0; i < dados.size(); ++i){
			Livro l = dados.get(i);
			if(l != null){
				row[col_id] = i+1;
				row[col_nome] = l.getNome();
				row[col_autor] = l.getAutor();
				row[col_descricao] = l.getDescricao();
				row[col_edicao] = Integer.toString( l.getEdicao() );
				row[col_diaAquisicao] = l.getDia_aquisicao();
				row[col_diaDevolucao] = l.getDia_devolucao();
			}
			
			modelTabela.addRow(row);
		}
	}
	
	/**
	 * Atualiza os dados expostos na tabela a partir dos valores contido no Livro e seu índice.
	 * @param l O livro que foi, provavelmente, alterado
	 * @param indice Corresponde ao número da linha na matriz de linhas (ou, id - 1)
	 */
	private void atualizarLivroTabela(Livro l, int indice){
		tblLivrosEmprestados.setValueAt(l.getNome(),indice,col_nome);
		tblLivrosEmprestados.setValueAt(l.getAutor(),indice,col_autor);
		tblLivrosEmprestados.setValueAt(l.getDescricao(),indice,col_descricao);
		tblLivrosEmprestados.setValueAt(l.getEdicao(),indice,col_edicao);
	}
	
	/**
	 * Apaga do modelo da tabela as linhas que foram seleciondas.<br>
	 * Define a lista dos índices (row) das linhas removidas.
	 */
	private void removerSelecionados(){
		this.indicesRemovidos = tblLivrosEmprestados.removerLinhasSelecionadas(col_id, modelTabelaLivros);//retira as linhas da tabela
	}
	
	
	/**
	 * Salva as mudanças feitas diretamente nas células (incluindo remoção), atualizando a lista do Usuario e o BD.
	 */
	private void salvarAlteracoesVisuais(){
		/// percorrer itens da JTable e pra cada índice da tabela acessar
		Livro l;
		LivroDAO ldao = new LivroDAO();

		for(Object[] rowData : tblLivrosEmprestados.getTableData()){
			Integer indice = Integer.valueOf( (Integer) rowData[col_id] ) - 1;
			l = livrosEmprestados.get(indice);
			l.setNome( (String) (rowData[col_nome]) );
			l.setAutor( (String) (rowData[col_autor]) );

			/// atualiza no BD
			ldao.atualizarLivroDoUsuario(usuarioLogado, l);
		}
		if((indicesRemovidos != null) && (livrosEmprestados != null)){
			for(int j=0; j < indicesRemovidos.size(); ++j){
				try{
					int i = indicesRemovidos.get(j);
					
					ldao.apagarLivroUnico(livrosEmprestados.get(i).getId());
					livrosEmprestados.remove(i);
				}catch(IndexOutOfBoundsException e){
					System.out.println("salvarAlteracoesVisuais " + e.getMessage());
				}
			}
			
			atualizarDadosTabela(this.tblLivrosEmprestados, this.livrosEmprestados);
		}
	}
	

	

	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
		        case "CANCELAR": dispose(); break;
		        case "FECHAR": System.exit(0); break;
	        
	        	case "SALVAR": salvarAlteracoesVisuais(); break;
	        	case "REMOVER": removerSelecionados(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
		
		
		
}
