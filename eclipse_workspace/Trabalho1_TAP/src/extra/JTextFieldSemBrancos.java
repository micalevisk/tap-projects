package extra;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 * JTextField com placeholder, only lower case e sem espa�os.
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 24 de dez de 2016
 */
public class JTextFieldSemBrancos extends JAdvancedTextField {

	public JTextFieldSemBrancos() {
		super();
	}
	
	public JTextFieldSemBrancos(String placeholder) {
		super(placeholder);
	}

	// (c) https://coderanch.com/t/335672/java/force-uppercase-TextField
	///////// PARA CONVERTER PARA MIN�SCULAS E SEM ESPA�OS
	@Override
	protected Document createDefaultModel() {
		return new LimitedDocument(); 
	}

	static class LimitedDocument extends PlainDocument {
		@Override
		public void insertString(int offs, String str, AttributeSet attr) throws BadLocationException {
			if (str == null) return;
			
			String newstr = str.toLowerCase().trim(); 
			super.insertString(offs, newstr, attr);
		}

	}

}
