package extra;

import java.awt.Color;
import java.awt.Cursor;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.border.Border;

import model.bean.Livro;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import java.awt.Component;

/**
 * Guarda os valores e m�todos padr�es IMUT�VEIS.
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 28 de dez de 2016
 */
public class Constantes {
	
	public static final String COR_PARES ="#4b2186";
	public static final String COR_IMPARES ="#218668";
	
	public static final String CATEGORIAS_DEFAULT = "t�cnico,romance,a��o/aventura,religioso,revista,did�tico,biogr�fico";

	public static final Cursor CURSOR_BOTOES = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	public static final String PATH_ICONS = "C:\\Users\\user\\Desktop\\Micael\\Eclipse2(programa e workspace)\\tap-projects\\eclipse_workspace\\Trabalho1_TAP\\src\\icones\\";

	public static final Border brITEM_OBRIGATORIO = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red);
	public static final Border brPADRAO = new JTextField().getBorder();

	public static final int MAX_CHARS_LOGIN = 25;	// quantidade m�xima de caracteres no nick do usu�rio
	public static final int MAX_CHARS_PASS = 25;	// quantidade m�xima de caracteres na senha do usu�rio
	public static final int MAX_CHARS_DESC = 200;	// quantidade m�xima de caracteres na descri��o do t�tulo
	
	//// BD
	public static final String TBL_USUARIOS	= " tbl_usuarios ";
	public static final String TBL_CONFIGS	= " tbl_configs ";
	public static final String TBL_LIVROS	= " tbl_livros ";
	
	public static final String INFO_BASICAS   = " nome,autor,descricao,edicao ";
	public static final String INFO_AVANCADAS =  INFO_BASICAS+",categoria,dia_aquisicao,dia_devolucao ";
	
	
	//// A��ES
	public static void abrirSobre(Component parent){
//	    JOptionPane.showMessageDialog(parent, "Copyright (c) 2016-2017 Micael Levi L. C. 21554923", "T.A.P v1", JOptionPane.INFORMATION_MESSAGE);
	    String msg = "<HTML><font size=+1>&copy; 2016&#8722;2017 Micael Levi</font><br><font color=red>21554923</font> <b>mllc&#64;icomp.ufam.edu.br</b><HTML>";
	    String title = "T.A.P. 2016/2";
	    JOptionPane optionPane = new JOptionPane();
	    optionPane.setMessage(msg);
	    optionPane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
	    JDialog dialog = optionPane.createDialog(null, title);
	    dialog.setVisible(true);
	}
	
	public static void definirGUI(String className, String type){
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (type.equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(className).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(className).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(className).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(className).log(java.util.logging.Level.SEVERE, null, ex);
        }
	}

	

	
}
