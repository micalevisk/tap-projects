package extra;

/**
 * (c) http://www.devmedia.com.br/tipos-enum-no-java/25729
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 30 de dez de 2016
 */
public enum RetornosLogin {

	/**
	 * O login e a senha informados existem no BD e est�o associadas. 
	 */
	TUDO_CERTO(1),
	
	/**
	 * O login existe mas a senha informada n�o � a mesma do BD.
	 */
	SENHA_INVALIDA(2),
	
	/**
	 * O login informado n�o foi encontrado no BD.
	 */
	LOGIN_NAO_EXISTE(3);
	
	private final int valor;
	RetornosLogin(int valor){
		this.valor = valor;
	}
	
	public int getValor(){
		return this.valor;
	}
	
}
