package extra;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * L� uma tabela e salva em um arquivo PDF
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 7 de jan de 2017
 */
public class WriteTablePDF {


	private static PdfPTable criarTabelaPDF(float[] comprimentoColunas, String[] cabecalho, Object[][] dados){
		PdfPTable table = new PdfPTable(comprimentoColunas);

		//// INSERINDO OS HEADERS
		for(int i = 0; i < cabecalho.length; i++) {
			String header = cabecalho[i];
			PdfPCell cell = new PdfPCell();
			cell.setGrayFill(0.9f);
			cell.setPhrase(new Phrase(header.toUpperCase(), new Font(Font.HELVETICA, 10, Font.BOLD)));
			table.addCell(cell);
		}
		table.completeRow();
		
		/// INSERINDO DADOS
		for (int i = 0; i < dados.length; i++) {
			for (int j = 0; j < dados[i].length; j++) {
				String datum = new String();
				
				Object d = dados[i][j];
				if(d.getClass() != String.class) datum = Integer.toString((Integer)d); // se n�o for String ent�o � Integer/int
				else datum = (String) d;
					
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(datum, new Font(Font.HELVETICA, 10, Font.NORMAL)));
				table.addCell(cell);
			}
			table.completeRow();
		}

		return table;
	}
	
	
	public static boolean writePDF(String filename, String autorDocumento, String tituloDocumento,  String[] headers1, JAdvancedTable tabela1,  String[] headers2, JAdvancedTable tabela2){
		Document document = new Document(PageSize.LETTER.rotate());

		try {
			
			PdfWriter.getInstance(document, new FileOutputStream(new File(filename)));
			document.open();
			document.addTitle(tituloDocumento);
			document.addAuthor(autorDocumento);

			float[] columnWidths1 = { 10f, 20f, 22f, 26f, 25f, 18f, 25f, 22f, 25f, 27f};
			Object[][] data1 = tabela1.getTableData();
			document.add( WriteTablePDF.criarTabelaPDF(columnWidths1, headers1, data1) );
			
//			document.newPage();
			
			float[] columnWidths2 = { 50f, 50f, 50f, 50f, 50f };
			Object[][] data2 = tabela2.getTableData();
			document.add( WriteTablePDF.criarTabelaPDF(columnWidths2, headers2, data2) );
			
			return true;

		} catch (DocumentException | FileNotFoundException e) {
			e.printStackTrace();
			return false;
			
		} finally {
			document.close();
		}
	}
	
}
