/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 1 de jan de 2017
*/
package extra;

public enum KeyUsuario {
	/// no DAO
	QTD_USUARIOS(1),
	QTD_LIVROS(2),
	QTD_FAVORITOS(3),
	QTD_DESEJADOS(4),
	QTD_EMPRESTIMOS(5);
	
	
	private final int valor;
	KeyUsuario(int valor){
		this.valor = valor;
	}
	
	public int getValor(){
		return this.valor;
	}
}
