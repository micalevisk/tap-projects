package extra;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
// (c) https://github.com/redpois0n/placeholder-textfield/blob/master/src/com/redpois0n/placeholder/JPlaceholderTextField.java
@SuppressWarnings("serial")
/**
 * JTextField com placholder opcional
 */
public class JAdvancedTextField extends JTextField {
	private String placeholder;

	/**
	 * Campo de texto com lower case
	 */
	public JAdvancedTextField(){ 
		this(null);
	}
	
	/**
	 * Campo de texto com placeholder e lower case
	 * @param placeholder
	 */
	public JAdvancedTextField(String placeholder){ this.placeholder = placeholder; }

	@Override
	public String getText() {
		String text = super.getText().trim();
		if (text.length() == 0 && placeholder != null) text = placeholder;
		return text;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (super.getText().length() > 0 || placeholder == null) return;
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(super.getDisabledTextColor());
		g2.drawString(placeholder, getInsets().left, g.getFontMetrics().getMaxAscent() + getInsets().top );
	}
	
	
	public boolean textIsEmpty(){
		return this.getText().equals(placeholder);
	}

	
	
	
}

