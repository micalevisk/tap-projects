/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 30 de dez de 2016
*/
package extra;

public enum KeyLivro {
	
	/// atributos importantes do livro
	NOME(1),
	AUTOR(2),
	EDICAO(3),
	DESCRICAO(4),
	CATEGORIA(5),
	EHEMPRESTADO(6),
	EHDESEJADO(7),
	EHFAVORITO(8),
	AQUISICAO(9),
	DEVOLUCAO(10),
	TIPO(11),
	
	/// para listar os tipos de livros
	COMPRADOS(12),
	EMPRESTADOS(13),
	DESEJADOS(14),
	FAVORITOS(15);
	
	private final int valor;
	KeyLivro(int valor){
		this.valor = valor;
	}
	
	public int getValor(){
		return this.valor;
	}
	
}
