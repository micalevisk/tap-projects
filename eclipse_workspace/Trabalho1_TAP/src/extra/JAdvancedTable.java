package extra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * Métodos extras para trabalhar em JTable's
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 26 de dez de 2016
 */
public class JAdvancedTable extends JTable{
	public JAdvancedTable(){
		super();
	}
	public JAdvancedTable(TableModel m){
		super(m);
	}
	
	public String getIdFromRow(int row, int colID) throws ArrayIndexOutOfBoundsException  {
		try{
			return Integer.toString( (Integer)this.getValueAt(row, colID) ); // o valor da coluna 'id' das linhas selecionadas
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Recupera o valor (como String) da coluna passada como argumento da linha selecionada 
	 * @param colID A coluna alvo
	 * @return O valor
	 */
	public String getIdFromSelectedRow(int colID){
		int row;
		if((row = this.getSelectedRow()) >= 0){
			return (String) this.getValueAt(row, colID);
		}
		return null;
	}
	
	/**
	 * Remove linhas selecionadas da tabela & retorna os valores da 'colMestre' em um array de String.
	 * @param colMestre A coluna cujo valores são únicos pra cada linha.
	 * @param modelTabela o modelo da tabela.
	 * @return Os índices (da JTable) dos elementos removidos.
	 */
	public List<Integer> removerLinhasSelecionadas(int colMestre, DefaultTableModel modelTabela){
		int[] linhasSelecionadas = this.getSelectedRows();
		Arrays.sort(linhasSelecionadas);
		List<Integer> indices = new ArrayList<>();
		
		for(int i=linhasSelecionadas.length - 1; i >=0; --i){
			String colunaMestreValue = getIdFromRow(linhasSelecionadas[i], colMestre);
			modelTabela.removeRow(linhasSelecionadas[i]);
			indices.add(Integer.parseInt(colunaMestreValue) - 1);
		}
		
		return indices;
	}
	
	/**
	 * @return A matriz onde cada linha representa uma row na tabela visual.
	 */
	public Object[][] getTableData(){
	    DefaultTableModel dtm = (DefaultTableModel) this.getModel();
	    int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
	    Object[][] tableData = new Object[nRow][nCol];
	    for (int i = 0 ; i < nRow ; i++)
	        for (int j = 0 ; j < nCol ; j++)
	            tableData[i][j] = dtm.getValueAt(i,j);
	    
	    return tableData;
	}
	

	
}

// vide http://stackoverflow.com/questions/19940653/getting-the-id-from-a-row
// vide http://stackoverflow.com/questions/1117888/how-to-remove-a-row-from-jtable