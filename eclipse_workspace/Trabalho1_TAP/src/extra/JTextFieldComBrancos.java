package extra;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;


@SuppressWarnings("serial")
/**
 * JTextField com placeholder e only lower case
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 24 de dez de 2016
 */
public class JTextFieldComBrancos extends JAdvancedTextField{
	public JTextFieldComBrancos(String placeholder){
		super(placeholder);
	}
	public JTextFieldComBrancos(){
		super();
	}
	

	
	// (c) https://coderanch.com/t/335672/java/force-uppercase-TextField
	///////// PARA CONVERTER PARA MIN�SCULAS E SEM ESPA�OS
	@Override
	protected Document createDefaultModel() {
		return new LimitedDocument();
	}
	
	static class LimitedDocument extends PlainDocument {
		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			if(str == null) return;
			String newstr = str.toLowerCase();
			super.insertString(offs, newstr, a);
		}
	}

	
}

