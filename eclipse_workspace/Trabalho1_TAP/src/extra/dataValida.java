package extra;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.toedter.calendar.JDateChooser;

/**
* (c) http://www.dreamincode.net/forums/topic/14886-date-validation-using-simpledateformat/
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 28 de dez de 2016
*/
public class dataValida {
	// date validation using SimpleDateFormat
	// it will take a string and make sure it's in the proper
	// format as defined by you, and it will also make sure that
	// it's a legal date
	public static boolean isValidDate(JDateChooser DATE, String strDateFormat){
		SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		Date testDate = null;
		if( !DATE.isEnabled() ) return true;
		if(DATE == null || DATE.getDate() == null) return false;
		String date = DateFormat.getDateInstance().format(DATE.getDate());
			
		
		try{
			testDate = sdf.parse(date);
		}catch (ParseException e){
			System.out.println( "the date you provided is in an invalid date " + strDateFormat + " format." );
			return false;
		}

		if(!sdf.format(testDate).equals(date)){
			System.out.println( "The date that you provided is invalid." );
			return false;
		}
		return true;
	}
}
