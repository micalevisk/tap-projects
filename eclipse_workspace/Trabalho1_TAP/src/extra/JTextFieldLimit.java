package extra;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


/**
 * JTextField only lower case, sem espa�os e com limite de caracteres
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 6 de jan de 2017
*/
public class JTextFieldLimit extends PlainDocument {
	
	private int limit;
	
	public JTextFieldLimit(int limit){
		super();
		this.limit = limit;
	}
	
	@Override
	public void insertString(int offs, String str, AttributeSet attr) throws BadLocationException {
		if (str == null) return;
		
		String newstr = str.toLowerCase().trim(); 
		if((getLength() + newstr.length()) <= limit)
			
		super.insertString(offs, newstr, attr);
	}

}
