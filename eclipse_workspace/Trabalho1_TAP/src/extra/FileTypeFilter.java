package extra;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
* (c) http://www.codejava.net/java-se/swing/add-file-filter-for-jfilechooser-dialog
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 25 de dez de 2016
*/
public class FileTypeFilter extends FileFilter {
    private String extension;
    private String description;
 
    /**
     * @param extension - glob
     * @param description - descri��o do tipo
     */
    public FileTypeFilter(String extension, String description) {
        this.extension = "." + extension.toLowerCase();
        this.description = description;
    }
 
    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        return file.getName().endsWith(extension);
    }
 
    @Override
    public String getDescription() {
        return this.description + String.format(" (*%s)", extension);
    }
    
    /**
     * A extens�o selecionada (com ponto)
     * @return
     */
    public String getExtension(){
    	return this.extension;
    }
}