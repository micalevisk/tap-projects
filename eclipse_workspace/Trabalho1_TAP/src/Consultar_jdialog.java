//TODO campo para filtra por: autor
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import extra.JAdvancedTable;
import extra.JTextFieldComBrancos;
import extra.KeyLivro;
import model.bean.Usuario;
import model.dao.LivroDAO;
import model.bean.Livro;
import extra.Constantes;
import java.awt.event.KeyAdapter;
/**
* α - a janela de consultas a tabela de livros	[PRIMEIRA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 23 de dez de 2016
*/
public class Consultar_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private final String TABELA_DB = "";
	
	// CARREGAR DADOS DA TABELA
	private String[] nomeColunas ={ "#", "Nome", "Autor(es)", "Descrição","Edição" }; // informações básicas
	private int col_id=0;// índice na lista correspondente
	private int col_nome=1, col_autor=2, col_descricao=3, col_edicao=4;
	
    private JAdvancedTable tblLivros;
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(null,nomeColunas) {
		boolean[] columnEditables = {false, true, true, false, false}; 
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {Integer.class,String.class,String.class,String.class,String.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() { // (c) http://stackoverflow.com/questions/16113950/jtable-change-column-font
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment( JLabel.CENTER );
	        setToolTipText("editar detalhes");
	        return this;
	    }
	};
	
    private JButton btnSalvarAlteracoes;
    private JButton btnApagarSelecionados;
    private JTextFieldComBrancos txtAutorProcurado;
    
    private Usuario usuarioLogado;
    private List<Livro> livrosComprados;
    private List<Integer> indicesRemovidos;
	//////////////////////////////////////////////////////////////////////////////////////////////


    public Consultar_jdialog(JFrame parent, Usuario usuarioLogado){
    	this(parent, usuarioLogado, true);
    }
    
    /**
	 * @wbp.parser.constructor
	 */
	public Consultar_jdialog(JFrame parent, Usuario usuarioLogado, boolean modal){
		super(parent, modal);
		setResizable(true);
		initComponents();
		
    	this.usuarioLogado = usuarioLogado;
    	this.livrosComprados = usuarioLogado.getLivrosUsuario().get(KeyLivro.COMPRADOS);
    	atualizarDadosTabela(this.tblLivros, this.livrosComprados);
	}
	
	
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	public void initComponents(){
		setTitle("Consultar Títulos Adquiridos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 558, 389);
		setLocationRelativeTo(null);
		JPanel  pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.inactiveCaption);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlPrincipal);

		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(new Color(255, 255, 255));
		pnlPrincipal.add(pnlBotoes, BorderLayout.NORTH);
		pnlBotoes.setLayout(new BorderLayout(0, 0));
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuSecundario = new JMenuBar();
		setJMenuBar(mnbMenuSecundario);
		
		JMenu mnArquivo = new JMenu();
		mnArquivo.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cog.png"));
		mnArquivo.setMnemonic(KeyEvent.VK_A);
		mnbMenuSecundario.add(mnArquivo);
		
		JMenuItem mniCancelar = new JMenuItem("Cancelar");
		mniCancelar.setActionCommand("_CANCELAR");
		mniCancelar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cancel.png"));
		mniCancelar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		mniCancelar.addActionListener(acaoBotao);
		mnArquivo.add(mniCancelar);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cross.png"));
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setToolTipText("fechar programa");
		mnArquivo.add(mniFechar);
		
		
		////////////////////////[ BOTÕES ]////////////////////////
		btnSalvarAlteracoes = new JButton("Salvar Alterações");
		btnSalvarAlteracoes.setActionCommand("_SALVAR");
		btnSalvarAlteracoes.setMnemonic(KeyEvent.VK_S);
		btnSalvarAlteracoes.addActionListener(acaoBotao);
		btnSalvarAlteracoes.setCursor(Constantes.CURSOR_BOTOES);
		btnSalvarAlteracoes.setForeground(new Color(0, 128, 0));
		pnlBotoes.add(btnSalvarAlteracoes, BorderLayout.WEST);
		/*
		btnBuscar = new JButton("Buscar");
		btnBuscar.setActionCommand("_BUSCAR");
		btnBuscar.setMnemonic(KeyEvent.VK_B);
		btnBuscar.addActionListener(acaoBotao);
		btnBuscar.setCursor(Constantes.CURSOR_BOTOES);
		btnBuscar.setForeground(new Color(0, 128, 128));
		pnlBotoes.add(btnBuscar, BorderLayout.EAST);
		*/
		String textApagarSelecionados = "Remover Selecionados";
		btnApagarSelecionados = new JButton(textApagarSelecionados);
		btnApagarSelecionados.setActionCommand("_REMOVER" );
		btnApagarSelecionados.addActionListener(acaoBotao);
//		btnApagarSelecionados.setMnemonic(KeyEvent.VK_A);
		btnApagarSelecionados.setCursor(Constantes.CURSOR_BOTOES);
		btnApagarSelecionados.setEnabled(false);
		btnApagarSelecionados.setForeground(new Color(128, 0, 0));
		pnlBotoes.add(btnApagarSelecionados, BorderLayout.CENTER);
		
		txtAutorProcurado = new JTextFieldComBrancos("buscar autor");
		txtAutorProcurado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent ke) {
				JTextFieldComBrancos campo = (JTextFieldComBrancos) ke.getSource();
				String autor = campo.textIsEmpty() ? null : campo.getText();
				buscar(autor, livrosComprados, tblLivros, usuarioLogado);
			}
		});
		txtAutorProcurado.setToolTipText("aperte ENTER para efetivar a busca");
		pnlBotoes.add(txtAutorProcurado, BorderLayout.EAST);
		txtAutorProcurado.setHorizontalAlignment(SwingConstants.LEFT);
		txtAutorProcurado.setColumns(10);
		
		
		/////////////////////////[ FUNDO DA TABELA ]/////////////////////// 
		JScrollPane scrpnlTabela = new JScrollPane();
		scrpnlTabela.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlPrincipal.add(scrpnlTabela);
		scrpnlTabela.addMouseListener(new MouseAdapter() { // unselect when clicked out table
			@Override
			public void mouseClicked(MouseEvent me) {
				tblLivros.clearSelection();
			}
		});

		/////////////////////////[ TABELA ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivros = new JAdvancedTable(modelTabelaLivros);
		scrpnlTabela.setViewportView(tblLivros);
		tblLivros.getTableHeader().setReorderingAllowed(false); // disable user column draggin
		tblLivros.setRowSorter(new TableRowSorter<DefaultTableModel>(modelTabelaLivros)); // definindo ordenação
		tblLivros.getRowSorter().toggleSortOrder(col_id); // definindo ordenação padrão pela coluna mestre
		tblLivros.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivros.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivros.setBackground(SystemColor.textHighlightText);
		tblLivros.setDefaultRenderer(Integer.class, centerRenderer); 
		tblLivros.getColumnModel().getColumn(col_edicao).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(col_id).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(col_id).setCellRenderer(tableRendererParaID); // definir fonte e cursor para a coluna mestre
		tblLivros.addMouseListener(new MouseAdapter() { // definir ação quando clicar duas vezes na coluna mestre
			@Override
			public void mouseClicked(MouseEvent me) {
				Point p = me.getPoint();
				int row = tblLivros.rowAtPoint(p);
				int col = tblLivros.columnAtPoint(p);
				if((col == col_id) && (me.getClickCount() == 2)){
//					int indice = Integer.parseInt(tblLivros.getIdFromRow(row, col_id)) - 1;
					chamarDetalhes( tblLivros.getSelectedRow() );
//					JOptionPane.showMessageDialog(null, "MOSTRAR informações avançadas DO LIVRO  livrosComprados.get("+indice+")");
				}
			}
		});
		tblLivros.getSelectionModel().addListSelectionListener(new ListSelectionListener() { // habilitar somente se tiver alguma linha selecionada
			@Override
	        public void valueChanged(ListSelectionEvent e){ 
	            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	            btnApagarSelecionados.setEnabled(!lsm.isSelectionEmpty());
	            
	            int qtdSelecionadas = tblLivros.getSelectedRows().length;
	            btnApagarSelecionados.setText(textApagarSelecionados+ (qtdSelecionadas>0 ? (" ("+qtdSelecionadas+")") : ""));
	        }
		});
		tblLivros.addMouseMotionListener(new MouseMotionAdapter() { // definir cursor quando estiver sobre a coluna mestre
			@Override
			public void mouseMoved(MouseEvent mv) {
				int col = tblLivros.columnAtPoint(mv.getPoint());
				if(col == col_id) setCursor(Constantes.CURSOR_BOTOES);
				else setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});
		// definir clique quando apertar ENTER numa linha da tabela:
		tblLivros.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "_EDITAR");
		tblLivros.getActionMap().put("_EDITAR",new AbstractAction() { 
			@Override
			public void actionPerformed(ActionEvent ae) {
				chamarDetalhes( tblLivros.getSelectedRow() );
			}
		});
		tblLivros.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
		    @Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
		        JLabel renderedLabel = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        renderedLabel.setHorizontalAlignment(JLabel.CENTER);
		        renderedLabel.setBackground(row % 2 == 0 ? new Color(204, 255, 204) : Color.WHITE);
		        
		        return renderedLabel;
		    }
		});
		
		
	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void chamarDetalhes(int indice){
		try{
			Livro lclicado = this.livrosComprados.get(indice);
			KeyLivro tipoAntes = lclicado.getTipo();
			
			Detalhes_jdialog detalhes = new Detalhes_jdialog(this.usuarioLogado, lclicado, indice);
			detalhes.setLocationRelativeTo(null);
			detalhes.setVisible(true);
			KeyLivro tipoDepois = lclicado.getTipo();
			
			if(tipoAntes == tipoDepois)	atualizarLivroTabela(lclicado, indice);
			else /// caso o livro tenha "migrado" de lista
				atualizarDadosTabela(this.tblLivros, this.livrosComprados); 
			
		}catch(ArrayIndexOutOfBoundsException ex){
			System.out.println("ERRO chamarDetalhes() "+ex.getMessage());
		}
	}
	
	/**
	 * Filtra as linhas de uma tabela pelo atributo {@link Livro#getAutor()}.<br>
	 * Exibe todos os elementos da lista caso o <code>autor</code> seja <code>NULL</code>.
	 * @param autor O nome do autor que será buscado.
	 * @param titulosAlvo A Lista de livros em que será efetuada a busca.
	 * @param tabelaAlvo A tabela que será alterada com essa busca.
	 * @param usuarioAlvo
	 */
	private void buscar(String autor, List<Livro> titulosAlvo, JTable tabelaAlvo, Usuario usuarioAlvo){
		if(autor == null) atualizarDadosTabela(tabelaAlvo, titulosAlvo);
		else atualizarDadosTabela(tabelaAlvo, usuarioAlvo.filtarLivrosAutor(titulosAlvo, autor));
	}
	
	
	/**
	 * Carrega os dados de uma lista de Livro para a visualização na JTable.<br>
	 * Utiliza a lista de livros nessa classe para preencher a JTable.
	 * @param tabela A JTable alvo da alteração. 
	 * @param dados A lista dos títulos que serão carregados.
	 */
	private void atualizarDadosTabela(JTable tabela, List<Livro> dados){
		DefaultTableModel modelTabela = (DefaultTableModel) tabela.getModel();
		modelTabela.setRowCount(0); // zerando
		
		Object[] row = new Object[modelTabela.getColumnCount()];
		for(int i=0; i < dados.size(); ++i){
			Livro l = dados.get(i);
			if(l != null){
				row[col_id] = i+1;
				row[col_nome] = l.getNome();
				row[col_autor] = l.getAutor();
				row[col_descricao] = l.getDescricao();
				row[col_edicao] = Integer.toString( l.getEdicao() );
			}
			
			modelTabela.addRow(row);
		}
	}
	
	/**
	 * Atualiza os dados expostos na tabela a partir dos valores contido no Livro e seu índice.
	 * @param l O livro que foi, provavelmente, alterado
	 * @param indice Corresponde ao número da linha na matriz de linhas (ou, id - 1)
	 */
	private void atualizarLivroTabela(Livro l, int indice){
		tblLivros.setValueAt(l.getNome(),indice,col_nome);
		tblLivros.setValueAt(l.getAutor(),indice,col_autor);
		tblLivros.setValueAt(l.getDescricao(),indice,col_descricao);
		tblLivros.setValueAt(l.getEdicao(),indice,col_edicao);
	}
	
	/**
	 * Apaga do modelo da tabela as linhas que foram seleciondas.<br>
	 * Define a lista dos índices (row) das linhas removidas.
	 */
	private void removerSelecionados(){
		this.indicesRemovidos = tblLivros.removerLinhasSelecionadas(col_id, modelTabelaLivros);//retira as linhas da tabela
	}
	
	/**
	 * Salva as mudanças feitas diretamente nas células (incluindo remoção), atualizando a lista do Usuario e o BD.
	 */
	private void salvarAlteracoesVisuais(){
		/// percorrer itens da JTable e pra cada índice da tabela acessar
		Livro l;
		LivroDAO ldao = new LivroDAO();
		
		for(Object[] rowData : tblLivros.getTableData()){
			Integer indice = Integer.valueOf( (Integer) rowData[col_id] ) - 1; // (c) http://stackoverflow.com/questions/3661413/how-to-cast-an-object-to-an-int-in-java
			l = livrosComprados.get(indice);
			l.setNome( (String) (rowData[col_nome]) );
			l.setAutor( (String) (rowData[col_autor]) );

			/// atualiza no BD
			ldao.atualizarLivroDoUsuario(usuarioLogado, l);
		}
		/// TODO atualizar lista de favoritos
		if((indicesRemovidos != null) && (livrosComprados != null)){
			for(int j=0; j < indicesRemovidos.size(); ++j){
				try{				
					int i = indicesRemovidos.get(j);
					int idLivroCorrente = livrosComprados.get(i).getId();
					
					if( !ldao.apagarLivroUnico(idLivroCorrente) ) return;
					livrosComprados.remove(i);
					usuarioLogado.removerLivroComId(idLivroCorrente, KeyLivro.FAVORITOS); // pois está nas duas listas
					
				}catch(IndexOutOfBoundsException e){
					System.out.println("salvarAlteracoesVisuais " + e.getMessage());
				}
			}
			
			atualizarDadosTabela(this.tblLivros, this.livrosComprados);
		}
	}
	

	

	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
		        case "CANCELAR": dispose(); break;
		        case "FECHAR": System.exit(0); break;
		        
	        	case "SALVAR": salvarAlteracoesVisuais(); break;
	        	case "REMOVER": removerSelecionados(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
		
	

}