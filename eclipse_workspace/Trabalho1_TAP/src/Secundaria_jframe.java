import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import org.apache.commons.lang3.text.WordUtils;

import connection.BancoDeDados;

import java.awt.Toolkit;
import extra.Constantes;
import extra.KeyLivro;
import model.bean.Livro;
import model.bean.Usuario;
/**
 * ψ - onde o programa útil inicia	[JANELA SECUNDÁRIA]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 23 de dez de 2016
 */
public class Secundaria_jframe extends JFrame {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private JPanel pnlPrincipal;
	
	private Usuario usuarioLogado = null;
	//////////////////////////////////////////////////////////////////////////////////////////////

	
	public Secundaria_jframe(boolean personalizado){
        if(personalizado) Constantes.definirGUI(Secundaria_jframe.class.getName(), "Windows");		
		
		/// INICIA COM A TELA DE LOGIN
		Principal_jdialog telaLogin = new Principal_jdialog(this);
		telaLogin.setLocationRelativeTo(null);
		telaLogin.setVisible(true);
		
		/// DEFININDO USUÁRIO
		this.usuarioLogado = telaLogin.getUsuarioLogado();
		
		initComponents();
		setVisible(true);
	}
	
	
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	private void initComponents() {
		setTitle("Gerenciar Biblioteca de "+ WordUtils.capitalize(usuarioLogado.getLogin()));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // DEFINIR LOGOUT
		setBounds(100, 100, 447, 267);
		setLocationRelativeTo(null);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Constantes.PATH_ICONS+"book_open.png"));
		
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuSecundario = new JMenuBar();
		setJMenuBar(mnbMenuSecundario);
		
		JMenu mnArquivo = new JMenu();
		mnArquivo.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cog.png"));
		mnArquivo.setMnemonic(KeyEvent.VK_A);
		mnbMenuSecundario.add(mnArquivo);
		
		JMenuItem mniSobre = new JMenuItem("Sobre");
		mniSobre.setActionCommand("_SOBRE");
		mniSobre.setIcon(new ImageIcon(Constantes.PATH_ICONS+"information.png"));
		mniSobre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		mniSobre.addActionListener(acaoBotao);
		mnArquivo.add(mniSobre);
		
		JMenuItem mniSair = new JMenuItem("Sair");
		mniSair.setActionCommand("_SAIR");
		mniSair.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK));
		mniSair.setIcon(new ImageIcon(Constantes.PATH_ICONS+"door_out.png"));
		mniSair.addActionListener(acaoBotao);
		mniSair.setToolTipText("fazer logout");
		mnArquivo.add(mniSair);

		JMenuItem mniAtualizar = new JMenuItem("Atualizar");
		mniAtualizar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_MASK));
		mniAtualizar.setActionCommand("_ATUALIZAR");
		mniAtualizar.addActionListener(acaoBotao);
		mniAtualizar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"arrow_refresh.png"));
		mniAtualizar.setToolTipText("recarregar banco de dados");
		mnArquivo.add(mniAtualizar);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cross.png"));
		mniFechar.setToolTipText("fechar programa");
		mnArquivo.add(mniFechar);
		
		
		
		JScrollPane scrpnPrincipal = new JScrollPane();
		scrpnPrincipal.setBounds(0, 0, 434, 261);
		scrpnPrincipal.setBorder(null);
		setContentPane(scrpnPrincipal);
		
		pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.control);
		pnlPrincipal.setBorder(null);
//		setContentPane(pnlPrincipal);
		pnlPrincipal.setLayout(null);
		scrpnPrincipal.setViewportView(pnlPrincipal);
		
		JSeparator sptDivisor = new JSeparator();
		sptDivisor.setOrientation(SwingConstants.VERTICAL);
		sptDivisor.setBounds(215, 11, 11, 195);
		pnlPrincipal.add(sptDivisor);
		
		/////////////////////////////////////[ SUPERIOR ESQUERDO ]/////////////////////////////////////
		JPanel pnlTerciarioSuperiorEsquerdo = new JPanel();
		pnlTerciarioSuperiorEsquerdo.setBackground(SystemColor.inactiveCaption);
		pnlTerciarioSuperiorEsquerdo.setBounds(10, 11, 195, 102);
		pnlPrincipal.add(pnlTerciarioSuperiorEsquerdo);
		pnlTerciarioSuperiorEsquerdo.setLayout(new BorderLayout(0, 0));
		
		JButton btnConsultar = new JButton("Consultar Títulos");
		btnConsultar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnConsultar.setActionCommand("_CONSULTAR");
		btnConsultar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"book_edit.png"));
		btnConsultar.setMnemonic(KeyEvent.VK_C);
		btnConsultar.setToolTipText("consultar títulos adquiridos");
		btnConsultar.addActionListener(acaoBotao);
		btnConsultar.setCursor(Constantes.CURSOR_BOTOES);
		btnConsultar.setBounds(10, 11, 195, 74);
		pnlTerciarioSuperiorEsquerdo.add(btnConsultar);
		
		JButton btnFavoritos = new JButton("favoritos");
		pnlTerciarioSuperiorEsquerdo.add(btnFavoritos, BorderLayout.SOUTH);
		btnFavoritos.setActionCommand("_FAVORITOS");
		btnFavoritos.setIcon(new ImageIcon(Constantes.PATH_ICONS+"heart.png"));
		btnFavoritos.setToolTipText("consultar títulos favoritos");
		btnFavoritos.setMnemonic(KeyEvent.VK_F);
		btnFavoritos.addActionListener(acaoBotao);
		btnFavoritos.setCursor(Constantes.CURSOR_BOTOES);
		btnFavoritos.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		/////////////////////////////////////[ SUPERIOR DIREITO ]/////////////////////////////////////
		JPanel pnlTerciarioSuperiorDireito = new JPanel();
		pnlTerciarioSuperiorDireito.setBackground(SystemColor.inactiveCaption);
		pnlTerciarioSuperiorDireito.setBounds(225, 11, 204, 74);
		pnlPrincipal.add(pnlTerciarioSuperiorDireito);
		pnlTerciarioSuperiorDireito.setLayout(new BorderLayout(0, 0));
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCadastrar.setActionCommand("_CADASTRAR");
		btnCadastrar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"book_add.png"));
		btnCadastrar.setMnemonic(KeyEvent.VK_D);
		btnCadastrar.addActionListener(acaoBotao);
		btnCadastrar.setCursor(Constantes.CURSOR_BOTOES);
		btnCadastrar.setToolTipText("adicionar um novo título");
		btnCadastrar.setBounds(225, 11, 204, 74);
		pnlTerciarioSuperiorDireito.add(btnCadastrar, BorderLayout.CENTER);
		
		/////////////////////////////////////[ INFERIOR DIREITO ]/////////////////////////////////////
		JPanel pnlTerciarioInferiorDireito = new JPanel();
		pnlTerciarioInferiorDireito.setBackground(SystemColor.inactiveCaption);
		pnlTerciarioInferiorDireito.setBounds(225, 96, 204, 110);
		pnlPrincipal.add(pnlTerciarioInferiorDireito);
		pnlTerciarioInferiorDireito.setLayout(new BorderLayout(0, 0));
		
		JButton btnListaDeDesejos = new JButton("Lista de Desejos");
		btnListaDeDesejos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnListaDeDesejos.setActionCommand("_DESEJOS");
		btnListaDeDesejos.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cart.png"));
		btnListaDeDesejos.addActionListener(acaoBotao);
		btnListaDeDesejos.setToolTipText("consultar a lista de desejos");
		btnListaDeDesejos.setMnemonic(KeyEvent.VK_L);
		btnListaDeDesejos.setCursor(Constantes.CURSOR_BOTOES);
		pnlTerciarioInferiorDireito.add(btnListaDeDesejos, BorderLayout.CENTER);
		
		JButton btnGerarRelatorio = new JButton("Gerar Relatório");
		pnlTerciarioInferiorDireito.add(btnGerarRelatorio, BorderLayout.SOUTH);
		btnGerarRelatorio.setToolTipText("visão geral do sistema");
		btnGerarRelatorio.setActionCommand("_RELATORIO");
		btnGerarRelatorio.setIcon(new ImageIcon(Constantes.PATH_ICONS+"application_view_list.png"));
		btnGerarRelatorio.addActionListener(acaoBotao);
		btnGerarRelatorio.setMnemonic(KeyEvent.VK_R);
		btnGerarRelatorio.setCursor(Constantes.CURSOR_BOTOES);
		btnGerarRelatorio.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		JPanel pnlTerciarioInferiorEsquerdo = new JPanel();
		pnlTerciarioInferiorEsquerdo.setBackground(SystemColor.inactiveCaption);
		pnlTerciarioInferiorEsquerdo.setBounds(10, 124, 195, 82);
		pnlPrincipal.add(pnlTerciarioInferiorEsquerdo);
		pnlTerciarioInferiorEsquerdo.setLayout(new BorderLayout(0, 0));
		
		/////////////////////////////////////[ INFERIOR ESQUERDO ]/////////////////////////////////////
		JButton btnEmprestimos = new JButton("Empréstimos");
		btnEmprestimos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEmprestimos.setActionCommand("_EMPRESTIMOS");
		btnEmprestimos.setIcon(new ImageIcon(Constantes.PATH_ICONS+"clock.png"));
		btnEmprestimos.setMnemonic(KeyEvent.VK_E);
		btnEmprestimos.addActionListener(acaoBotao);
		pnlTerciarioInferiorEsquerdo.add(btnEmprestimos, BorderLayout.CENTER);
		btnEmprestimos.setCursor(Constantes.CURSOR_BOTOES);
		btnEmprestimos.setToolTipText("gerenciar empréstimos");
	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	

	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	
	private void abrirConsultar(){
		Consultar_jdialog telaConsultar = new Consultar_jdialog(this, this.usuarioLogado);
		telaConsultar.setLocationRelativeTo(null);
		telaConsultar.setVisible(true);
	}
	
	private void abrirCadastro(){
		Cadastrar_jdialog telaCadastrar = new Cadastrar_jdialog(this,this.usuarioLogado);
		telaCadastrar.setLocationRelativeTo(null);
		telaCadastrar.setVisible(true);
	}
	
	
	private void abrirEmprestimos(){
		Emprestimos_jdialog telaEmprestimos = new Emprestimos_jdialog(this,this.usuarioLogado);
		telaEmprestimos.setLocationRelativeTo(null);
		telaEmprestimos.setVisible(true);
	}
	
	
	private void abrirGerarRelatorio(){
		GerarRelatorio_jdialog telaGerarRelatorio = new GerarRelatorio_jdialog(this, this.usuarioLogado);
		telaGerarRelatorio.setLocationRelativeTo(null);
		telaGerarRelatorio.setVisible(true);
	}
	
	
	private void abrirListaDesejos(){
		ListaDesejos_jdialog telaListaDesejos = new ListaDesejos_jdialog(this,this.usuarioLogado);
		telaListaDesejos.setLocationRelativeTo(null);
		telaListaDesejos.setVisible(true);
	}
	
	
	private void abrirFavoritos(){
		Favoritos_jdialog telaFavoritos = new Favoritos_jdialog(this, this.usuarioLogado);
		telaFavoritos.setLocationRelativeTo(null);
		telaFavoritos.setVisible(true);
	}

	
	private void sairConta(){
		this.usuarioLogado.fazerLogout();
		dispose();
		new Secundaria_jframe(true);
	}
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
		        case "SOBRE": Constantes.abrirSobre(null); break;
		        case "SAIR": sairConta(); break;
		        case "ATUALIZAR": usuarioLogado.atualizarUsuario(); break;
		    	case "FECHAR": System.exit(0); break;
	        
	        	case "CONSULTAR": abrirConsultar(); break;
	        	case "CADASTRAR": abrirCadastro(); break;
	        	case "EMPRESTIMOS": abrirEmprestimos(); break;
	        	case "RELATORIO": abrirGerarRelatorio(); break;
	        	case "DESEJOS": abrirListaDesejos(); break;
	        	case "FAVORITOS": abrirFavoritos(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
}


//		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();  
//		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
//		cbxCategorias.setRenderer(UIResource);
//		cbxCategorias.addItem("categoria");
// ####### recuperar do BD #######
//		String[] strCategorias = new String[] {"categoria", "técnico", "romance", ação/aventura", "religioso", "revista", "didático", "biográfico"};

