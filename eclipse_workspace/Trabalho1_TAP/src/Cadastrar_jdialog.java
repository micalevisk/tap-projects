import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.text.AbstractDocument;
import javax.swing.text.NumberFormatter;

import com.toedter.calendar.JDateChooser;

import extra.DocumentSizeFilter;
import extra.JTextFieldComBrancos;
import extra.KeyLivro;
import extra.dataValida;
import model.bean.Config;
import model.bean.Livro;
import model.bean.Usuario;
import model.dao.ConfigDAO;
import extra.Constantes;
/**
 * β - a janela dedicada ao cadastramento de novos títulos	[SEGUNDA dJANELA]
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 26 de dez de 2016
 */
public class Cadastrar_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private final Font lblFONTE = new Font("Ubuntu Light", Font.BOLD, 11);
	private final Font chxFONTE = new Font("Tahoma", Font.PLAIN, 12);

	private JTextFieldComBrancos txtNomeLivro;	private String txtNomeLivro_placeholder="digite aqui o nome do livro";//*
	private JTextFieldComBrancos txtAutor;		private String txtAutor_placeholder="digite aqui o nome do(s) autor(es)";//*
	private JSpinner spnEdicao;//*
	
//	protected String[] strCategorias = new String[] {"categoria", "técnico", "romance", "ação/aventura", "religioso", "revista", "didático", "biográfico"};// ## do banco de dados
	protected Vector<String> strCategorias = new Vector<>();
	private JComboBox cbxCategoria;//*
	private JTextArea txtarDescricao;
	private JRadioButton rbtnEmprestado, rbtnFavorito, rbtnDesejado, rbtnComprado;
	
	private String strFormatoData = "dd/MM/yyyy";
	private String textAquisicao  = "Dia da Aquisição";
	private String textEmprestimo = "Dia do Empréstimo";
	private JLabel lblDiaDevolucao;
	private JLabel lblDiaAquisicao;
	JDateChooser dcrDiaAquisicao, dcrDiaDevolucao;
	JButton btnCadastrar;
	
	Usuario usuarioLogado = null;
	Livro livroCadastrado = null; /// NULL indica que não há um livro novo (ação cancelada ou deu errada)
	Config configsAtualizadas = null;
	//////////////////////////////////////////////////////////////////////////////////////////////

	/*
	public static void main(String[] args) {
		try {
			Cadastrar_jdialog dialog = new Cadastrar_jdialog(new JFrame(),true);
			dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/

	
	/**
	 * Recupera os dados importantes para a tabela no BD.<br>
	 * KeyLivro.NOME: nome do título 	(String)<br>
	 * KeyLivro.AUTOR: autor do título	(String)<br>
	 * KeyLivro.EDICAO: edição do título	(String)<br>
	 * KeyLivro.DESCRICAO: descrição sobre o título	(String)<br>
	 * KeyLivro.CATEGORIA: a categoria escolhida		(String)<br>
	 * KeyLivro.EMPRESTADO: indica se o título é emprestado	(Boolean)<br>
	 * KeyLivro.DESEJADO: indica se o título é desejado		(Boolean)<br>
	 * KeyLivro.FAVORITO: indica se o título é favorito		(Boolean)<br>
	 * KeyLivro.AQUISICAO: dia de aquisição						(String)<br>
	 * KeyLivro.DEVOLUCAO: dia da devolução, se for emprestado	(String)<br>
	 * KeyLivro.TIPO: identificação do grupo do livro	(KeyLivro)
	 * @return A tabela Hash que contém todos os dados úteis com as chaves acima.
	 */
	private HashMap<KeyLivro, Object> getDadosUteis(){
		boolean ehEmprestado = rbtnEmprestado.isSelected(); // para não ter que fazer a mesma pergunta duas vezes
		return new HashMap<KeyLivro,Object>() 
		{{
			put(KeyLivro.NOME, txtNomeLivro.getText() );
			put(KeyLivro.AUTOR, txtAutor.getText() );
			put(KeyLivro.EDICAO, (Integer)spnEdicao.getValue());
			put(KeyLivro.DESCRICAO, txtarDescricao.getText() );
			put(KeyLivro.CATEGORIA, cbxCategoria.getSelectedItem().toString().toLowerCase() );
			put(KeyLivro.EHEMPRESTADO, ehEmprestado );
			put(KeyLivro.EHDESEJADO, new Boolean(rbtnDesejado.isSelected()) );
			put(KeyLivro.EHFAVORITO, new Boolean(rbtnFavorito.isSelected()) );
			put(KeyLivro.AQUISICAO, DateFormat.getDateInstance().format(dcrDiaAquisicao.getDate()) );
			put(KeyLivro.DEVOLUCAO, (ehEmprestado) ? DateFormat.getDateInstance().format(dcrDiaDevolucao.getDate()) : null );
			put(KeyLivro.TIPO, tipoDoLivroCadastrado());
		}};
	}
	

	public Cadastrar_jdialog(JFrame parent, Usuario usuarioLogado){
		this(parent, usuarioLogado, true);
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public Cadastrar_jdialog(JFrame parent, Usuario usuarioLogado, boolean modal){
		super(parent, modal);
		
		this.usuarioLogado = usuarioLogado;
		this.configsAtualizadas = usuarioLogado.getConfigsUsuario();
		this.strCategorias = configsAtualizadas.getCategoriasComoVector();
		this.strCategorias.add("CATEGORIA");
		
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		initComponents();
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	private void initComponents() {
		String textTitulo = "Cadastrar Um Novo Título";
		setTitle(textTitulo);
		setResizable(false);
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 509, 361);
		
		JScrollPane scrpnPrincipal = new JScrollPane();
		scrpnPrincipal.setBounds(0, 0, 434, 261);
		scrpnPrincipal.setBorder(null);
		getContentPane().add(scrpnPrincipal);
		
		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(new Color(245, 245, 245));
		pnlPrincipal.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(95, 160, 160)));
		pnlPrincipal.setLayout(null);
		scrpnPrincipal.setViewportView(pnlPrincipal);
		
		JLabel lblTituloJanela = new JLabel(textTitulo);
		lblTituloJanela.setEnabled(false);
		lblTituloJanela.setFont(new Font("Ubuntu Mono", Font.PLAIN, 14));
		lblTituloJanela.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloJanela.setBounds(22, 11, 457, 14);
		pnlPrincipal.add(lblTituloJanela);
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		CheckTextField alternarBotaoEntrar = new CheckTextField();
		
		////////////////////////////////////////////[ SOBRE O TÍTULO ]////////////////////////////////////////////
		JPanel pnlSobreOTitulo = new JPanel();
		pnlSobreOTitulo.setForeground(UIManager.getColor("Button.foreground"));
		pnlSobreOTitulo.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "<HTML><B>sobre o título</B></HTML>", TitledBorder.RIGHT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlSobreOTitulo.setBackground(new Color(245, 245, 245));
		pnlSobreOTitulo.setBounds(22, 36, 281, 111);
		pnlPrincipal.add(pnlSobreOTitulo);
		pnlSobreOTitulo.setLayout(null);
		
		
		///////////////////[ RÓTULOS ]///////////////////
		JLabel lblNomeDoLivro = new JLabel("Nome do título");
		lblNomeDoLivro.setHorizontalAlignment(SwingConstants.LEFT);
		lblNomeDoLivro.setFont(lblFONTE);
		lblNomeDoLivro.setBounds(10, 11, 256, 14);
		pnlSobreOTitulo.add(lblNomeDoLivro);
		
		JLabel lblAutor = new JLabel("Autor(es)");
		lblAutor.setHorizontalAlignment(SwingConstants.LEFT);
		lblAutor.setFont(lblFONTE);
		lblAutor.setBounds(88, 58, 178, 14);
		pnlSobreOTitulo.add(lblAutor);
		
		JLabel lblEdicao = new JLabel("Edição");
		lblEdicao.setVerticalAlignment(SwingConstants.TOP);
		lblEdicao.setHorizontalAlignment(SwingConstants.LEFT);
		lblEdicao.setFont(lblFONTE);
		lblEdicao.setBounds(10, 58, 43, 14);
		pnlSobreOTitulo.add(lblEdicao);
		
		
		//////////////////////////////////////////[ DATAS ]//////////////////////////////////////////
		JPanel pnlDatas = new JPanel();
		pnlDatas.setBackground(new Color(245, 245, 245));
		pnlDatas.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "<HTML><B>datas</B></HTML>", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlDatas.setBounds(313, 36, 166, 111);
		pnlPrincipal.add(pnlDatas);
		pnlDatas.setLayout(null);
		
		lblDiaAquisicao = new JLabel(textAquisicao);
		lblDiaAquisicao.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiaAquisicao.setFont(lblFONTE);
		lblDiaAquisicao.setBounds(10, 14, 146, 14);
		pnlDatas.add(lblDiaAquisicao);
		
		dcrDiaAquisicao = new JDateChooser(new Date());// definir dia atual
		dcrDiaAquisicao.setBorder(null);
		dcrDiaAquisicao.setBounds(10, 29, 146, 20);
		dcrDiaAquisicao.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent e){
				toggleBtnCadastrar();
			}
		});
		pnlDatas.add(dcrDiaAquisicao);
		
		lblDiaDevolucao = new JLabel("Dia da Devolução");
		lblDiaDevolucao.setEnabled(false);
		lblDiaDevolucao.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiaDevolucao.setFont(lblFONTE);
		lblDiaDevolucao.setBounds(10, 65, 146, 14);
		pnlDatas.add(lblDiaDevolucao);
		
		dcrDiaDevolucao = new JDateChooser();
		dcrDiaDevolucao.setEnabled(false);
		dcrDiaDevolucao.setToolTipText("se o título for emprestado");
		dcrDiaDevolucao.setBorder(null);
		dcrDiaDevolucao.setBounds(10, 80, 146, 20);
		dcrDiaDevolucao.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent e){
				toggleBtnCadastrar();
			}
		});
		pnlDatas.add(dcrDiaDevolucao);
		

		/////////////////////////////////////[ BOTÕES PRINCIPAIS ]/////////////////////////////////////
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setEnabled(false);
		btnCadastrar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"add.png"));
		btnCadastrar.setActionCommand("_CADASTRAR");
		btnCadastrar.setMnemonic(KeyEvent.VK_C);
		btnCadastrar.addActionListener(acaoBotao);
		btnCadastrar.setCursor(Constantes.CURSOR_BOTOES);
		btnCadastrar.setForeground(new Color(0, 128, 0));
		btnCadastrar.setFont(new Font("Tw Cen MT", Font.PLAIN, 14));
		btnCadastrar.setToolTipText("cadastrar título");
		btnCadastrar.setBounds(353, 166, 126, 128);
		pnlPrincipal.add(btnCadastrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cancel.png"));
		btnCancelar.setActionCommand("_CANCELAR");
		btnCancelar.setMnemonic(KeyEvent.VK_R);
		btnCancelar.addActionListener(acaoBotao);
		btnCancelar.setCursor(Constantes.CURSOR_BOTOES);
		btnCancelar.setForeground(new Color(128, 0, 0));
		btnCancelar.setToolTipText("fechar");
		btnCancelar.setFont(new Font("Tw Cen MT", Font.PLAIN, 14));
		btnCancelar.setBounds(353, 305, 126, 43);
		pnlPrincipal.add(btnCancelar);
		
		////////////////////////[ CAMPOS ]////////////////////////
		txtNomeLivro = new JTextFieldComBrancos(txtNomeLivro_placeholder);
		txtNomeLivro.setBounds(10, 25, 256, 26);
		txtNomeLivro.setBorder(Constantes.brITEM_OBRIGATORIO);
		txtNomeLivro.addCaretListener(alternarBotaoEntrar);
		pnlSobreOTitulo.add(txtNomeLivro);
		txtNomeLivro.setColumns(10);
		
		txtAutor = new JTextFieldComBrancos(txtAutor_placeholder);
		txtAutor.setColumns(10);
		txtAutor.setBorder(Constantes.brITEM_OBRIGATORIO);
		txtAutor.addCaretListener(alternarBotaoEntrar);
		txtAutor.setBounds(88, 74, 178, 26);
		pnlSobreOTitulo.add(txtAutor);
		
		spnEdicao = new JSpinner();
		spnEdicao.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spnEdicao.setBounds(10, 74, 43, 26);
		// apenas números
		JFormattedTextField txtonlynumbers = ((JSpinner.NumberEditor) spnEdicao.getEditor()).getTextField();
		((NumberFormatter) txtonlynumbers.getFormatter()).setAllowsInvalid(false);
		// texto centralizado
		JComponent editor = spnEdicao.getEditor();
		JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor)editor;
		spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		pnlSobreOTitulo.add(spnEdicao);
		

		/////////////////////////////////////[ GERAL ]/////////////////////////////////////
		JPanel pnlDetalhes = new JPanel();
		pnlDetalhes.setToolTipText("informações gerais");
		pnlDetalhes.setBackground(new Color(245, 245, 245));
		pnlDetalhes.setBorder(new TitledBorder(null, "<HTML><B>geral</B></HTML>", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDetalhes.setBounds(22, 158, 321, 192);
		pnlPrincipal.add(pnlDetalhes);
		pnlDetalhes.setLayout(null);
		
		JScrollPane scrpnlBoxParaDescricao = new JScrollPane();
		scrpnlBoxParaDescricao.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrpnlBoxParaDescricao.setBounds(10, 22, 148, 157);
		pnlDetalhes.add(scrpnlBoxParaDescricao);
		
		//////////////////////////////[ RADIOBUTTONS ]//////////////////////////////
		JPanel pnlCheckboxes = new JPanel();
		pnlCheckboxes.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		pnlCheckboxes.setBackground(SystemColor.controlHighlight);
		pnlCheckboxes.setBounds(168, 62, 143, 117);
		pnlDetalhes.add(pnlCheckboxes);
		pnlCheckboxes.setLayout(null);

		rbtnEmprestado = new JRadioButton("Emprestado");
		rbtnEmprestado.setToolTipText("o título é emprestado?");
		rbtnEmprestado.setMnemonic(KeyEvent.VK_E);
		rbtnEmprestado.setActionCommand("_RADIOBUTTON");
		rbtnEmprestado.addActionListener(acaoBotao);
		rbtnEmprestado.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnEmprestado.setBounds(6, 6, 131, 23);
		rbtnEmprestado.setBackground(null);
		rbtnEmprestado.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnEmprestado);

		rbtnDesejado = new JRadioButton("Desejado");
		rbtnDesejado.setToolTipText("adicioná-lo na lista de desejos?");
		rbtnDesejado.setMnemonic(KeyEvent.VK_D);
		rbtnDesejado.setActionCommand("_RADIOBUTTON");
		rbtnDesejado.addActionListener(acaoBotao);
		rbtnDesejado.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnDesejado.setBackground((Color) null);
		rbtnDesejado.setBounds(6, 33, 131, 23);
		rbtnDesejado.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnDesejado);

		rbtnFavorito = new JRadioButton("Favorito");
		rbtnFavorito.setToolTipText("é o seu título favorito?");
		rbtnFavorito.setMnemonic(KeyEvent.VK_F);
		rbtnFavorito.setActionCommand("_RADIOBUTTON");
		rbtnFavorito.addActionListener(acaoBotao);
		rbtnFavorito.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnFavorito.setBounds(6, 59, 131, 23);
		rbtnFavorito.setBackground(null);
		rbtnFavorito.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnFavorito);
		
		rbtnComprado = new JRadioButton("Apenas Comprado");
		rbtnComprado.setSelected(true);
		rbtnComprado.setMnemonic(KeyEvent.VK_A);
		rbtnComprado.setToolTipText("não é favorito");
		rbtnComprado.setActionCommand("_RADIOBUTTON");
		rbtnComprado.addActionListener(acaoBotao);
		rbtnComprado.setHorizontalAlignment(SwingConstants.LEFT);
		rbtnComprado.setBounds(6, 87, 131, 23);
		rbtnComprado.setBackground(null);
		rbtnComprado.setFont(chxFONTE);
		pnlCheckboxes.add(rbtnComprado);

		ButtonGroup grupoRadioButtons = new ButtonGroup();
		grupoRadioButtons.add(rbtnEmprestado);
		grupoRadioButtons.add(rbtnDesejado);
		grupoRadioButtons.add(rbtnFavorito);
		grupoRadioButtons.add(rbtnComprado);
		
		
		//////////////////////////////////[ GERAL ]//////////////////////////////////
		txtarDescricao = new JTextArea();
		txtarDescricao.setFont(new Font("Nirmala UI", Font.PLAIN, 12));
		txtarDescricao.setBackground(new Color(255, 255, 255));
		txtarDescricao.setLineWrap(true);
		txtarDescricao.setWrapStyleWord(true);
		txtarDescricao.setToolTipText("escreva algo sobre este título (ex: editora, resumo, etc)");
		scrpnlBoxParaDescricao.setViewportView(txtarDescricao);
		// limitando quantidade de caracteres:
		AbstractDocument pDoc = (AbstractDocument)txtarDescricao.getDocument();
		pDoc.setDocumentFilter(new DocumentSizeFilter(Constantes.MAX_CHARS_DESC));
		
		JLabel lblDescricao = new JLabel("Descrição");
		lblDescricao.setBackground(new Color(245, 245, 245));
		lblDescricao.setFont(lblFONTE);
		lblDescricao.setHorizontalAlignment(SwingConstants.CENTER);
		scrpnlBoxParaDescricao.setColumnHeaderView(lblDescricao);
		
		cbxCategoria = new JComboBox(strCategorias);
		cbxCategoria.setToolTipText("aperte ENTER para efetivar");
		cbxCategoria.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				if(ie.getStateChange() == ItemEvent.SELECTED){
					toggleBtnCadastrar();
				}
			}
		});
		cbxCategoria.setEditable(true);
//		cbxCategoria.setModel(new DefaultComboBoxModel(strCategorias));
		cbxCategoria.setBorder(Constantes.brITEM_OBRIGATORIO);
		cbxCategoria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbxCategoria.setBounds(168, 22, 143, 29);
		pnlDetalhes.add(cbxCategoria);
		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();  
		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
		cbxCategoria.setRenderer(UIResource);
		cbxCategoria.setSelectedIndex(strCategorias.size()-1);
		
	
	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	
	
	
	public Livro criarLivro(){
		HashMap<KeyLivro, Object> dadosUteis = getDadosUteis();
		
		return new Livro(-1, /// atributo irrelevante para o cadastro
				this.usuarioLogado.getLogin(), 
				(Boolean) dadosUteis.get(KeyLivro.EHDESEJADO), 
				(Boolean) dadosUteis.get(KeyLivro.EHFAVORITO), 
				(String) dadosUteis.get(KeyLivro.NOME), 
				(String) dadosUteis.get(KeyLivro.AUTOR), 
				(String) dadosUteis.get(KeyLivro.DESCRICAO), 
				(Integer) dadosUteis.get(KeyLivro.EDICAO), 
				(String) dadosUteis.get(KeyLivro.CATEGORIA), 
				(String) dadosUteis.get(KeyLivro.AQUISICAO), 
				(String) dadosUteis.get(KeyLivro.DEVOLUCAO),
				(KeyLivro) dadosUteis.get(KeyLivro.TIPO));
	}
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private KeyLivro tipoDoLivroCadastrado(){
		return rbtnDesejado.isSelected() ? KeyLivro.DESEJADOS : 
				rbtnEmprestado.isSelected() ? KeyLivro.EMPRESTADOS :
						KeyLivro.COMPRADOS; // tratar verificar se é favorito também (pelo atributo do livro)
	}
	
	
	private boolean camposValidos() {
		boolean validoNome = !txtNomeLivro.textIsEmpty();
		boolean validoAutor = !txtAutor.textIsEmpty();
		boolean validoCategoria = !(cbxCategoria.getSelectedIndex() == this.strCategorias.size()-1);
		boolean validoDiaAquisicao = dataValida.isValidDate(dcrDiaAquisicao, strFormatoData);
		boolean validoDiaDevolucao = (rbtnEmprestado.isSelected()) ? dataValida.isValidDate(dcrDiaDevolucao, strFormatoData) : true;

		txtNomeLivro.setBorder((validoNome) ? Constantes.brPADRAO : Constantes.brITEM_OBRIGATORIO);
		txtAutor.setBorder((validoAutor) ? Constantes.brPADRAO : Constantes.brITEM_OBRIGATORIO);
		cbxCategoria.setBorder((validoCategoria) ? null : Constantes.brITEM_OBRIGATORIO);

		return validoDiaAquisicao && validoDiaDevolucao && validoNome && validoAutor && validoCategoria ;
	}
	
	private void toggleBtnCadastrar(){
			btnCadastrar.setEnabled(camposValidos());
	}
	
	private void toggleDiaDevolucao(){
		boolean estaMarcado;
		if(estaMarcado = this.rbtnEmprestado.isSelected()) this.lblDiaAquisicao.setText(textEmprestimo);
		else this.lblDiaAquisicao.setText(textAquisicao);
		this.lblDiaDevolucao.setEnabled(estaMarcado);
		this.dcrDiaDevolucao.setEnabled(estaMarcado);
		toggleBtnCadastrar();
	}

	
	private void efetivarCadastro(){
		if(!camposValidos()) return;///FIXME quase redundante
		try{
			livroCadastrado = criarLivro();
			
			if( livroCadastrado.getDAO().adicionarLivro(usuarioLogado, livroCadastrado) ){
				configsAtualizadas.getDAO().anexarNovaCategoria(configsAtualizadas, livroCadastrado.getCategoria());
				
				///ATUALIZANDO A LISTA LOCAL DO Usuario:
				this.usuarioLogado.setConfigsUsuario(this.configsAtualizadas);
				HashMap<KeyLivro, List<Livro>> livrosUsuarioLogado = this.usuarioLogado.getLivrosUsuario();
				KeyLivro tipo = livroCadastrado.getTipo();
				livrosUsuarioLogado.get(tipo).add( livroCadastrado );
				if( livroCadastrado.isFavorito() ) livrosUsuarioLogado.get(KeyLivro.FAVORITOS).add(livroCadastrado);
				
				this.dispose();
			}else{
				JOptionPane.showMessageDialog(null, "Um título semelhante e do mesmo autor já foi cadastrado!", "Erro Ao Cadastrar", JOptionPane.ERROR_MESSAGE);
				txtNomeLivro.requestFocusInWindow();
			}
			
		}catch(NullPointerException ex){
			System.out.println("ERRO efetivarCadastro() "+ex.getMessage());
		}
		
	}
	
	private void cancelarCadastro(){
		this.usuarioLogado = null;
		this.livroCadastrado = null;
		
		this.dispose();
	}
	
	

	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class CheckTextField implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent e) {
			toggleBtnCadastrar();
		}
	}
	
	
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "EMPRESTADO": toggleDiaDevolucao(); break;
	        	case "CADASTRAR": efetivarCadastro(); break;
	        	case "CANCELAR": cancelarCadastro(); break;
	        	case "RADIOBUTTON": toggleDiaDevolucao(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
}
