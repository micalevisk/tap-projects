import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import extra.JAdvancedTable;
import extra.JTextFieldComBrancos;
import extra.KeyLivro;
import model.bean.Livro;
import model.bean.Usuario;
import model.dao.LivroDAO;
import extra.Constantes;
/**
* η - exibir os títulos marcados como 'favorito'	[SEXTA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 26 de dez de 2016
*/
public class Favoritos_jdialog extends JDialog {
	//////////////////////////////////////////////////////////////////////////////////////////////
	protected String[] nomeColunas = { "#", 	"Nome", "Autor(es)","Descrição","Edição" }; // informações básicas
	private int col_id=0, col_nome=1, col_autor=2, col_descricao=3, col_edicao=4;
    
    private JAdvancedTable tblLivros;
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(null,nomeColunas) {
		boolean[] columnEditables = {false, true, true, false, false};
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {Integer.class,String.class,String.class,String.class,String.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() {
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment( JLabel.CENTER );
	        setToolTipText("editar detalhes");
	        return this;
	    }
	};
	
    
    private JButton btnSalvarAlteracoes;
    private JButton btnRemover;
    private JTextFieldComBrancos txtAutorProcurado;
    
    private Usuario usuarioLogado;
    private List<Livro> livrosFavoritos;
    private List<Integer> indicesRemovidos;
	//////////////////////////////////////////////////////////////////////////////////////////////


    public Favoritos_jdialog(JFrame parent, Usuario usuarioLogado){
    	this(parent, usuarioLogado, true);
    }
    
    /**
	 * @wbp.parser.constructor
	 */
	public Favoritos_jdialog(JFrame parent, Usuario usuarioLogado, boolean modal){
		super(parent, modal);
		setResizable(true);
		initComponents();
		
    	this.usuarioLogado = usuarioLogado;
    	this.livrosFavoritos = usuarioLogado.getLivrosUsuario().get(KeyLivro.FAVORITOS);
    	atualizarDadosTabela(tblLivros, livrosFavoritos);
	}
	
	
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	public void initComponents() {
		setType(Type.POPUP);
		setTitle("Consultar Títulos Favoritos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 558, 389);
		setLocationRelativeTo(null);
		JPanel  pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.inactiveCaption);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlPrincipal);

		
		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(new Color(255, 255, 255));
		pnlPrincipal.add(pnlBotoes, BorderLayout.NORTH);
		pnlBotoes.setLayout(new BorderLayout(0, 0));
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuSecundario = new JMenuBar();
		setJMenuBar(mnbMenuSecundario);
		
		JMenu mnArquivo = new JMenu();
		mnArquivo.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cog.png"));
		mnArquivo.setMnemonic(KeyEvent.VK_A);
		mnbMenuSecundario.add(mnArquivo);
		
		JMenuItem mniCancelar = new JMenuItem("Cancelar");
		mniCancelar.setActionCommand("_CANCELAR");
		mniCancelar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cancel.png"));
		mniCancelar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		mniCancelar.addActionListener(acaoBotao);
		mnArquivo.add(mniCancelar);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(Constantes.PATH_ICONS+"cross.png"));
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setToolTipText("fechar programa");
		mnArquivo.add(mniFechar);
		
		
		////////////////////////[ BOTÕES ]////////////////////////
		btnSalvarAlteracoes = new JButton("Salvar Alterações");
		btnSalvarAlteracoes.setToolTipText("registrar no banco");
		btnSalvarAlteracoes.setActionCommand("_SALVAR");
		btnSalvarAlteracoes.setMnemonic(KeyEvent.VK_S);
		btnSalvarAlteracoes.addActionListener(acaoBotao);
		btnSalvarAlteracoes.setCursor(Constantes.CURSOR_BOTOES);
		btnSalvarAlteracoes.setForeground(new Color(0, 128, 0));
		pnlBotoes.add(btnSalvarAlteracoes, BorderLayout.WEST);
		
		String textRemoverDaLista = "Remover (Da Lista) Selecionados";
		btnRemover = new JButton(textRemoverDaLista);
		btnRemover.setActionCommand("_REMOVER" );
//		btnRemover.setMnemonic(KeyEvent.VK_R);// FIXME erro quando o table#row é ímpar {Exception in thread "AWT-EventQueue-0" java.lang.ArrayIndexOutOfBoundsException: 2 >= 2} 
		btnRemover.addActionListener(acaoBotao);
		btnRemover.setCursor(Constantes.CURSOR_BOTOES);
		btnRemover.setEnabled(false);
		btnRemover.setForeground(new Color(128, 0, 0));
		pnlBotoes.add(btnRemover, BorderLayout.CENTER);
		
		txtAutorProcurado = new JTextFieldComBrancos("buscar autor");
		txtAutorProcurado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent ke) {
				JTextFieldComBrancos campo = (JTextFieldComBrancos) ke.getSource();
				String autor = campo.textIsEmpty() ? null : campo.getText();
				buscar(autor, livrosFavoritos, tblLivros, usuarioLogado);
			}
		});
		pnlBotoes.add(txtAutorProcurado, BorderLayout.EAST);
		txtAutorProcurado.setHorizontalAlignment(SwingConstants.LEFT);
		txtAutorProcurado.setColumns(10);
		
		
		/////////////////////////[ FUNDO DA TABELA ]/////////////////////// 
		JScrollPane scrpnlTabela = new JScrollPane();
		scrpnlTabela.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlPrincipal.add(scrpnlTabela);
		scrpnlTabela.addMouseListener(new MouseAdapter() { // unselect when clicked out table
			@Override
			public void mouseClicked(MouseEvent me) {
				tblLivros.clearSelection();
			}
		});

		/////////////////////////[ TABELA ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivros = new JAdvancedTable(modelTabelaLivros);
		tblLivros.getTableHeader().setReorderingAllowed(false); // disable user column draggin
		tblLivros.setRowSorter(new TableRowSorter<DefaultTableModel>(modelTabelaLivros)); // definindo ordenação
		tblLivros.getRowSorter().toggleSortOrder(col_id); // definindo ordenação padrão pela coluna mestre
		tblLivros.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivros.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivros.setBackground(SystemColor.textHighlightText);
		tblLivros.setDefaultRenderer(Integer.class, centerRenderer);
		tblLivros.getColumnModel().getColumn(col_id).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(col_id).setCellRenderer(tableRendererParaID); // definir fonte e cursor para a coluna mestre
		scrpnlTabela.setViewportView(tblLivros);
		tblLivros.addMouseListener(new MouseAdapter() { // definir ação quando clicar duas vezes na coluna mestre
			@Override
			public void mousePressed(MouseEvent me) {
				Point p = me.getPoint();
				int row = tblLivros.rowAtPoint(p);
				int col = tblLivros.columnAtPoint(p);
				if(col == col_id){
//					JOptionPane.showMessageDialog(null, "MOSTRAR informações avançadas DO LIVRO DE id = " + tblLivros.getIdFromRow(row, col_id));
					chamarDetalhes( tblLivros.getSelectedRow() );
				}
			}
		});
		tblLivros.getSelectionModel().addListSelectionListener(new ListSelectionListener() { // habilitar somente se tiver alguma linha selecionada
			@Override
	        public void valueChanged(ListSelectionEvent e){ 
	            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	            btnRemover.setEnabled(!lsm.isSelectionEmpty());
	            
	            int qtdSelecionadas = tblLivros.getSelectedRows().length;
	            btnRemover.setText(textRemoverDaLista+ (qtdSelecionadas>0 ? (" ("+qtdSelecionadas+")") : ""));
	        }
		});
		tblLivros.addMouseMotionListener(new MouseMotionAdapter() { // definir cursor quando estiver sobre a coluna mestre
			@Override
			public void mouseMoved(MouseEvent mv) {
				int col = tblLivros.columnAtPoint(mv.getPoint());
				if(col == col_id) setCursor(Constantes.CURSOR_BOTOES);
				else setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});
		// definir clique quando apertar ENTER numa linha da tabela:
		tblLivros.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "_EDITAR");
		tblLivros.getActionMap().put("_EDITAR",new AbstractAction() { 
			@Override
			public void actionPerformed(ActionEvent ae) {
				chamarDetalhes( tblLivros.getSelectedRow() );
			}
		});
		tblLivros.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
		    @Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
		        JLabel renderedLabel = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        renderedLabel.setHorizontalAlignment(JLabel.CENTER);
		        renderedLabel.setBackground(row % 2 == 0 ? new Color(230, 249, 255) : Color.WHITE);
		        
		        return renderedLabel;
		    }
		});


	}
	// %%%%%%%%%%%%%%%%%%%%%%%%% //
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void chamarDetalhes(int indice){
		try{
			Livro lclicado = this.livrosFavoritos.get(indice);

			Detalhes_jdialog detalhes = new Detalhes_jdialog(this.usuarioLogado, lclicado, indice);
			detalhes.setLocationRelativeTo(null);
			detalhes.setVisible(true);

			if(lclicado.isFavorito()) atualizarLivroTabela(lclicado, indice);
			else /// caso o livro tenha "migrado" de lista
				atualizarDadosTabela(this.tblLivros, this.livrosFavoritos);
			
		}catch(ArrayIndexOutOfBoundsException ex){
			System.out.println("ERRO chamarDetalhes() "+ex.getMessage());
		}
	}
	
	/**
	 * Filtra as linhas de uma tabela pelo atributo {@link Livro#getAutor()}.<br>
	 * Exibe todos os elementos da lista caso o <code>autor</code> seja <code>NULL</code>.
	 * @param autor O nome do autor que será buscado.
	 * @param titulosAlvo A Lista de livros em que será efetuada a busca.
	 * @param tabelaAlvo A tabela que será alterada com essa busca.
	 * @param usuarioAlvo
	 */
	private void buscar(String autor, List<Livro> titulosAlvo, JTable tabelaAlvo, Usuario usuarioAlvo){
		if(autor == null) atualizarDadosTabela(tabelaAlvo, titulosAlvo);
		else atualizarDadosTabela(tabelaAlvo, usuarioAlvo.filtarLivrosAutor(titulosAlvo, autor));
	}
	
	/**
	 * Carrega os dados de uma lista de Livro para a visualização na JTable.<br>
	 * Utiliza a lista de livros nessa classe para preencher a JTable.
	 * @param tabela A JTable alvo da alteração. 
	 * @param dados A lista dos títulos que serão carregados.
	 */
	private void atualizarDadosTabela(JTable tabela, List<Livro> dados){
		DefaultTableModel modelTabela = (DefaultTableModel) tabela.getModel();
		modelTabela.setRowCount(0); // zerando
		
		Object[] row = new Object[modelTabela.getColumnCount()];
		for(int i=0; i < dados.size(); ++i){
			Livro l = dados.get(i);
			if(l != null){
				row[col_id] = i+1;
				row[col_nome] = l.getNome();
				row[col_autor] = l.getAutor();
				row[col_descricao] = l.getDescricao();
				row[col_edicao] = Integer.toString( l.getEdicao() );
			}
			
			modelTabela.addRow(row);
		}
	}
	
	/**
	 * Atualiza os dados expostos na tabela a partir dos valores contido no Livro e seu índice.
	 * @param l O livro que foi, provavelmente, alterado
	 * @param indice Corresponde ao número da linha na matriz de linhas (ou, id - 1)
	 */
	private void atualizarLivroTabela(Livro l, int indice){
		tblLivros.setValueAt(l.getNome(),indice,col_nome);
		tblLivros.setValueAt(l.getAutor(),indice,col_autor);
		tblLivros.setValueAt(l.getDescricao(),indice,col_descricao);
		tblLivros.setValueAt(l.getEdicao(),indice,col_edicao);
	}
	
	/**
	 * Apaga do modelo da tabela as linhas que foram seleciondas.<br>
	 * Define a lista dos índices (row) das linhas removidas.
	 */
	private void removerSelecionados(){
		this.indicesRemovidos = tblLivros.removerLinhasSelecionadas(col_id, modelTabelaLivros);//retira as linhas da tabela
	}
	
	/**
	 * Salva as mudanças feitas diretamente nas células (incluindo remoção), atualizando a lista do Usuario e o BD.
	 */
	private void salvarAlteracoesVisuais(){
		/// percorrer itens da JTable e pra cada índice da tabela acessar
		Livro l;
		LivroDAO ldao = new LivroDAO();

		for(Object[] rowData : tblLivros.getTableData()){
			Integer indice = Integer.valueOf( (Integer) rowData[col_id] ) - 1;
			l = livrosFavoritos.get(indice);
			l.setNome( (String) (rowData[col_nome]) );
			l.setAutor( (String) (rowData[col_autor]) );

			/// atualiza no BD
			ldao.atualizarLivroDoUsuario(usuarioLogado, l);
		}
		
		if((indicesRemovidos != null) && (livrosFavoritos != null)){
			for(int j=0; j < indicesRemovidos.size(); ++j){
				try{
					int i = indicesRemovidos.get(j);
					int idLivroCorrente = livrosFavoritos.get(i).getId();
					
					if( !ldao.apagarLivroUnico(idLivroCorrente) ) return;
					livrosFavoritos.remove(i);
//					usuarioLogado.removerLivroComId(idLivroCorrente, KeyLivro.COMPRADOS); // pois está nas duas listas
					
				}catch(IndexOutOfBoundsException e){
					System.out.println("salvarAlteracoesVisuais " + e.getMessage());
					return;
				}
			}
			
			atualizarDadosTabela(this.tblLivros, this.livrosFavoritos);
		}
	}
	
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
		        case "CANCELAR": dispose(); break;
		        case "FECHAR": System.exit(0); break;
	        
	        	case "SALVAR": salvarAlteracoesVisuais(); break;
	        	case "REMOVER": removerSelecionados(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
	
}
