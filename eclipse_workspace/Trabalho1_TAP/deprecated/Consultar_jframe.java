package deprecated;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import java.awt.event.MouseMotionAdapter;

import extra.JAdvancedTable;

/**
* α - a janela de consultas a tabela de livros	[PRIMEIRA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 23 de dez de 2016
*/
public class Consultar_jframe extends JFrame {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private final Cursor CURSOR_BOTOES = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	
	private int colMestre = 0; // a coluna do id
	// CARREGAR DADOS DA TABELA
	protected String[] nomeColunas ={ "id", "Nome", "Autor","Edição" }; // informações básicas
	protected Object[][] dadosUteis={{ "1", "B", 	"C",	new Integer(1) },
								    { "2", 	"B", 	"C",	new Integer(1) },
								    { "3", 	"B", 	"C",	new Integer(1) },
								    { "4", 	"B", 	"C",	new Integer(1) },
								    { "5", 	"B", 	"C",	new Integer(1) }};
    
    private JAdvancedTable tblLivros;
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(dadosUteis,nomeColunas) {
		boolean[] columnEditables = {false, true, true, true}; // a primeira coluna não é editável
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {String.class,String.class,String.class,Integer.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() { // (c) http://stackoverflow.com/questions/16113950/jtable-change-column-font
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment( JLabel.CENTER );
	        setToolTipText("editar detalhes");
	        return this;
	    }
	};
	
    private JButton btnSalvarAlteracoes;
    private JButton btnBuscar;
    private JButton btnApagarSelecionados;
	//////////////////////////////////////////////////////////////////////////////////////////////

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Consultar_jframe frame = new Consultar_jframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Consultar_jframe() {
		setTitle("Consultar Títulos Adquiridos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 558, 389);
		setLocationRelativeTo(null);
		JPanel  pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.inactiveCaption);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlPrincipal);

		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(new Color(255, 255, 255));
		pnlPrincipal.add(pnlBotoes, BorderLayout.NORTH);
		pnlBotoes.setLayout(new BorderLayout(0, 0));
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		////////////////////////[ BOTÕES ]////////////////////////
		btnSalvarAlteracoes = new JButton("Salvar Alterações");
		btnSalvarAlteracoes.setActionCommand("_SALVAR");
		btnSalvarAlteracoes.setMnemonic(KeyEvent.VK_S);
		btnSalvarAlteracoes.addActionListener(acaoBotao);
		btnSalvarAlteracoes.setCursor(CURSOR_BOTOES);
		btnSalvarAlteracoes.setForeground(new Color(0, 128, 0));
		pnlBotoes.add(btnSalvarAlteracoes, BorderLayout.WEST);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setActionCommand("_BUSCAR");
		btnBuscar.setMnemonic(KeyEvent.VK_B);
		btnBuscar.addActionListener(acaoBotao);
		btnBuscar.setCursor(CURSOR_BOTOES);
		btnBuscar.setForeground(new Color(0, 128, 128));
		pnlBotoes.add(btnBuscar, BorderLayout.EAST);
		
		String textApagarSelecionados = "Apagar Selecionados";
		btnApagarSelecionados = new JButton(textApagarSelecionados);
		btnApagarSelecionados.setActionCommand("_DELETAR" );
		btnApagarSelecionados.addActionListener(acaoBotao);
//		btnApagarSelecionados.setMnemonic(KeyEvent.VK_A);
		btnApagarSelecionados.setCursor(CURSOR_BOTOES);
		btnApagarSelecionados.setEnabled(false);
		btnApagarSelecionados.setForeground(new Color(128, 0, 0));
		pnlBotoes.add(btnApagarSelecionados, BorderLayout.CENTER);
		
		
		/////////////////////////[ FUNDO DA TABELA ]/////////////////////// 
		JScrollPane scrpnlTabela = new JScrollPane();
		scrpnlTabela.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlPrincipal.add(scrpnlTabela);
		scrpnlTabela.addMouseListener(new MouseAdapter() { // unselect when clicked out table
			@Override
			public void mouseClicked(MouseEvent me) {
				tblLivros.clearSelection();
			}
		});

		/////////////////////////[ TABELA ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivros = new JAdvancedTable(modelTabelaLivros);
		scrpnlTabela.setViewportView(tblLivros);
		tblLivros.getTableHeader().setReorderingAllowed(false); // disable user column draggin
		tblLivros.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivros.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivros.setBackground(SystemColor.textHighlightText);
		tblLivros.setDefaultRenderer(Integer.class, centerRenderer); 
		tblLivros.getColumnModel().getColumn(3).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(colMestre).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(colMestre).setCellRenderer(tableRendererParaID); // definir fonte e cursor para a coluna mestre
		tblLivros.addMouseListener(new MouseAdapter() { // definir ação quando clicar duas vezes na coluna mestre
			@Override
			public void mouseClicked(MouseEvent me) {
				Point p = me.getPoint();
				int row = tblLivros.rowAtPoint(p);
				int col = tblLivros.columnAtPoint(p);
				if((col == colMestre) && (me.getClickCount() == 2)){
//					JOptionPane.showMessageDialog(null, "MOSTRAR informações avançadas DO LIVRO DE id = "+ tblLivros.getIdFromRow(row, colMestre));
					chamarDetalhes(tblLivros.getIdFromRow(row, colMestre));
				}
			}
		});
		tblLivros.getSelectionModel().addListSelectionListener(new ListSelectionListener() { // habilitar somente se tiver alguma linha selecionada
			@Override
	        public void valueChanged(ListSelectionEvent e){ 
	            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	            btnApagarSelecionados.setEnabled(!lsm.isSelectionEmpty());
	            
	            int qtdSelecionadas = tblLivros.getSelectedRows().length;
	            btnApagarSelecionados.setText(textApagarSelecionados+ (qtdSelecionadas>0 ? (" ("+qtdSelecionadas+")") : ""));
	        }
		});
		tblLivros.addMouseMotionListener(new MouseMotionAdapter() { // definir cursor quando estiver sobre a coluna mestre
			@Override
			public void mouseMoved(MouseEvent mv) {
				int col = tblLivros.columnAtPoint(mv.getPoint());
				if(col == colMestre) setCursor(CURSOR_BOTOES);
				else setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});
		
	}
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void chamarDetalhes(String id){
		/*
		Detalhes_jdialog detalhes = new Detalhes_jdialog(this, true);
		detalhes.setLocationRelativeTo(null);
		
		///...BUSCA NA TABELA DO BD AS INFORMAÇÕES GERAIS DO TÍTULO PELO id
		
//		detalhes.carregarDados("livro", "autor", "4", "aaaaaaaaaaaa", "romance", true, false, true, "28/12/2016", "01/01/2017");
		detalhes.setVisible(true);
		///... ATUALIZAR TABELAS, SE A ALTERAÇÃO FOI REALIZADA COM SUCESSO (refresh)
		 */
	}
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "SALVAR": break;
	        	case "DELETAR": tblLivros.removerLinhasSelecionada(colMestre, modelTabelaLivros); break;
	        	case "BUSCAR": break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
		
	

}
