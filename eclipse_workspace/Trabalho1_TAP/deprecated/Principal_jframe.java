package deprecated;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import extra.JTextFieldSemBrancos;

/**
* ω - invocará os sitemas de login, registro e a janela secundária	[JANELA PRINCIPAL]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 23 de dez de 2016
*/
public class Principal_jframe extends JFrame {
	private final Cursor CURSOR_BOTOES = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	private final String PATH_ICONS = "C:\\Users\\user\\Desktop\\Micael\\Eclipse2(programa e workspace)\\tap-projects\\eclipse_workspace\\Trabalho1_TAP\\src\\icones\\";

	private JPanel pnlPrincipal;
	private JTextFieldSemBrancos txtLogin; private String txtLogin_placeholder="seu nick aqui";
	private JLabel lblCadastrar; 
	private JPasswordField pswSenha; private String pswSenha_toolTipText="sua senha aqui";
	private JButton btnEntrar;
	private JPanel pnlCadastro;

	private Border bordaExited = new LineBorder(Color.BLACK, 1);
	private Border bordaEntered = new LineBorder(Color.CYAN, 1);
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal_jframe frame = new Principal_jframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Principal_jframe() {
		setTitle("Biblioteca Pessoal");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 389, 149);
		pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(new Color(255, 250, 250));
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlPrincipal);
		pnlPrincipal.setLayout(null);
		setLocationRelativeTo(null);
	
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		CheckTextField alternarBotaoEntrar = new CheckTextField();
		
		
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuPrimario = new JMenuBar();
		setJMenuBar(mnbMenuPrimario);
		
		JMenu mnPrograma = new JMenu("Programa");
		mnPrograma.setIcon(new ImageIcon(PATH_ICONS+"application.png"));
		mnPrograma.setMnemonic(KeyEvent.VK_P);
		mnbMenuPrimario.add(mnPrograma);
		
		JMenuItem mniSobre = new JMenuItem("Sobre");
		mniSobre.setIcon(new ImageIcon(PATH_ICONS+"information.png"));
		mniSobre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		mniSobre.setActionCommand("_SOBRE");
		mniSobre.addActionListener(acaoBotao);
		mnPrograma.add(mniSobre);
		
		JMenuItem mniFechar = new JMenuItem("Fechar");
		mniFechar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
		mniFechar.setIcon(new ImageIcon(PATH_ICONS+"cross.png"));
		mniFechar.setActionCommand("_FECHAR");
		mniFechar.addActionListener(acaoBotao);
		mniFechar.setToolTipText("fechar programa");
		mnPrograma.add(mniFechar);
		/////////////////////////////////////////////////////////// 
		
		///////////////////[ CAMPOS PRINCIPAIS ]///////////////////
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setBounds(10, 11, 162, 14);
		pnlPrincipal.add(lblLogin);
		
		txtLogin = new JTextFieldSemBrancos(txtLogin_placeholder);
		txtLogin.setBounds(10, 28, 162, 20);
		pnlPrincipal.add(txtLogin);
		txtLogin.setColumns(10);
		txtLogin.addCaretListener(alternarBotaoEntrar);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(201, 11, 162, 14);
		pnlPrincipal.add(lblSenha);
		
		pswSenha = new JPasswordField();
		pswSenha.setActionCommand("_ENTRAR");
		pswSenha.setEchoChar('×');
		pswSenha.setToolTipText(pswSenha_toolTipText);
		pswSenha.setBounds(201, 28, 162, 20);
		pnlPrincipal.add(pswSenha);
		pswSenha.addCaretListener(alternarBotaoEntrar);
		pswSenha.addActionListener(acaoBotao);
		
		
		// (c) http://stackoverflow.com/questions/14159536/creating-jbutton-with-customized-look
		btnEntrar = new JButton("ENTRAR");
		btnEntrar.setActionCommand("_ENTRAR");
		btnEntrar.setCursor(CURSOR_BOTOES);
		btnEntrar.setBorder(bordaExited);
		btnEntrar.setEnabled(false);
		btnEntrar.setBounds(201, 54, 162, 34);
		btnEntrar.setBackground(new Color(59, 89, 182));
		btnEntrar.setForeground(Color.WHITE);
		btnEntrar.setFocusPainted(false);
		btnEntrar.setFont(new Font("Tahoma", Font.BOLD, 12));
		pnlPrincipal.add(btnEntrar);
		btnEntrar.addActionListener(acaoBotao);
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				if(btnEntrar.isEnabled()) btnEntrar.setBorder(bordaEntered);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnEntrar.setBorder(bordaExited);
			}
		});
		btnEntrar.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if(btnEntrar.isEnabled()) btnEntrar.setBorder(bordaEntered);
			}
			@Override
			public void focusLost(FocusEvent e) {
				btnEntrar.setBorder(bordaExited);
			}
		});
		
		
		////////////////[ EXTRAS ]////////////////
		pnlCadastro = new JPanel();
		pnlCadastro.setBackground(pnlPrincipal.getBackground());
		pnlCadastro.setBounds(10, 54, 162, 34);
		pnlPrincipal.add(pnlCadastro);
		pnlCadastro.setLayout(new BorderLayout(0, 0));
		
		JLabel lblInfoConta = new JLabel("não possui uma conta?");
		pnlCadastro.add(lblInfoConta, BorderLayout.NORTH);
		lblInfoConta.setVerticalAlignment(SwingConstants.TOP);
		lblInfoConta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblInfoConta.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblCadastrar = new JLabel("cadastrar-me");
		lblCadastrar.setEnabled(false);
		pnlCadastro.add(lblCadastrar, BorderLayout.CENTER);
		lblCadastrar.setCursor(CURSOR_BOTOES);
		lblCadastrar.setHorizontalAlignment(SwingConstants.CENTER);
		lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCadastrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent me){
				// GERENCIAR CADASTRAMENTO
				lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 15));
				chamarCadastroUsuario();
			}
			@Override
			public void mouseReleased(MouseEvent me){
				lblCadastrar.setFont(new Font("Tahoma", Font.BOLD, 13));
			}
		});

		
	}
	
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private boolean camposValidos(){
		return (pswSenha.getPassword().length > 0) && 
				!(txtLogin.textIsEmpty());
	}
	
	private void toggleBtnEntrar(){
		boolean tudoValido = camposValidos();
		lblCadastrar.setEnabled(tudoValido);
		btnEntrar.setEnabled(tudoValido);
	}
	
	private void chamarCadastroUsuario(){}
	private void chamarEntrar(){}
	
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class CheckTextField implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent e) {
			toggleBtnEntrar();
		}
	}
	
	
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "ENTRAR": chamarEntrar(); break;
	        	case "FECHAR": dispose(); break;
	        	case "SOBRE": break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
	
	
}
