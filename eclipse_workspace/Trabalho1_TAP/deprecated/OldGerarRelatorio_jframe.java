package deprecated;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import extra.FileTypeFilter;

/**
* δ - para exibir todos os dados possíveis (em quantidade)	[QUARTA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 25 de dez de 2016
*/
public class OldGerarRelatorio_jframe extends JFrame {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private Cursor cursorBotoes = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	private Font lblFonte = new Font("Ubuntu Light", Font.BOLD, 13);
	private final String PATH_ICONS = "C:\\Users\\user\\Desktop\\Micael\\Eclipse2(programa e workspace)\\tap-projects\\eclipse_workspace\\Trabalho1_TAP\\src\\icones\\";
	
	private FileFilter pdfFilter = new FileTypeFilter("pdf", "Portable Document Format");
	private FileFilter csvFilter = new FileTypeFilter("csv", "Comma-Separated Values");
	
	// CARREGAR DADOS DA TABELA
	private String[] nomeColunas =  { "id","Nome","Autor",	"Edição",		"Categoria","Dia da Aquisição","Dia da Devolução" }; // informações básicas + categoria&datas
    protected Object[][] dadosUteis={{"6", "F", 	"X",	new Integer(1),	 "?",		"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "5", "E", 	"X",	new Integer(2),	 "?",		"dd/MM/yyyy",	"-"},
    								{ "3", "D", 	"X",	new Integer(3),	 "?",		"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "4", "C", 	"X",	new Integer(5),	 "?",		"dd/MM/yyyy",	"-"},
    								{ "1", "B", 	"X",	new Integer(4),	 "?",		"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "2", "A", 	"X",	new Integer(6),	 "?",		"dd/MM/yyyy",	"dd/MM/yyyy"}};    
    private int colMestre = 0; // a coluna do id
    private int colCategoria=4, colDiaAquisicao=5, colDiaDevolucao=6;
    
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(dadosUteis,nomeColunas) {
		boolean[] columnEditables = {false, true, true, true, true, false, false};
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {String.class,String.class,String.class,Integer.class,String.class,String.class,String.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	
	private String[] nomeColunas2 = {"Usuários", "Empréstimos", "Favoritos", "Livros Adquiridos/Emprestados", "Livros Desejados"};
	private Integer[][] dadosUteis2 = {{0,		0,				0,			0,								0}};
	private DefaultTableModel modelTabelaQuantidades = new DefaultTableModel(dadosUteis2, nomeColunas2){
			boolean[] columnEditables = new boolean[] {false,false,false,false,false};
			public boolean isCellEditable(int row, int column) {return columnEditables[column];	}
			Class[] columnTypes = new Class[] {Integer.class,Integer.class,Integer.class,Integer.class,Integer.class};
			public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() {
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment(JLabel.CENTER);
	        return this;
	    }
	};
	

	private JTable tblLivros;
	private JTable tblQuantidades;
    private JComboBox cbxOrdenar;
	private JFileChooser fcrExportar;
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public static void main(String[] args) {
//		Object[] extensoesValidas = { new String[]{"pdf", "Portable Document Format"}, new String[]{"csv", "Comma-Separated Values"} };
//		Hashtable extensoesValidas = new Hashtable<String,String>() {{ put("pdf", "Portable Document Format"); put("csv", "Comma-Separated Values"); }};
//		extensoesValidas.get("pdf");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OldGerarRelatorio_jframe frame = new OldGerarRelatorio_jframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public OldGerarRelatorio_jframe() {
		setResizable(false);
		setType(Type.UTILITY);
		setTitle("Consultar Títulos Emprestados");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 802, 608);
		setLocationRelativeTo(null);
		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBorder(null);
		pnlPrincipal.setBackground(SystemColor.control);
		setContentPane(pnlPrincipal);
		
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
	
		/////////////////////[ BARRA DE MENU ]///////////////////// 
		JMenuBar mnbMenuSecundário = new JMenuBar();
		setJMenuBar(mnbMenuSecundário);
		
		JMenu mnExportar = new JMenu("exportar");
		mnExportar.setIcon(new ImageIcon(PATH_ICONS+"save.gif"));
		mnExportar.setMnemonic(KeyEvent.VK_E);
		mnExportar.setToolTipText("salvar tabelas");
		mnExportar.setHorizontalAlignment(SwingConstants.CENTER);
		mnbMenuSecundário.add(mnExportar);
		
		JMenuItem mniSalvarPDF = new JMenuItem("como PDF");
		mniSalvarPDF.setActionCommand("_EXPORTAR");
		mniSalvarPDF.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.ALT_MASK));
		mniSalvarPDF.addActionListener(acaoBotao);
		mnExportar.add(mniSalvarPDF);
		
		JMenuItem mniSalvarCSV = new JMenuItem("como CSV");
		mniSalvarCSV.setActionCommand("_EXPORTAR");
		mniSalvarCSV.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		mniSalvarCSV.addActionListener(acaoBotao);
		mnExportar.add(mniSalvarCSV);
		
		Component horizontalStrut = Box.createHorizontalStrut(479);
		mnbMenuSecundário.add(horizontalStrut);
		JLabel lblOrdenarpor = new JLabel(" Ordenar Por ");
		lblOrdenarpor.setBackground(new Color(245, 245, 245));
		lblOrdenarpor.setFont(lblFonte);
		lblOrdenarpor.setHorizontalAlignment(SwingConstants.CENTER);
		mnbMenuSecundário.add(lblOrdenarpor);
		
		cbxOrdenar = new JComboBox(new DefaultComboBoxModel(new String[] {"Nome", "Autor"}));
		cbxOrdenar.setActionCommand("_ORDENAR");
		cbxOrdenar.addItemListener(new ComboBoxItemListener());
		cbxOrdenar.setToolTipText("forma de ordenação");
		cbxOrdenar.setSelectedIndex(0);
		cbxOrdenar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		mnbMenuSecundário.add(cbxOrdenar);
		
		
		// DIALOG PARA SALVAR ARQUIVO
		fcrExportar = new JFileChooser();
		fcrExportar.setDialogTitle("Escolha um local para salvar o relatório");
		fcrExportar.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fcrExportar.setFileFilter(pdfFilter);
		fcrExportar.setFileFilter(csvFilter);
		fcrExportar.setAcceptAllFileFilterUsed(false);
		pnlPrincipal.setLayout(null);
		
		
		/////////////////////////[ FUNDO DAS TABELAS ]/////////////////////// 
		JScrollPane scrpnTabela = new JScrollPane();
		scrpnTabela.setBounds(10, 11, 776, 458);
		pnlPrincipal.add(scrpnTabela);

		JScrollPane scrpnContagem = new JScrollPane();
		scrpnContagem.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrpnContagem.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrpnContagem.setBounds(10, 505, 776, 40);
		pnlPrincipal.add(scrpnContagem);
		
		/////////////////////////[ TABELAS ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivros = new JTable(modelTabelaLivros);
		tblLivros.setEnabled(false);
		tblLivros.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivros.setDefaultRenderer(Integer.class, centerRenderer); 
		tblLivros.getColumnModel().getColumn(colCategoria).setCellRenderer( centerRenderer );
		tblLivros.getColumnModel().getColumn(colDiaAquisicao).setCellRenderer( centerRenderer );
		tblLivros.getColumnModel().getColumn(colDiaDevolucao).setCellRenderer( centerRenderer );
		tblLivros.getColumnModel().getColumn(colMestre).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(colMestre).setCellRenderer(tableRendererParaID);
		tblLivros.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivros.setBackground(SystemColor.textHighlightText);
		scrpnTabela.setViewportView(tblLivros);
		
		JLabel lblTotal = new JLabel("TOTAL");
		lblTotal.setFont(lblFonte);
		lblTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotal.setBounds(10, 480, 776, 24);
		pnlPrincipal.add(lblTotal);
		
		tblQuantidades = new JTable(modelTabelaQuantidades);
		tblQuantidades.setRowSelectionAllowed(false);
		tblQuantidades.setEnabled(false);
		tblQuantidades.getColumnModel().getColumn(3).setPreferredWidth(150);
		// CENTRALIZAR COLUNAS DE CLASSE 'Integer'
		tblQuantidades.setDefaultRenderer(Integer.class, centerRenderer); 
		scrpnContagem.setViewportView(tblQuantidades);

		
	}
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void salvarRelatorio(){// TODO adicionar ação para gerar 1 PDF das tabelas & 1 CSV das tabelas (texto bruto)
		//(c) http://www.codejava.net/java-se/swing/show-save-file-dialog-using-jfilechooser
		//(c) http://www.codejava.net/java-se/swing/add-file-filter-for-jfilechooser-dialog		
		int userSelection = fcrExportar.showSaveDialog(null);
		 
		if (userSelection == JFileChooser.APPROVE_OPTION) {
			FileTypeFilter filtroSelecionado = (FileTypeFilter)fcrExportar.getFileFilter();
		    File fileToSave = fcrExportar.getSelectedFile();
		    if( !filtroSelecionado.accept(fileToSave) ) fileToSave = new File( fileToSave + filtroSelecionado.getExtension() );
		    System.out.println("Salvar como: " + fileToSave.getAbsolutePath());
		}

	}
	
	private void ordenarTabelaLivros(Object itemSelecionado){
		System.out.println("ordenar por: " + itemSelecionado.toString() );
	}
	


	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "EXPORTAR": salvarRelatorio(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
	
	private class ComboBoxItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent ie) {
			JComboBox cb = (JComboBox)ie.getSource();
			Object item = ie.getItem();
			String comando = cb.getActionCommand().substring(1);
			
			switch(comando){
				case "ORDENAR":{
					if(ie.getStateChange() == ItemEvent.SELECTED) ordenarTabelaLivros(cb.getSelectedItem().toString().toLowerCase());
					break;
				}
				
				default: throw new IllegalArgumentException("Invalid Command: " + comando);
			}
			
		}
	}
}
