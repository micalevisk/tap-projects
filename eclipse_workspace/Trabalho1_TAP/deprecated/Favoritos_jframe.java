package deprecated;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import extra.JAdvancedTable;

/**
* η - exibir os títulos marcados como 'favorito'	[SEXTA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 26 de dez de 2016
*/
public class Favoritos_jframe extends JFrame {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private Cursor cursorBotoes = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	
	// CARREGAR DADOS DA TABELA
	protected String[] nomeColunas = { "id", 	"Nome", "Autor","Edição" }; // informações básicas
	protected Object[][] dadosUteis= {{ "1", 	"B", 	"C",	new Integer(1) },
									  { "2", 	"B", 	"C",	new Integer(1) },
									  { "3", 	"B", 	"C",	new Integer(1) },
									  { "4", 	"B", 	"C",	new Integer(1) },
									  { "5", 	"B", 	"C",	new Integer(1) }};
	
	private int colMestre = 0; // a coluna do id
    
    private JAdvancedTable tblLivros;
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(dadosUteis,nomeColunas) {
		boolean[] columnEditables = {false, true, true, true}; // a primeira coluna não é editável
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {String.class,String.class,String.class,Integer.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() {
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment( JLabel.CENTER );
	        setToolTipText("editar detalhes");
	        return this;
	    }
	};
	
    
    private JButton btnSalvarAlteracoes;
    private JButton btnRemover;
	//////////////////////////////////////////////////////////////////////////////////////////////

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Favoritos_jframe frame = new Favoritos_jframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Favoritos_jframe() {
		setType(Type.POPUP);
		setTitle("Consultar Títulos Favoritos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 558, 389);
		setLocationRelativeTo(null);
		JPanel  pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.inactiveCaption);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlPrincipal);

		
		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(new Color(255, 255, 255));
		pnlPrincipal.add(pnlBotoes, BorderLayout.NORTH);
		pnlBotoes.setLayout(new BorderLayout(0, 0));
		
		////////////////////////[ BOTÕES ]////////////////////////
		btnSalvarAlteracoes = new JButton("Salvar Alterações");
		btnSalvarAlteracoes.setActionCommand("_SALVAR");
		btnSalvarAlteracoes.setMnemonic(KeyEvent.VK_S);
		btnSalvarAlteracoes.addActionListener(new ButtonClickActions());
		btnSalvarAlteracoes.setCursor(cursorBotoes);
		btnSalvarAlteracoes.setForeground(new Color(0, 128, 0));
		pnlBotoes.add(btnSalvarAlteracoes, BorderLayout.WEST);
		
		String textRemoverDaLista = "Remover da Lista";
		btnRemover = new JButton(textRemoverDaLista);
		btnRemover.setActionCommand("_REMOVER" );
//		btnRemover.setMnemonic(KeyEvent.VK_R);// FIXME erro quando o table#row é ímpar {Exception in thread "AWT-EventQueue-0" java.lang.ArrayIndexOutOfBoundsException: 2 >= 2} 
		btnRemover.addActionListener(new ButtonClickActions());
		btnRemover.setCursor(cursorBotoes);
		btnRemover.setEnabled(false);
		btnRemover.setForeground(new Color(128, 0, 0));
		btnRemover.addActionListener(new ButtonClickActions());
		pnlBotoes.add(btnRemover, BorderLayout.CENTER);
		
		
		/////////////////////////[ FUNDO DA TABELA ]/////////////////////// 
		JScrollPane scrpnlTabela = new JScrollPane();
		scrpnlTabela.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlPrincipal.add(scrpnlTabela);
		scrpnlTabela.addMouseListener(new MouseAdapter() { // unselect when clicked out table
			@Override
			public void mouseClicked(MouseEvent me) {
				tblLivros.clearSelection();
			}
		});

		/////////////////////////[ TABELA ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivros = new JAdvancedTable(modelTabelaLivros);
		tblLivros.getTableHeader().setReorderingAllowed(false); // disable user column draggin
		tblLivros.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivros.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivros.setBackground(SystemColor.textHighlightText);
		tblLivros.setDefaultRenderer(Integer.class, centerRenderer);
		tblLivros.getColumnModel().getColumn(colMestre).setPreferredWidth(1);
		tblLivros.getColumnModel().getColumn(colMestre).setCellRenderer(tableRendererParaID); // definir fonte e cursor para a coluna mestre
		scrpnlTabela.setViewportView(tblLivros);
		tblLivros.addMouseListener(new MouseAdapter() { // definir ação quando clicar duas vezes na coluna mestre
			@Override
			public void mousePressed(MouseEvent me) {
				Point p = me.getPoint();
				int row = tblLivros.rowAtPoint(p);
				int col = tblLivros.columnAtPoint(p);
				if(col == colMestre){
//					JOptionPane.showMessageDialog(null, "MOSTRAR informações avançadas DO LIVRO DE id = " + tblLivros.getIdFromRow(row, colMestre));
					chamarDetalhes(tblLivros.getIdFromRow(row, colMestre));
				}
			}
		});
		tblLivros.getSelectionModel().addListSelectionListener(new ListSelectionListener() { // habilitar somente se tiver alguma linha selecionada
			@Override
	        public void valueChanged(ListSelectionEvent e){ 
	            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	            btnRemover.setEnabled(!lsm.isSelectionEmpty());
	            
	            int qtdSelecionadas = tblLivros.getSelectedRows().length;
	            btnRemover.setText(textRemoverDaLista+ (qtdSelecionadas>0 ? (" ("+qtdSelecionadas+")") : ""));
	        }
		});
		tblLivros.addMouseMotionListener(new MouseMotionAdapter() { // definir cursor quando estiver sobre a coluna mestre
			@Override
			public void mouseMoved(MouseEvent mv) {
				int col = tblLivros.columnAtPoint(mv.getPoint());
				if(col == colMestre) setCursor(cursorBotoes);
				else setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});



	}
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void chamarDetalhes(String id){
		/*
		Detalhes_jdialog detalhes = new Detalhes_jdialog(this, true);
		detalhes.setLocationRelativeTo(null);
		
		///...BUSCA NA TABELA DO BD AS INFORMAÇÕES GERAIS DO TÍTULO PELO id
		
//		detalhes.carregarDados("livro", "autor", "4", "aaaaaaaaaaaa", "romance", true, false, true, "28/12/2016", "01/01/2017");
		detalhes.setVisible(true);
		///... ATUALIZAR TABELAS, SE A ALTERAÇÃO FOI REALIZADA COM SUCESSO (refresh)
		 */
	}
	
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "SALVAR": break;
	        	case "REMOVER": tblLivros.removerLinhasSelecionada(colMestre, modelTabelaLivros); break;
	        	case "BUSCAR": break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
}
