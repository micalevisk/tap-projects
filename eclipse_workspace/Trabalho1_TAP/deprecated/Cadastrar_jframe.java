package deprecated;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.text.NumberFormatter;
import com.toedter.calendar.JDateChooser;

import extra.JTextFieldComBrancos;
import javax.swing.ImageIcon;

/**
* β - a janela dedicada ao cadastramento de novos títulos	[SEGUNDA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 24 de dez de 2016
*/
public class Cadastrar_jframe extends JFrame {
	private final Cursor CURSOR_BOTOES = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	private final String PATH_ICONS = "C:\\Users\\user\\Desktop\\Micael\\Eclipse2(programa e workspace)\\tap-projects\\eclipse_workspace\\Trabalho1_TAP\\src\\icones\\";
	private final Font lblFONTE = new Font("Ubuntu Light", Font.BOLD, 11);
	private final Font chxFONTE = new Font("Tahoma", Font.PLAIN, 12);
	private final Border brITEM_OBRIGATORIO = BorderFactory.createMatteBorder(1, 1, 1, 2, Color.red);

	private JTextFieldComBrancos txtNomeLivro, txtAutor;//*
	private JSpinner spnEdicao;//*
	
	protected String[] strCategorias = new String[] {"categoria", "técnico", "romance", "ação/aventura", "religioso", "revista", "didático", "biográfico"};// ## do banco de dados
	private JComboBox cbxCategoria;//*
	private JTextArea txtarDescricao;
	private JPanel pnlCheckboxes;
	private JCheckBox chxEmprestado, chxFavorito;
	
	private String textAquisicao  = "Dia da Aquisição";
	private String textEmprestimo = "Dia do Empréstimo";
	private JLabel lblDiaDevolucao;
	private JLabel lblDiaAquisicao;
	JDateChooser dcrDiaAquisicao, dcrDiaDevolucao;
	
	JButton btnCadastrar;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cadastrar_jframe frame = new Cadastrar_jframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cadastrar_jframe() {
		setResizable(false);
		setType(Type.UTILITY);
		setTitle("Cadastrar Títulos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 493, 337);
		JPanel pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(new Color(245, 245, 245));
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlPrincipal);
		pnlPrincipal.setLayout(null);
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		////////////////////////////////////////////[ SOBRE O TÍTULO ]////////////////////////////////////////////
		JPanel pnlSobreOTitulo = new JPanel();
		pnlSobreOTitulo.setForeground(UIManager.getColor("Button.foreground"));
		pnlSobreOTitulo.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Sobre o Título", TitledBorder.RIGHT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlSobreOTitulo.setBackground(new Color(245, 245, 245));
		pnlSobreOTitulo.setBounds(10, 11, 281, 111);
		pnlPrincipal.add(pnlSobreOTitulo);
		pnlSobreOTitulo.setLayout(null);
		
		
		///////////////////[ RÓTULOS ]///////////////////
		JLabel lblNomeDoLivro = new JLabel("Nome do Livro");
		lblNomeDoLivro.setHorizontalAlignment(SwingConstants.LEFT);
		lblNomeDoLivro.setFont(lblFONTE);
		lblNomeDoLivro.setBounds(10, 11, 256, 14);
		pnlSobreOTitulo.add(lblNomeDoLivro);
		
		JLabel lblAutor = new JLabel("Autor");
		lblAutor.setHorizontalAlignment(SwingConstants.LEFT);
		lblAutor.setFont(lblFONTE);
		lblAutor.setBounds(88, 58, 178, 14);
		pnlSobreOTitulo.add(lblAutor);
		
		JLabel lblEdicao = new JLabel("Edição");
		lblEdicao.setVerticalAlignment(SwingConstants.TOP);
		lblEdicao.setHorizontalAlignment(SwingConstants.LEFT);
		lblEdicao.setFont(lblFONTE);
		lblEdicao.setBounds(10, 58, 43, 14);
		pnlSobreOTitulo.add(lblEdicao);

		
		////////////////////////[ CAMPOS ]////////////////////////
		txtNomeLivro = new JTextFieldComBrancos("digite aqui o nome do livro");
		txtNomeLivro.setBounds(10, 25, 256, 26);
		txtNomeLivro.setBorder(brITEM_OBRIGATORIO);
		pnlSobreOTitulo.add(txtNomeLivro);
		txtNomeLivro.setColumns(10);
		
		txtAutor = new JTextFieldComBrancos("digite aqui o nome do autor");
		txtAutor.setColumns(10);
		txtAutor.setBorder(brITEM_OBRIGATORIO);
		txtAutor.setBounds(88, 74, 178, 26);
		pnlSobreOTitulo.add(txtAutor);
		
		spnEdicao = new JSpinner();
		spnEdicao.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spnEdicao.setBounds(10, 74, 43, 26);
		// apenas números
		JFormattedTextField txtonlynumbers = ((JSpinner.NumberEditor) spnEdicao.getEditor()).getTextField();
		((NumberFormatter) txtonlynumbers.getFormatter()).setAllowsInvalid(false);
		// texto centralizado
		JComponent editor = spnEdicao.getEditor();
		JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor)editor;
		spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		pnlSobreOTitulo.add(spnEdicao);
		

		/////////////////////////////////////[ GERAL ]/////////////////////////////////////
		JPanel pnlDetalhes = new JPanel();
		pnlDetalhes.setToolTipText("informações gerais");
		pnlDetalhes.setBackground(new Color(245, 245, 245));
		pnlDetalhes.setBorder(new TitledBorder(null, "Geral", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDetalhes.setBounds(10, 133, 321, 164);
		pnlPrincipal.add(pnlDetalhes);
		pnlDetalhes.setLayout(null);
		
		JScrollPane scrpnlBoxParaDescricao = new JScrollPane();
		scrpnlBoxParaDescricao.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrpnlBoxParaDescricao.setBounds(10, 22, 148, 131);
		pnlDetalhes.add(scrpnlBoxParaDescricao);
		
		txtarDescricao = new JTextArea();
		txtarDescricao.setFont(new Font("Nirmala UI", Font.PLAIN, 12));
		txtarDescricao.setBackground(new Color(255, 255, 255));
		txtarDescricao.setLineWrap(true);
		txtarDescricao.setWrapStyleWord(true);
		txtarDescricao.setToolTipText("digite uma breve descrição sobre o livro");
		scrpnlBoxParaDescricao.setViewportView(txtarDescricao);
		
		JLabel lblDescricao = new JLabel("Descrição");
		lblDescricao.setBackground(new Color(245, 245, 245));
		lblDescricao.setFont(lblFONTE);
		lblDescricao.setHorizontalAlignment(SwingConstants.CENTER);
		scrpnlBoxParaDescricao.setColumnHeaderView(lblDescricao);
		
		cbxCategoria = new JComboBox(strCategorias);
		cbxCategoria.setEditable(true);
//		cbxCategoria.setModel(new DefaultComboBoxModel(strCategorias));
		cbxCategoria.setBorder(brITEM_OBRIGATORIO);
		cbxCategoria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbxCategoria.setSelectedIndex(0);
		cbxCategoria.setBounds(168, 22, 143, 29);
		pnlDetalhes.add(cbxCategoria);
		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();  
		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
		cbxCategoria.setRenderer(UIResource);
		
		//////////////////////////////[ CHECKBOXES ]//////////////////////////////
		pnlCheckboxes = new JPanel();
		pnlCheckboxes.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		pnlCheckboxes.setBackground(SystemColor.controlHighlight);
		pnlCheckboxes.setBounds(168, 62, 143, 91);
		pnlDetalhes.add(pnlCheckboxes);
		pnlCheckboxes.setLayout(null);
		
		chxEmprestado = new JCheckBox("Emprestado");
		chxEmprestado.setMnemonic(KeyEvent.VK_E);
		chxEmprestado.setActionCommand("_EMPRESTADO");
		chxEmprestado.addActionListener(acaoBotao);
		chxEmprestado.setToolTipText("o livro é emprestado?");
		chxEmprestado.setHorizontalAlignment(SwingConstants.LEFT);
		chxEmprestado.setBounds(6, 6, 131, 23);
		chxEmprestado.setBackground(null);
		chxEmprestado.setFont(chxFONTE);
		pnlCheckboxes.add(chxEmprestado);
		
		JCheckBox chxDesejado = new JCheckBox("Desejado");
		chxDesejado.setToolTipText("adicionar à lista de desejo?");
		chxDesejado.setMnemonic(KeyEvent.VK_D);
		chxDesejado.setHorizontalAlignment(SwingConstants.LEFT);
		chxDesejado.setBackground((Color) null);
		chxDesejado.setBounds(6, 33, 131, 23);
		chxDesejado.setFont(chxFONTE);
		pnlCheckboxes.add(chxDesejado);
		
		chxFavorito = new JCheckBox("Favorito");
		chxFavorito.setMnemonic(KeyEvent.VK_F);
		chxFavorito.setToolTipText("é o seu livro favorito?");
		chxFavorito.setHorizontalAlignment(SwingConstants.LEFT);
		chxFavorito.setBounds(6, 61, 131, 23);
		chxFavorito.setBackground(null);
		chxFavorito.setFont(chxFONTE);
		pnlCheckboxes.add(chxFavorito);
		
		
		//////////////////////////////////////////[ DATAS ]//////////////////////////////////////////
		JPanel pnlDatas = new JPanel();
		pnlDatas.setBackground(new Color(245, 245, 245));
		pnlDatas.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datas", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlDatas.setBounds(301, 11, 166, 111);
		pnlPrincipal.add(pnlDatas);
		pnlDatas.setLayout(null);
		
		lblDiaAquisicao = new JLabel(textAquisicao);
		lblDiaAquisicao.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiaAquisicao.setFont(lblFONTE);
		lblDiaAquisicao.setBounds(10, 14, 146, 14);
		pnlDatas.add(lblDiaAquisicao);
		
		dcrDiaAquisicao = new JDateChooser();
		dcrDiaAquisicao.setDate(new Date()); // definir dia atual
		dcrDiaAquisicao.setBorder(null);
		dcrDiaAquisicao.setBounds(10, 29, 146, 20);
		pnlDatas.add(dcrDiaAquisicao);
		
		lblDiaDevolucao = new JLabel("Dia da Devolução");
		lblDiaDevolucao.setEnabled(false);
		lblDiaDevolucao.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDiaDevolucao.setFont(lblFONTE);
		lblDiaDevolucao.setBounds(10, 65, 146, 14);
		pnlDatas.add(lblDiaDevolucao);
		
		dcrDiaDevolucao = new JDateChooser();
		dcrDiaDevolucao.setEnabled(false);
		dcrDiaDevolucao.setToolTipText("se o livro for emprestado");
		dcrDiaDevolucao.setBorder(null);
		dcrDiaDevolucao.setBounds(10, 80, 146, 20);
		pnlDatas.add(dcrDiaDevolucao);
		
		
		/////////////////////////////////////[ BOTÕES PRINCIPAIS ]/////////////////////////////////////
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setEnabled(false);
		btnCadastrar.setIcon(new ImageIcon(PATH_ICONS+"add.png"));
		btnCadastrar.setActionCommand("_CADASTRAR");
		btnCadastrar.setMnemonic(KeyEvent.VK_C);
		btnCadastrar.addActionListener(acaoBotao);
		btnCadastrar.setCursor(CURSOR_BOTOES);
		btnCadastrar.setForeground(new Color(0, 128, 0));
		btnCadastrar.setFont(new Font("Tw Cen MT", Font.PLAIN, 13));
		btnCadastrar.setToolTipText("cadastrar livro");
		btnCadastrar.setBounds(341, 141, 126, 116);
		pnlPrincipal.add(btnCadastrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(PATH_ICONS+"cancel.png"));
		btnCancelar.setActionCommand("_CANCELAR");
		btnCancelar.setMnemonic(KeyEvent.VK_R);
		btnCancelar.addActionListener(acaoBotao);
		btnCancelar.setCursor(CURSOR_BOTOES);
		btnCancelar.setForeground(new Color(128, 0, 0));
		btnCancelar.setToolTipText("voltar");
		btnCancelar.setFont(new Font("Tw Cen MT", Font.PLAIN, 13));
		btnCancelar.setBounds(341, 267, 126, 30);
		pnlPrincipal.add(btnCancelar);
	}
	
	
	private boolean nenhumaPendencia(){return false;}
	private void toggleCadastrar(){}
	
	private void efetivarCadastro(){}
	
	private void toggleDiaDevolucao(){
		boolean estaMarcado;
		if(estaMarcado = this.chxEmprestado.isSelected()) this.lblDiaAquisicao.setText(textEmprestimo);
		else this.lblDiaAquisicao.setText(textAquisicao);
		this.lblDiaDevolucao.setEnabled(estaMarcado);
		this.dcrDiaDevolucao.setEnabled(estaMarcado);
	}
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().replaceFirst("_", "");

	        switch(comando){
	        	case "EMPRESTADO": toggleDiaDevolucao(); break;
	        	case "CADASTRAR": efetivarCadastro();
	        	case "CANCELAR": dispose(); break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
}
