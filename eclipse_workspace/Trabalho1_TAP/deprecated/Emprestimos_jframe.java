package deprecated;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import extra.JAdvancedTable;

/**
* γ - exibir informações dos livros da tabela de 'livros emprestados' para edição	[TERCEIRA JANELA]
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 25 de dez de 2016
*/
public class Emprestimos_jframe extends JFrame {
	//////////////////////////////////////////////////////////////////////////////////////////////
	private Cursor cursorBotoes = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	
	
	protected String[] strCategorias = new String[] {"categoria", "técnico", "romance", "ação/aventura", "religioso", "revista", "didático", "biográfico"};
	// CARREGAR DADOS DA TABELA
	private String[] nomeColunas =  { "id","Nome","Autor",	"Edição",		"Categoria",		"Dia da Aquisição","Dia da Devolução" }; // informações básicas + categoria&datas
    protected Object[][] dadosUteis={{"1", "B", 	"X",	new Integer(1),	 strCategorias[0],	"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "2", "B", 	"X",	new Integer(1),	 strCategorias[0],	"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "3", "B", 	"X",	new Integer(1),	 strCategorias[0],	"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "4", "B", 	"X",	new Integer(1),	 strCategorias[0],	"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "5", "B", 	"X",	new Integer(1),	 strCategorias[0],	"dd/MM/yyyy",	"dd/MM/yyyy"},
    								{ "6", "B", 	"X",	new Integer(1),	 strCategorias[0],	"dd/MM/yyyy",	"dd/MM/yyyy"}};    
    
    private int colMestre = 0; // a coluna do id
    private int colCategoria=4, colDiaAquisicao=5, colDiaDevolucao=6;
    
    private JAdvancedTable tblLivrosEmprestados;
    private DefaultTableModel modelTabelaLivros = new DefaultTableModel(dadosUteis,nomeColunas) {
		boolean[] columnEditables = {false, true, true, true, true, false, false};
		public boolean isCellEditable(int row, int column){	return columnEditables[column];	}
		Class[] columnTypes = new Class[] {String.class,String.class,String.class,Integer.class,String.class,String.class,String.class};
		public Class getColumnClass(int columnIndex) {return columnTypes[columnIndex];}
	};
	
	private DefaultTableCellRenderer tableRendererParaID = new DefaultTableCellRenderer() {
	    Font font = new Font("Tahoma", Font.BOLD, 12); // apenas na coluna mestre
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        setFont(font);
	        setHorizontalAlignment(JLabel.CENTER);
	        setToolTipText("editar detalhes");
	        return this;
	    }
	};
	
	
	private JComboBox cbxCategoria;
    private JButton btnSalvarAlteracoes;
    private JButton btnBuscar;
    private JButton btnRemoverSelecionados;
	//////////////////////////////////////////////////////////////////////////////////////////////

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Emprestimos_jframe frame = new Emprestimos_jframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Emprestimos_jframe() {
		setResizable(false);
		setType(Type.POPUP);
		setTitle("Consultar Títulos Emprestados");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 802, 389);
		setLocationRelativeTo(null);
		JPanel  pnlPrincipal = new JPanel();
		pnlPrincipal.setBackground(SystemColor.inactiveCaption);
		pnlPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlPrincipal);

		
		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(new Color(255, 255, 255));
		pnlPrincipal.add(pnlBotoes, BorderLayout.NORTH);
		pnlBotoes.setLayout(new BorderLayout(0, 0));
		
		/////////////////////[ PRIVADAS ]/////////////////////
		ButtonClickActions acaoBotao = new ButtonClickActions();
		
		////////////////////////[ BOTÕES ]////////////////////////
		btnSalvarAlteracoes = new JButton("Salvar Alterações");
		btnSalvarAlteracoes.setActionCommand("_SALVAR");
		btnSalvarAlteracoes.setMnemonic(KeyEvent.VK_S);
		btnSalvarAlteracoes.setCursor(cursorBotoes);
		btnSalvarAlteracoes.setForeground(new Color(0, 128, 0));
		pnlBotoes.add(btnSalvarAlteracoes, BorderLayout.WEST);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setActionCommand("_BUSCAR");
		btnBuscar.setMnemonic(KeyEvent.VK_B);
		btnBuscar.setCursor(cursorBotoes);
		btnBuscar.setForeground(new Color(0, 128, 128));
		pnlBotoes.add(btnBuscar, BorderLayout.EAST);
		
		String textRemoverSelecionados = "Remover Selecionados";
		btnRemoverSelecionados = new JButton(textRemoverSelecionados);
		btnRemoverSelecionados.setActionCommand("_REMOVER");
//		btnRemoverSelecionados.setMnemonic(KeyEvent.VK_R);
		btnRemoverSelecionados.setCursor(cursorBotoes);
		btnRemoverSelecionados.setEnabled(false);
		btnRemoverSelecionados.setForeground(new Color(128, 0, 0));
		btnRemoverSelecionados.addActionListener(acaoBotao);
		pnlBotoes.add(btnRemoverSelecionados, BorderLayout.CENTER);
		
		
		/////////////////////////[ FUNDO DA TABELA ]/////////////////////// 
		JScrollPane scrpnlTabela = new JScrollPane();
		pnlPrincipal.add(scrpnlTabela);
		scrpnlTabela.addMouseListener(new MouseAdapter() { // unselect when clicked out table
			@Override
			public void mouseClicked(MouseEvent me) {
				tblLivrosEmprestados.clearSelection();
			}
		});

		/////////////////////////[ TABELA ]///////////////////////
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tblLivrosEmprestados = new JAdvancedTable(modelTabelaLivros);
		scrpnlTabela.setViewportView(tblLivrosEmprestados);
		tblLivrosEmprestados.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tblLivrosEmprestados.getTableHeader().setReorderingAllowed(false); // disable user column draggin
		tblLivrosEmprestados.setDefaultRenderer(Integer.class, centerRenderer); 
		tblLivrosEmprestados.getColumnModel().getColumn(colCategoria).setCellRenderer( centerRenderer );
		tblLivrosEmprestados.getColumnModel().getColumn(colDiaAquisicao).setCellRenderer( centerRenderer );
		tblLivrosEmprestados.getColumnModel().getColumn(colDiaDevolucao).setCellRenderer( centerRenderer );
		tblLivrosEmprestados.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblLivrosEmprestados.setBackground(SystemColor.textHighlightText);
		tblLivrosEmprestados.getColumnModel().getColumn(colMestre).setPreferredWidth(1);
		tblLivrosEmprestados.getColumnModel().getColumn(colMestre).setCellRenderer(tableRendererParaID); // definir fonte e cursor para a coluna mestre
		tblLivrosEmprestados.addMouseListener(new MouseAdapter() { // definir ação quando clicar duas vezes na coluna mestre
			@Override
			public void mousePressed(MouseEvent me) {
				Point p = me.getPoint();
				int row = tblLivrosEmprestados.rowAtPoint(p);
				int col = tblLivrosEmprestados.columnAtPoint(p);
				if(col == colMestre){
//					JOptionPane.showMessageDialog(null, "MOSTRAR informações avançadas DO LIVRO DE id = " + tblLivrosEmprestados.getIdFromRow(row, colMestre));
					chamarDetalhes(tblLivrosEmprestados.getIdFromRow(row, colMestre));
				}
			}
		});
		tblLivrosEmprestados.getSelectionModel().addListSelectionListener(new ListSelectionListener() { // habilitar somente se tiver alguma linha selecionada
			@Override
	        public void valueChanged(ListSelectionEvent e){ 
	            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	            btnRemoverSelecionados.setEnabled(!lsm.isSelectionEmpty());
	            
	            int qtdSelecionadas = tblLivrosEmprestados.getSelectedRows().length;
	            btnRemoverSelecionados.setText(textRemoverSelecionados+ (qtdSelecionadas>0 ? (" ("+qtdSelecionadas+")") : ""));
	        }
		});
		tblLivrosEmprestados.addMouseMotionListener(new MouseMotionAdapter() { // definir cursor quando estiver sobre a coluna mestre
			@Override
			public void mouseMoved(MouseEvent mv) {
				int col = tblLivrosEmprestados.columnAtPoint(mv.getPoint());
				if(col == colMestre) setCursor(cursorBotoes);
				else setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});

		cbxCategoria = new JComboBox(strCategorias);
		cbxCategoria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		BasicComboBoxRenderer.UIResource UIResource = new BasicComboBoxRenderer.UIResource();  
		UIResource.setHorizontalAlignment(SwingConstants.CENTER);
		cbxCategoria.setRenderer(UIResource);
		tblLivrosEmprestados.getColumnModel().getColumn(colCategoria).setCellEditor(new DefaultCellEditor(cbxCategoria));	
	}
	
	
	
	////////////////////////////////[ FUNÇÕES AUXILIARES ]////////////////////////////////
	private void chamarDetalhes(String id){
		/*
		Detalhes_jdialog detalhes = new Detalhes_jdialog(this, true);
		detalhes.setLocationRelativeTo(null);
		
		///...BUSCA NA TABELA DO BD AS INFORMAÇÕES GERAIS DO TÍTULO PELO id
		
//		detalhes.carregarDados("livro", "autor", "4", "aaaaaaaaaaaa", "romance", true, false, true, "28/12/2016", "01/01/2017");
		detalhes.setVisible(true);
		///... ATUALIZAR TABELAS, SE A ALTERAÇÃO FOI REALIZADA COM SUCESSO (refresh)
		 */
	}
	
	
	
	/////////////////////////////////[ EVENT HANDLING ]/////////////////////////////////
	private class ButtonClickActions implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	        String comando = e.getActionCommand().substring(1);

	        switch(comando){
	        	case "SALVAR": break;
	        	case "REMOVER": tblLivrosEmprestados.removerLinhasSelecionada(colMestre, modelTabelaLivros); break;
	        	case "BUSCAR": break;
	        
	        	default: throw new IllegalArgumentException("Invalid Command: " + comando);
	        }
	     }		
	}
		
	

}
