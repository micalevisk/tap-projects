package view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

public class TelaPrincipalSDI {
	private JFrame frmLogin;
	
	private JDesktopPane dpnTelaPrincipal;
		
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipalSDI window = new TelaPrincipalSDI();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TelaPrincipalSDI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage(TelaPrincipalMDI.class.getResource("/images/feed.png")));
		frmLogin.setTitle("Interface on Java");
		frmLogin.setResizable(false);
		frmLogin.setBounds(100, 100, 571, 464);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		frmLogin.setLocationRelativeTo(null);
		
		dpnTelaPrincipal = new JDesktopPane();
		dpnTelaPrincipal.setBackground(SystemColor.inactiveCaptionBorder);
		dpnTelaPrincipal.setBounds(0, 26, 565, 409);
		frmLogin.getContentPane().add(dpnTelaPrincipal);		
		
		////////////////////////////////////////////[ BARRA DE MENU ]////////////////////////////////////////////
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 565, 27);
		frmLogin.getContentPane().add(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		mnMenu.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/page.png")));
		mnMenu.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnMenu);
		
		JMenu mnUsuarios = new JMenu("Usu�rios");
		mnUsuarios.setHorizontalAlignment(SwingConstants.CENTER);
		mnUsuarios.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/user.png")));
		menuBar.add(mnUsuarios);
		
		JMenu mnClientes = new JMenu("Clientes");
		mnClientes.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/group.png")));
		mnClientes.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnClientes);
		
		
		///////////////////////////////////////////[ MENU "Menu" ]/////////////////////////////////////////////////////////
		JMenuItem mniSobre = new JMenuItem("Sobre");
		mniSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Desenvolvido por Micael Levi!", "Informa��es", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mniSobre.setHorizontalAlignment(SwingConstants.LEFT);
		mniSobre.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/information.png")));
		mnMenu.add(mniSobre);
		
		JMenuItem mniSair = new JMenuItem("Sair");
		mniSair.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0); // TODO salvar configs e fechar
			}
		});
		mniSair.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/door_out.png")));
		mniSair.setHorizontalAlignment(SwingConstants.LEFT);
		mnMenu.add(mniSair);
		
		JMenuItem mniCadastrarUsuario = new JMenuItem("Cadastrar");
		mniCadastrarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new TelaCadastroUsuarioSDI().setVisible(true);
			}
		});
		mniCadastrarUsuario.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/user_add.png")));
		mnUsuarios.add(mniCadastrarUsuario);
		
		JMenuItem mniExcluirUsuario = new JMenuItem("Excluir");
		mniExcluirUsuario.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/user_delete.png")));
		mnUsuarios.add(mniExcluirUsuario);
		
		JMenuItem mniAlterar = new JMenuItem("Alterar");
		mniAlterar.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/user_edit.png")));
		mnUsuarios.add(mniAlterar);
		
		JMenuItem mniProcurar = new JMenuItem("Procurar");
		mniProcurar.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/find.png")));
		mnUsuarios.add(mniProcurar);

		///////////////////////////////////////////[ MENU "Clientes" ]/////////////////////////////////////////////////////////
		JMenuItem mniCadastrarCliente = new JMenuItem("Cadastrar");
		mniCadastrarCliente.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/group_add.png")));
		mnClientes.add(mniCadastrarCliente);
		
		JMenuItem mniExcluirCliente = new JMenuItem("Excluir");
		mniExcluirCliente.setIcon(new ImageIcon(TelaPrincipalMDI.class.getResource("/images/iconsbar/subitems/group_delete.png")));
		mnClientes.add(mniExcluirCliente);
		


	}
	
}
