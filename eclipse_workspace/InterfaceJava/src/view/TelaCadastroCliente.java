package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.toedter.calendar.JDateChooser;
import javax.swing.JCheckBox;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import com.toedter.components.JLocaleChooser;
import com.toedter.components.JSpinField;
import com.toedter.calendar.JDayChooser;

public class TelaCadastroCliente extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroCliente frame = new TelaCadastroCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroCliente() {
		setTitle("Cadastrar Um Cliente");
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 536, 388);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel pnlDadosPessoais = new JPanel();
		pnlDadosPessoais.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dados Pessoais", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlDadosPessoais.setBackground(SystemColor.control);
		pnlDadosPessoais.setBounds(10, 22, 500, 147);
		contentPane.add(pnlDadosPessoais);
		
		JPanel pnlContato = new JPanel();
		pnlContato.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Contato", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlContato.setBackground(SystemColor.menu);
		pnlContato.setBounds(10, 180, 500, 126);
		contentPane.add(pnlContato);
		pnlContato.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 22, 170, 93);
		pnlContato.add(scrollPane);
		
		JTextArea txtrJtextarea = new JTextArea();
		txtrJtextarea.setWrapStyleWord(true);
		scrollPane.setViewportView(txtrJtextarea);
		txtrJtextarea.setLineWrap(true);
		txtrJtextarea.setText("JTextArea dentro de um JScrollPane");
		
		JSpinner mySpinner = new JSpinner();
		mySpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		scrollPane.setColumnHeaderView(mySpinner);
		///////// APENAS N�MEROS
		JFormattedTextField txtonlynumbers = ((JSpinner.NumberEditor) mySpinner.getEditor()).getTextField();
		((NumberFormatter) txtonlynumbers.getFormatter()).setAllowsInvalid(false);
		/////////
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"categoria", "romance"}));
		comboBox.setSelectedIndex(0);
		comboBox.setBounds(190, 23, 149, 20);
		pnlContato.add(comboBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(190, 54, 38, 20);
		pnlContato.add(spinner);
		
		JDateChooser dcrJDateChooser = new JDateChooser();
		dcrJDateChooser.setBorder(null);
		dcrJDateChooser.setBounds(236, 54, 103, 20);
		pnlContato.add(dcrJDateChooser);
		
		JCheckBox chxEmprestado = new JCheckBox("Emprestado");
		chxEmprestado.setBounds(186, 81, 153, 23);
		pnlContato.add(chxEmprestado);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println( comboBox.getSelectedIndex() +"" + chxEmprestado.isSelected());
			}
		});
		btnSalvar.setBounds(10, 317, 500, 23);
		contentPane.add(btnSalvar);
	}
}
