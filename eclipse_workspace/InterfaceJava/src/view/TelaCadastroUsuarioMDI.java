package view;


import javax.swing.JInternalFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class TelaCadastroUsuarioMDI extends JInternalFrame {
	/**
	 * 
	 */
	private JTextField txtNew_usuario;
	private JTextField txtNew_senha;
	private JTextField txtNew_senha2;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public TelaCadastroUsuarioMDI() {
		setRootPaneCheckingEnabled(false);
		setClosable(true);
		setFrameIcon(new ImageIcon(TelaCadastroUsuarioMDI.class.getResource("/images/iconsbar/subitems/user_add.png")));
		setTitle("cadastrar um novo usu\u00E1rio");
		setBounds(100, 100, 213, 260);
		getContentPane().setLayout(null);
		
		JLabel lblNew_usuario = new JLabel("Usu\u00E1rio");
		lblNew_usuario.setFont(new Font("Ubuntu Mono", Font.PLAIN, 18));
		lblNew_usuario.setHorizontalAlignment(SwingConstants.LEFT);
		lblNew_usuario.setBounds(10, 11, 183, 20);
		getContentPane().add(lblNew_usuario);
		
		JLabel lblNew_senha = new JLabel("Senha");
		lblNew_senha.setHorizontalAlignment(SwingConstants.LEFT);
		lblNew_senha.setFont(new Font("Ubuntu Mono", Font.PLAIN, 18));
		lblNew_senha.setBounds(10, 72, 183, 20);
		getContentPane().add(lblNew_senha);
		
		JLabel lblNew_senha2 = new JLabel("Confirmar Senha");
		lblNew_senha2.setHorizontalAlignment(SwingConstants.LEFT);
		lblNew_senha2.setFont(new Font("Ubuntu Mono", Font.PLAIN, 18));
		lblNew_senha2.setBounds(10, 126, 183, 20);
		getContentPane().add(lblNew_senha2);
		
		txtNew_usuario = new JTextField();
		txtNew_usuario.setToolTipText("o seu username");
		txtNew_usuario.setBounds(10, 34, 183, 20);
		getContentPane().add(txtNew_usuario);
		txtNew_usuario.setColumns(10);
		
		txtNew_senha = new JTextField();
		txtNew_senha.setToolTipText("o seu username");
		txtNew_senha.setColumns(10);
		txtNew_senha.setBounds(10, 95, 183, 20);
		getContentPane().add(txtNew_senha);
		
		txtNew_senha2 = new JTextField();
		txtNew_senha2.setToolTipText("o seu username");
		txtNew_senha2.setColumns(10);
		txtNew_senha2.setBounds(10, 150, 183, 20);
		getContentPane().add(txtNew_senha2);
		
		JButton btnCriar = new JButton("criar");
		btnCriar.setForeground(UIManager.getColor("Button.disabledForeground"));
		btnCriar.setFont(new Font("Verdana", Font.PLAIN, 10));
		btnCriar.setBounds(118, 181, 75, 35);
		getContentPane().add(btnCriar);
		
		JButton btnCancelar = new JButton("cancelar");
		btnCancelar.setFont(new Font("Verdana", Font.PLAIN, 10));
		btnCancelar.setBounds(10, 181, 98, 35);
		getContentPane().add(btnCancelar);

	}
}
