package view;

import java.awt.*;
import javax.swing.*;

public class TelaPrincipal {
	private JFrame frmLogin;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal window = new TelaPrincipal();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TelaPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage(TelaPrincipal.class.getResource("/images/feed.png")));
		frmLogin.setTitle("Interface on Java");
		frmLogin.setResizable(false);
		frmLogin.setBounds(100, 100, 450, 192);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		frmLogin.setLocationRelativeTo(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 444, 27);
		frmLogin.getContentPane().add(menuBar);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		mnArquivo.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/page.png")));
		mnArquivo.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnArquivo);
		
		JMenu mnSair = new JMenu("Sair");
		mnSair.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/door_out.png")));
		mnSair.setHorizontalAlignment(SwingConstants.CENTER);
		mnArquivo.add(mnSair);
		
		JMenu mnUsuarios = new JMenu("Usu�rios");
		mnUsuarios.setHorizontalAlignment(SwingConstants.CENTER);
		mnUsuarios.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/user.png")));
		menuBar.add(mnUsuarios);
		
		JMenu mnCadastrarUsuario = new JMenu("Cadastrar");
		mnCadastrarUsuario.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/user_add.png")));
		mnUsuarios.add(mnCadastrarUsuario);
		
		JMenu mnExcluirUsuario = new JMenu("Excluir");
		mnExcluirUsuario.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/user_delete.png")));
		mnUsuarios.add(mnExcluirUsuario);
		
		JMenu mnAlterar = new JMenu("Alterar");
		mnAlterar.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/user_edit.png")));
		mnUsuarios.add(mnAlterar);
		
		JMenu mnProcurar = new JMenu("Procurar");
		mnProcurar.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/find.png")));
		mnUsuarios.add(mnProcurar);
		
		JMenu mnClientes = new JMenu("Clientes");
		mnClientes.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/group.png")));
		mnClientes.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnClientes);
		
		JMenu mnCadastrarCliente = new JMenu("Cadastrar");
		mnCadastrarCliente.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/group_add.png")));
		mnClientes.add(mnCadastrarCliente);
		
		JMenu mnExcluirCliente = new JMenu("Excluir");
		mnExcluirCliente.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/subitems/group_delete.png")));
		mnClientes.add(mnExcluirCliente);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setEnabled(false);
		menuBar.add(horizontalGlue);
		
		JMenu mnSobre = new JMenu("Sobre");
		mnSobre.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/images/iconsbar/information.png")));
		mnSobre.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnSobre);
	}
	

}
