// TODO
// ====
// implementar geração de arquivo de configuração XML (objects to XML)

package view;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


/**
*
* @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
* @since 15 de dez de 2016
*/
public class TelaLogin {

	private JFrame frmLogin;
	private JTextField txtLogin;
	private JPasswordField pwdSenha;
	
	private static final char[] correctPass = "root".toCharArray(); /// <<< a senha salva, aqui.

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaLogin window = new TelaLogin();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TelaLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage(TelaLogin.class.getResource("/images/feed.png")));
		frmLogin.setTitle("Interface on Java");
		frmLogin.setResizable(false);
		frmLogin.setBounds(100, 100, 450, 192);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		frmLogin.setLocationRelativeTo(null); // centralizar janela
		
		
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setBounds(10, 32, 48, 20);
		lblLogin.setFont(new Font("Ubuntu Mono", Font.PLAIN, 15));
		lblLogin.setHorizontalAlignment(SwingConstants.LEFT);
		frmLogin.getContentPane().add(lblLogin);
		
		txtLogin = new JTextField();
		txtLogin.setToolTipText("seu nickname aqui");
		txtLogin.setBounds(68, 32, 356, 20);
		frmLogin.getContentPane().add(txtLogin);
		txtLogin.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setHorizontalAlignment(SwingConstants.LEFT);
		lblSenha.setFont(new Font("Ubuntu Mono", Font.PLAIN, 15));
		lblSenha.setBounds(10, 59, 48, 20);
		frmLogin.getContentPane().add(lblSenha);
		
		// (c) http://www.codejava.net/java-se/swing/jpasswordfield-basic-tutorial-and-examples
		pwdSenha = new JPasswordField();
		pwdSenha.setFont(new Font("Tahoma", Font.PLAIN, 15));
		pwdSenha.setToolTipText("digite sua senha");
		pwdSenha.setBounds(68, 59, 356, 20);
		frmLogin.getContentPane().add(pwdSenha);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setFont(new Font("Verdana", Font.PLAIN, 15));
		btnEntrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!txtLogin.getText().isEmpty()){
					System.out.println("--------------------------------------------------");
					System.out.println("LOGIN:   " + txtLogin.getText());
					System.out.println("SENHA:   " + new String(pwdSenha.getPassword())); // EVITAR
					System.out.println("CORRETA? " + Arrays.equals(pwdSenha.getPassword(), correctPass));
					System.out.println("--------------------------------------------------");
					jButtonActionPerformed(e);
				}
			}
		});
		btnEntrar.setBackground(SystemColor.inactiveCaptionBorder);
		btnEntrar.setBounds(96, 87, 328, 67);
		frmLogin.getContentPane().add(btnEntrar);
		
		JLabel lblImagemLogin = new JLabel("");
		lblImagemLogin.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/dukeWaveRed.gif")));
		lblImagemLogin.setBounds(10, 90, 76, 64);
		frmLogin.getContentPane().add(lblImagemLogin);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 444, 27);
		frmLogin.getContentPane().add(menuBar);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		mnArquivo.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/page.png")));
		mnArquivo.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnArquivo);
		
		JMenu mnSair = new JMenu("Sair");
		mnSair.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/door_out.png")));
		mnSair.setHorizontalAlignment(SwingConstants.CENTER);
		mnArquivo.add(mnSair);
		
		JMenu mnUsuarios = new JMenu("Usuários");
		mnUsuarios.setHorizontalAlignment(SwingConstants.CENTER);
		mnUsuarios.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/user.png")));
		menuBar.add(mnUsuarios);
		
		JMenu mnCadastrarUsuario = new JMenu("Cadastrar");
		mnCadastrarUsuario.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/user_add.png")));
		mnUsuarios.add(mnCadastrarUsuario);
		
		JMenu mnExcluirUsuario = new JMenu("Excluir");
		mnExcluirUsuario.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/user_delete.png")));
		mnUsuarios.add(mnExcluirUsuario);
		
		JMenu mnAlterar = new JMenu("Alterar");
		mnAlterar.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/user_edit.png")));
		mnUsuarios.add(mnAlterar);
		
		JMenu mnProcurar = new JMenu("Procurar");
		mnProcurar.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/find.png")));
		mnUsuarios.add(mnProcurar);
		
		JMenu mnClientes = new JMenu("Clientes");
		mnClientes.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/group.png")));
		mnClientes.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnClientes);
		
		JMenu mnCadastrarCliente = new JMenu("Cadastrar");
		mnCadastrarCliente.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/group_add.png")));
		mnClientes.add(mnCadastrarCliente);
		
		JMenu mnExcluirCliente = new JMenu("Excluir");
		mnExcluirCliente.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/subitems/group_delete.png")));
		mnClientes.add(mnExcluirCliente);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setEnabled(false);
		menuBar.add(horizontalGlue);
		
		JMenu mnSobre = new JMenu("Sobre");
		mnSobre.setIcon(new ImageIcon(TelaLogin.class.getResource("/images/iconsbar/information.png")));
		mnSobre.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mnSobre);
	}
	
	private void jButtonActionPerformed(ActionEvent evt){
		// (c) http://www.tiexpert.net/programacao/java/joptionpane.php
		if(Arrays.equals(pwdSenha.getPassword(), correctPass))
			JOptionPane.showMessageDialog(null, "Bem Vindo, "+txtLogin.getText());
		else
			JOptionPane.showMessageDialog(null, "Acesso Negado!!", "ERRO AO REALIZAR LOGIN", JOptionPane.ERROR_MESSAGE);
		
	}
}
